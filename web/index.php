<?php
use Symfony\Component\Debug\Debug;


require_once __DIR__.'/../vendor/autoload.php';

$app = require __DIR__.'/../src/app.php';

// If on localhost use the dev page.. OTherwise use the production page

if (!preg_match('/10\./',$_SERVER['REMOTE_ADDR']) || !in_array(@$_SERVER['REMOTE_ADDR'], array('10.0.2.2', '127.0.0.1', 'fe80::1', '::1') )
) {
    ini_set('display_errors', 0);
    require __DIR__.'/../config/prod.php';

} else {

    ini_set('display_errors', 1);
    Debug::enable();
    require __DIR__.'/../config/dev.php';

}

require __DIR__.'/../src/controllers.php';
$app->run();
