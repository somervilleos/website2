<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;


class ArtistMarketController {



    public function artMarket(Request $request, Application $app){
        $app['request'] = $request;

        //$csv = str_getcsv(file_get_contents('web/files/data/holiday2020.csv'));
        $file = fopen('web/files/data/holiday2020.csv', 'r');
        $csv= array();
        $artistIDs= array();
        while (($line = fgetcsv($file)) !== FALSE) {

            //$line is an array of the csv elements
            $csv[]=$line;
            if (!empty($line[3])){
                $artistIDs[] = $line[3];
            }
        }
        fclose($file);



        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $artistList = $artistsListObj->getArtistsbyItinList($artistIDs);
        $artistListById = array();
        if (!empty ($artistList)){
            foreach ($artistList as $oneArtist){
                $key=$oneArtist['id'];
                $artistListById[$key]= $oneArtist;

            }
        }

        // some artists don't have artist profiles.. So merge the
        $blankEntry = array('name'=>"", 'id'=>''  ,'short_description'=>'', 'link'=>'' );
        $allEntries = array();
        foreach ($csv as $oneRecord){
            $id = $oneRecord[3];
            $name= $oneRecord[1];
            $url=$oneRecord[2];
            if (array_key_exists($id, $artistListById)){
                $short_description = $artistListById[$id]['ShortDescription'];
                $image_id = $id;
            } else {
                $short_description = '';
                $image_id = '';
            }



            $allEntries[] = array('name'=>$name, 'id'=>$id  ,'short_description'=> $short_description, 'link'=>$url,'image_id'=>$image_id );

        }



        return $app['twig']->render('2020_Holiday_index_boot4.html.twig',array('market_data'=>$csv, 'artist_data'=>$allEntries));


    }

}