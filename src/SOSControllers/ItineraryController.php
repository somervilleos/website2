<?php


namespace SOSControllers;


use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class ItineraryController
{





    public function process ($command, $id, Request $request, Application $app){

        if ($command == 'add'){

            return $this->add ($id,  $request,  $app);
        } elseif ($command == 'remove'){

            return $this->remove ( $id,  $request,  $app);
        }



    }



    public function add ($idToAdd, Request $request, Application $app)

    {

        $itin = \SOS\ItineraryCookie::get();
        $itinObj = new \SOSModels\Itinerary($app['pdo']);

        // Get existing itinerary.  Reutrns empty if itinerary cookie is set and the itinerary row doesn't exist in the database.
        $doesRowExist = $itinObj->checkRowExists($itin);

       /* print "<pre> ";
        var_dump ($doesRowExist);
        print "</pre> ";
*/
        // No Cookie, crete one
        if (empty($itin) or empty($doesRowExist)) {


            $result = $itinObj->addFirstArtistToItinerary($idToAdd);

            // set the cookie

            \SOS\ItineraryCookie::create($result['editKey'], $result['shareKey']);


            $response = '{"message" : "Added First to Itinerary.  You can select to view your itinerary from the Browse page","total":1}';


        } else {


            $artistList = $itinObj->addArtistToItinerary($idToAdd, $itin['edit_key'], $itin['share_key']);


            if (is_null($artistList)){
                $response = '{"message" : "Could  not add","total":-1}';
            } else {
                $total = count($artistList);
                $response = '{"message" : "Added To Itinerary","total":' . $total . '}';
            }

        }

        return $response;
    }





    public function remove ($idToRemove, Request $request, Application $app)

    {

        $itin = \SOS\ItineraryCookie::get();
        $itinObj = new \SOSModels\Itinerary($app['pdo']);

        // No Cookie, crete one
        if (empty($itin)) {

            return '{"message" : "Could  not find cookie, to remove","total":-1}';


        } else {

            $artistList = $itinObj->removeArtistFromItinerary($idToRemove, $itin['edit_key'], $itin['share_key']);

            if (is_null($artistList)){
                $response = '{"message" : "Could  not add","total":-1}';
            } else {
                $total = count($artistList);
                $response = '{"message" : "Added To Itinerary","total":' . $total . '}';
            }

        }

        return $response;
    }


    public function manage (Request $request, Application $app)

    {
        $newItin = false;

        $app['request'] = $request;
        $itin = \SOS\ItineraryCookie::get();
        $itinObj = new \SOSModels\Itinerary($app['pdo']);

        if (empty($itin)) {

            $itinData = null;

        } else {

            $itinData = $itin;
/*
            array (size=2)
  'share_key' => string 'CGmV0004' (length=8)
  'edit_key' => string 'YGEe0004' (length=8)
  */
        }


        $formObj = new \SOSForms\ItineraryForm();
        $form = $formObj->getForm($app);


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();
                $formObj->processForm($app, $app['pdo'], $formData);

                $newItin = true;

            }
        }


        // Only send email if itinerary set



        $mailFormObj= new \SOSForms\ItineraryEmail();

        $mailForm = $mailFormObj->getForm($app);

        if (!empty($itinData)) {
            $mailForm->handleRequest($request);
            if ($mailForm->isSubmitted()) {
                if ($mailForm->isValid()) {
                    $formData = $mailForm->getData();
                    $mailFormObj->processForm($app, $itinData, $formData);

                    $newItin = true;

                }
            }
        }


        //Get artists


        $itin = \SOS\ItineraryCookie::get();
        if (empty($itin)) {
            $artistData = array();
        } else {
            $itinObj = new \SOSModels\Itinerary($app['pdo']);
            $artistIDs = $itinObj->getItinerayArtistIDsFromKey($itin['share_key']);
            if (!empty($artistIDs)) {
                $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);

                $artistData = $artistsListObj->getArtistsbyItinList($artistIDs);
            } else {
                $artistData = array();
            }
        }



        return $app['twig']->render('artists/manage_itinerary.html.twig',array('itin_data' => $itinData, 'form'=>$form->createView(), 'mail_form'=>$mailForm->createView(),'new_itin'=> $newItin, 'artist_data'=>$artistData )  );



    }


    /**
     * Set, sets the itinerary from the cookie.  It replaces existing cookie values.
     *
     *
     * @param Request $request
     * @param Application $app
     *
     */

    public function set ($share_key, $edit_key, Request $request, Application $app) {

        // set the cookie and redirect to the manage page.
        \SOS\ItineraryCookie::create($edit_key, $share_key);

        // redirect
        $app['session']->getFlashBag()->add('info','Updated Itinerary Information. ');

        return $app->redirect($app["url_generator"]->generate("manage_itin"));



    }





}