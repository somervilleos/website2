<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/2/15
 * Time: 12:36 AM
 */

namespace SOSControllers;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class ProfileEventPage {




    private function getExistingData($dbo, $member_id) {


        $sql = 'select * from `refunds2020`  where member_id = :member_id Limit 1  ';


        $stmt = $dbo->prepare($sql);
        $stmt->bindValue(':member_id',$member_id, \PDO::PARAM_STR);

        // print "sql: $sql <br>";

        if ($stmt->execute()) {

            $return = $stmt->fetch(\PDO::FETCH_ASSOC);

        } else {
            $return = false;

            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());
            }
        }

        return $return;

    }



    public function eventUpdate (Request $request, Application $app) {


        $app['request'] = $request;
        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfo();

        $eventObj = new \SOSModels\EventData($app['pdo'], $app['year']);
        $eventInfo = $eventObj->getEventListingAll($userInfo['id']);
        $onlyEvents = array();


        $eventsToKeep = array(1013,1020,1017);
        foreach ($eventsToKeep as $event_id){
            if (array_key_exists ($event_id, $eventInfo)){
                $onlyEvents[$event_id] = $eventInfo[$event_id];
            }

        }
        $saved = false;

        $formObj = new \SOSForms\Events2021Form();
        $form = $formObj->getForm($app, $profile, $eventInfo);
        $formStatus = false;


        $existingData = array();
        // Check form if submitted


        $form->handleRequest($request);


        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $formData = $form->getData();

                $returnPage = $formObj->processFormData($app['pdo'], $app,  $formData,  $userInfo['id'], $eventInfo );

                if ($returnPage) {
                    $app['session']->getFlashBag()->add('info', 'Preference Saved!  ');
                } else {
                    $app['session']->getFlashBag()->add('danger', 'Could Not Save.. Please Try Again (or email webmaster@somervilleopenstudios.org');

                }

                // get the event list again.. It might be updated.

                $eventInfo = $eventObj->getEventListingAll($userInfo['id']);

                $saved = true;

                foreach ($eventsToKeep as $event_id){
                    if (array_key_exists ($event_id, $eventInfo)){
                        $onlyEvents[$event_id] = $eventInfo[$event_id];
                    }

                }


            }
        }

        $eventlist = $formObj->defaultData;





        return $app['twig']->render('for_artists/events.html.twig',array(
            'member_info' => $artistData,
            "form"=>$form->createView(),
            "saved"=> $saved,
            "event_info"=>$onlyEvents));

    }


}