<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/2/15
 * Time: 12:36 AM
 */

namespace SOSControllers;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class RefundPage {




    private function getExistingData($dbo, $member_id) {


        $sql = 'select * from `refunds2020`  where member_id = :member_id Limit 1  ';


        $stmt = $dbo->prepare($sql);
        $stmt->bindValue(':member_id',$member_id, \PDO::PARAM_STR);

        // print "sql: $sql <br>";

        if ($stmt->execute()) {

            $return = $stmt->fetch(\PDO::FETCH_ASSOC);

        } else {
            $return = false;

            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());
            }
        }

        return $return;

    }



    public function refund($member_id,$hid,Request $request, Application $app) {




/* Full grants
"
("1944",
"1980".
"139",
"215",
"1297",
"907",
"2003")
*/

        $artistObj = new \SOSModels\ArtistData($app['pdo'], $member_id);
        $artistData = $artistObj->getArtistInfo();


        $app['request'] = $request;

        $hs =sha1("234(&3459&*".$member_id);


        if ($hid != $hs){

            return "invalid id";
        }
        // Get member profile:

        if (empty ($artistData )){
            return "invalid id";
        }
/*
        if ($hid != $artistData['temp_login']){
            return "invalid id";
        }
  */

/*
 * SELECT * FROM `payments` WHERE `date_posted` >= '2019-12-15' and payment_confirmed ="Y" and id in ("1940","1796","1602","1337") ORDER BY `payments`.`id` DESC 
 *
 */

        /*--------------------------------------
              Get Existing Selection if Any.
        */

        $existingData = $this->getExistingData($app['pdo'], $member_id);

        /*--------------------------------------
        Get Payment Info (If available.
        ---------------------------- */

        $paymentObj = new  \SOSModels\Payments($app['pdo']);
        $paymentData = $paymentObj->getPaymentByPaymentID($artistData['payment_id']);
        $searchForm = new \SOSForms\Refund2020Form();
        $form = $searchForm->getForm($app, $existingData, $member_id, $artistData, $paymentData );

        // Check form if submitted


        $form->handleRequest($request);


        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $formData = $form->getData();

                $returnPage = $searchForm->processFormData($app['pdo'],  $existingData, $member_id, $formData, $app);

                if ($returnPage) {
                    $app['session']->getFlashBag()->add('info', 'Preference Saved.  Return to this page anytime to check your refund status.');
                } else {
                    $app['session']->getFlashBag()->add('danger', 'Could Not Save.. Please Try Again (or email refunds@somervilleopenstudios.org');

                }

                $existingData = $this->getExistingData($app['pdo'], $member_id);


            }
        }


        return $app['twig']->render('refund_boot4.html.twig',array(
            'member_info' => $artistData,
            'existing_data' => $existingData,
            "form"=>$form->createView(),
            "payment_info"=>$paymentData));

    }


}