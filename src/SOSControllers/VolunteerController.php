<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

use Silex\Application;
use Symfony\Component\Form\FormError;
class VolunteerController
{



    public function signup(Request $request, Application $app)
    {
        $app['request'] = $request;

        $infoArray = array();
        $warningArray = array();
        $formDataOk= false;

        $start_num=1;
        $stmt = $app['pdo']->prepare('SELECT * FROM current_active_artists_view LIMIT ?,10');
        $stmt->bindValue(1, $start_num, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows= $stmt->fetchAll();
        //var_dump ($all_rows);


        $formObj = new \SOSForms\VolunteerForm();
        $form = $formObj->getForm($app);


        // Check form if submitted

/* Adding to a form object after the fact..
 *         $form->add ('vol_pos', 'choice',  array( 'label'=>"Volunteer Positions", 'choices'  => $volunteerChoices,
            'multiple' => true,
            'expanded' => true,
            'required' => true,
            'attr' => array('style' => 'margin-left:20px; color:red')));
*/





        $volunteerMenu = array('HOME'=>'homepage', 'ABOUT VOLUNTEERING'=>'volunteer.home', 'VOLUNTEER'=>'volunteer.signup');

        // Check form if submitted


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();
                // check password



                if (empty($formData['vol_pos'])){
                    $warningArray[]= ' At least one box needs to be checked';
                    $formDataOK = false;
                    $form->get('vol_pos')->addError(new FormError('Need at least one should be checked'));
                } else {

                    $formDataOK = true;

                    $addedVolunteer = $formObj->addNewVolunteer($app['pdo'], $formData);


                    if ($addedVolunteer){
                        return $app->redirect('/volunteer/thanks');
                    }

                }

            } else {
                $warningArray[] = 'Form could not be processed, please try again';
                //$app['session']->getFlashBag()->add('info', 'The form is bound, but not valid');
            }
        }

        $jobDetails = $formObj->positionList();

        return $app['twig']->render('volunteer/signup.html.twig',array('form'=> $form->createView(), 'infoArray'=>$infoArray, 'warningArray'=>$warningArray,
            'main_menu' => $volunteerMenu, 'job_details'=>$jobDetails));

        //return new Response("Actor Action respose (bar)".$id);







    }


}