<?php

/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 2/10/16
 * Time: 12:01 AM
 */
class Sos_Checkup {


    private $sos_dbo;
    private $imageProfile;
    private $memberID;


    public function __construct (\PDO $dbo , $memberID){
        $this->sos_dbo = $dbo;
        $this->memberID = $memberID;
    }



    public function auditUser($userID){

        // get member info
        $memberinfo=1;



    }


    public function getAuditInformation (){

        $memberInfo = $this->getArtistInfo();
        $problemList= array();
        if (empty($memberInfo['ShortDescription'])){
            $problemList[]="Your Short Description of your art is empty <br>Consider adding it";
        }

        if ($memberInfo['BusinessNameOption']=='Y' && empty($memberInfo['BusinessName'])) {
            $problemList[]="You have a business Name but it is currently empty<br>Consider adding it";
        }




        return $problemList;
    }




    public function getArtistInfo(){

        if ($this->imageProfile == null) {

            $sql = "SELECT * from current_active_artists_view v JOIN statement s ON (v.id=s.member_id) WHERE v.id = :member_id";

            $stmt = $this->sos_dbo->prepare($sql);
            $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);

            $stmt->execute();
            $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (empty($row)){
                return null;
            }
            $this->imageProfile = $row[0];

        }

        //shuffle ($all_rows);
        return $this->imageProfile;

    }



    /*
     *
     *
     *  array (size=3)
    0 =>
      array (size=7)
      'id' => string '3991' (length=4)
      'member_id' => string '592' (length=3)
      'ImageNumber' => string '1' (length=1)
      'Title' => string 'surfer - Hurley pro tour' (length=24)
      'Medium' => string 'photo' (length=5)
      'Dimensions' => string 'photo size' (length=10)
      'filename' => string 'artist_files/artist_images/profile/592-01.jpg' (length=45)
  1 =>
    array (size=7)
     *
     */

    // returns an array of images, but data only if the images exist

    public function getArtistImagesData(){

        $this->getArtistInfo();
        $oneArtist = $this->imageProfile ;


        if ($this->imageProfile['Expanded_Web'] == 'Y'){
            $numberOfImages = 8;
        } else {
            $numberOfImages = 3;
        }

        $sos_image_path='artist_files/artist_images/profile/';
        $imageArray = array();
        $jpg_number= array( '01','02','03','04','05','06','07','08');

        // lookup imaage details in database

        $sql = "SELECT * from image WHERE member_id = :member_id GROUP BY ImageNumber";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        // index based on image number..
        $imageDetails = array();
        foreach ($rows as $oneImage){
            $imageDetails[($oneImage['ImageNumber'])] = $oneImage;
        }


        for ($i=0; $i< $numberOfImages; $i++){
            $profile_jpg = $sos_image_path  . $oneArtist['member_id'] . '-'.$jpg_number[$i] .'.jpg';

            if (file_exists( $profile_jpg )){
                $indx = $i+1;

                if (array_key_exists($indx,$imageDetails)) {
                    $imageDetails[$indx]['filename']= $profile_jpg;
                    $imageArray[] = $imageDetails[$indx];

                } else {

                    // No data about the image.  Shouldn't happen, but if it does return blanks...
                    $imageArray[] = array ( 'filename'=> $profile_jpg, 'id' => '','member_id' =>'', 'ImageNumber' =>'','Title' => '','Medium' => '','Dimensions' => '');
                }
            }
        }

        return $imageArray;

    }


    // same as the above one

    public function getArtistImagesData(){

        $this->getArtistInfo();
        $oneArtist = $this->imageProfile ;


        if ($this->imageProfile['Expanded_Web'] == 'Y'){
            $numberOfImages = 8;
        } else {
            $numberOfImages = 3;
        }

        $sos_image_path='artist_files/artist_images/profile/';
        $imageArray = array();
        $jpg_number= array( '01','02','03','04','05','06','07','08');

        // lookup imaage details in database

        $sql = "SELECT * from image WHERE member_id = :member_id GROUP BY ImageNumber";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        var_dump ($rows);

        // index based on image number..
        $imageDetails = array();
        foreach ($rows as $oneImage){
            $imageDetails[($oneImage['ImageNumber'])] = $oneImage;
        }
        var_dump ($imageDetails);

        for ($i=0; $i< $numberOfImages; $i++){

            $profile_jpg = $sos_image_path  . $oneArtist['member_id'] . '-'.$jpg_number[$i] .'.jpg';
            if (file_exists( $profile_jpg )){

            } else {
                $profile_jpg = $sos_image_path  ."noimage.jpg";
            }
            $indx=$i+1;
            if (array_key_exists($indx,$imageDetails)) {

                $imageDetails[$indx]['filename']= $profile_jpg;
                $imageArray[] = $imageDetails[$indx];

            } else {

                // No data about the image.  Shouldn't happen, but if it does return blanks...
                $imageArray[] = array ( 'filename'=> $profile_jpg, 'id' => '','member_id' =>'', 'ImageNumber' =>$indx,'Title' => '','Medium' => '','Dimensions' => '');
            }

        }

        return $imageArray;

    }





}