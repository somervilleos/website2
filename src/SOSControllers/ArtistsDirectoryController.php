<?php

namespace SOSControllers;
use ClassesWithParents\D;
use SOS\ItineraryCookie;
use SOSForms\SearchArtistForm;
use SOSModels\ArtistsGenreList;
use SOSModels\ArtistsList;
use SOSModels\EventData;
use SOSModels\Globals;
use SOSModels\Itinerary;
use SOSModels\TrolleysList;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;
use SOS\mapOverlay;


// Class to retreive artist data from the database and display
//
// Some functions show search and by medium


class ArtistsDirectoryController {

    public function bar($id, Request $request,  Application $app)
    {

        return new Response("Actor Action respose (bar)".$id);


    }

    private function storeSearchIds (Request $request, Application $app, $artists, $searchName ='search'){

        $app['request'] = $request;

        $url = $request->getRequestUri();
        $resultSimple = array_map(function($row) {return $row['id'];}, $artists);

        /* searchType=alpha may result in duplicate rows if an artist's PublicLastName and BusinessName
           both have same initial letter.  Remove duplicates from $resultSimple so that 'Next Result'
           and 'Previous Result' iterations on artist profile pages work correctly. */

        $resultSimple = array_values(array_unique($resultSimple));

        $app['session']->set('searchList', $resultSimple);
        $app['session']->set('searchName', $searchName);
        $app['session']->set('searchURL', $url);


    }





    function showRandom($type,  Request $request, Application $app) {
        $app['request'] = $request;

        //echo $id ."<br>";
        $listName = "Random Order";
        $artistsListObj = new ArtistsList($app['pdo']);

       if ($type =='map'){
            $artistsListObj->setSort('map_number, PublicLastName, PublicFirstName');
            $artistsListObj->setOnlyParticipating();
        }
        $artistData = $artistsListObj->getRandom(500);



        // generate urls for the search for list/grid/map
        $typeLinks = array();
        $typeLinks['list'] = $app['url_generator']->generate('artists_directory_random', array('type'=> 'list'));
        $typeLinks['grid'] = $app['url_generator']->generate('artists_directory_random', array('type'=> 'grid'));
        $typeLinks['map'] = $app['url_generator']->generate('artists_directory_random', array('type'=> 'map'));

        $genre='All';
        $event='All';
        $map_number = 'All';
        $searchName = "All in Random Order";


        $this->storeSearchIds($request, $app, $artistData, $listName);


        return $this-> generateListing ($request, $app, $artistsListObj, $artistData, $genre, $event, $map_number, $type,"random", $searchName, $typeLinks );

    }





    public function showListbySearchTerm($search, $display_type, Request $request, Application $app){
        $app['request'] = $request;

        //echo $id ."<br>";

        $artistsListObj = new ArtistsList($app['pdo']);

       if ($display_type =='map'){
            $artistsListObj->setSort('map_number, PublicLastName, PublicFirstName');
            $artistsListObj->setOnlyParticipating();
        }

        $searchName = "Search For Term: ".$search;

        $artistData = $artistsListObj->getBySearchTerm($search);
        $this->storeSearchIds($request, $app, $artistData, $searchName);
        $genre='All';
        $event='All';
        $map_number = 'All';




        // generate urls for the search for list/grid/map
        $typeLinks = array();
        $typeLinks['list'] = $app['url_generator']->generate('artists_directory_search', array('search'=>$search,  'display_type'=> 'list'));
        $typeLinks['grid'] = $app['url_generator']->generate('artists_directory_search', array('search'=>$search,  'display_type'=> 'grid'));
        $typeLinks['map'] = $app['url_generator']->generate('artists_directory_search', array('search'=>$search,  'display_type'=> 'map'));

        $this->storeSearchIds($request, $app, $artistData, $searchName);
        // If one go directly to arist profile

        if (count($artistData) ==1){
            $oneArtists = reset($artistData);
            return $app->redirect($app["url_generator"]->generate("artists_profile",  array('id' => $oneArtists['id'])));

        }
        return $this-> generateListing ($request, $app, $artistsListObj, $artistData, $genre, $event, $map_number, $display_type, "default", $searchName, $typeLinks );



    }





//rraman: START

    public function trolleyLog(Request $request, Application $app){
        $num = $request->query->get('num');
        $lat = $request->query->get('lat');
        $lng = $request->query->get('lng');

        $trolleysListObj = new TrolleysList($app['pdo']);
        $trolleysListObj->update($num, $lat, $lng);
        return new Response("OK");
    }

    public function trolleyAjax(Request $request, Application $app){
        $trolleysListObj = new TrolleysList($app['pdo']);
        $updateJson = $trolleysListObj->updateJson();
        return new Response($updateJson);
    }

    public function showListTrolley(Request $request, Application $app){

        // copied from 2018's location.php:
        //$mapData = new mapOverlay("../../images/mapicons/");
        //$map_trolley_json = $mapData->getTrolleyStops();
        //$map_info_json = $mapData->getInfoBooths();
        //$map_parking_json = $mapData->getParkingLots();
        //$map_exhibits_json = $mapData->getExhibits();
        //$routeArray = $mapData->getTrolleyRoute();
        // END: copied...

        //echo $id ."<br>";
        $app['request'] = $request;


        $trolleysListObj = new TrolleysList($app['pdo']);
        $mapJson = '';


        $searchName = 'All Trolleys ';

        $type = 'map';
        // Map, only show participating artitst
        //if ($type =='map'){
        //$artistsListObj->setSort('map_number, PublicLastName, PublicFirstName');
        //$artistsListObj->setOnlyParticipating();
        //}
        $items = $trolleysListObj->getAll();
        //$viewlist = 'artists/artist_list.html.twig';

        //if ($type =='grid'){
        //    $viewlist = 'artists/artist_list_grid.html.twig';
        //} elseif ($type =='map'){
        $viewlist = 'artists/artist_list_trolley.html.twig';
        $mapJson = $trolleysListObj->listToJson($items/*, "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"'*/);
        //}

        // generate urls for the search for list/grid/map
        //$typeLinks = array();
        //$typeLinks['list'] = $app['url_generator']->generate('artists_directory_list_all', array('type'=> 'list'));
        //$typeLinks['grid'] = $app['url_generator']->generate('artists_directory_list_all', array('type'=> 'grid'));
        //$typeLinks['map'] = $app['url_generator']->generate('artists_directory_list_all', array('type'=> 'map'));

        $mapSettings = $this->getMapSettings();
        $mapSettings['geolocation'] = true;
        $mapSettings['showSponsors'] = false;
        //$this->storeSearchIds($request, $app, $artists);

        // establish that we want trolley markers to auto-update with ajax:
        $mapSettings['ajaxURI'] = '/artists/artist_directory/trolleyajax/';
        return $app['twig']->render($viewlist, array(
            //'artist_data'=>$artists,
            'map_json'=> $mapJson,
            'map_settings'=> $mapSettings,
            'map_settings_json'=> json_encode($mapSettings),
            'list_name'=>$searchName,
            'main_menu' =>  $app['artistsMenu'],
            //'map_trolley_json' => $map_trolley_json,
            //'map_info_json' => $map_info_json,
            //'map_parking_json' => $map_parking_json,
            //'routeArray' => $routeArray
            //'type_link'=>$typeLinks
        ));

    }

//rraman: END


    public function showArtists2($display_type, $genre, $event, $map_number, Request $request, Application $app) {
        $displayOrder = "random";
        return ($this->showArtists3(  $display_type, $genre, $event, $map_number, $displayOrder, $request, $app  ));

    }
// http://127.0.0.1:8888/artists/artist_directory2/display=list/medium=painting,photography/event=All/map_num=All


    /** 2024 ARtists lists
     *
     * This takes a lot of parameters and returns a list of artists.
     * map_number/event/genre can be comma separated lists of search terms.
     *
     * @param $display_type
     * @param $genre
     * @param $event  (favorites for favorite/itinerary)
     * @param $map_number
     * @param Request $request
     * @param Application $app
     * @return mixed
     */

    public function showArtists3($display_type, $genre, $event, $map_number, $display_order, Request $request, Application $app)
    {


        $app['request'] = $request;

        $artistsListObj = new ArtistsList($app['pdo']);

        if (strtolower($display_order) == "random"){
            $artistsListObj->setSort('random');

        } else if (strtolower($display_order) == "map") {
            $artistsListObj->setSort('map_number, PublicLastName, PublicFirstName');
        }


        if ($display_type =='map'){
          //  $artistsListObj->setSort('map_number, PublicLastName, PublicFirstName');
            $artistsListObj->setOnlyParticipating();
        }


        // favorites/itinerary



        // Get The Artists:
        if ($event =="favorites"){
            $top_includes = "blocks/itinerary_description.html.twig";
	            $itin = ItineraryCookie::get();
            if (empty($itin)) {
                $artistData = array();
            } else {
                $itinObj = new Itinerary($app['pdo']);
                $artistIDs = $itinObj->getItinerayArtistIDsFromKey($itin['share_key']);
                if (!empty($artistIDs)) {
                    $artistData = $artistsListObj->getArtistsbyItinList($artistIDs);
                } else {
                    $artistData = array();
                }
            }
        } else {

            $artistData = $artistsListObj->getArtistListMuliti($genre, $event, $map_number, 'and');
        }

        // generate urls for the search for list/grid/map  of this search

        $typeLinks = array();

        $typeLinks['list'] = $app['url_generator']->generate('artists_directory_list', array('display_type'=> 'list' ,'genre'=> $genre, 'event'=>$event, 'map_number'=>$map_number  ,"display_order" => $display_order));
        $typeLinks['grid'] = $app['url_generator']->generate('artists_directory_list', array('display_type'=> 'grid' ,'genre'=> $genre, 'event'=>$event, 'map_number'=>$map_number  ,"display_order" => $display_order ));
        $typeLinks['map'] = $app['url_generator']->generate('artists_directory_list', array('display_type'=> 'map'  ,'genre'=> $genre, 'event'=>$event, 'map_number'=>$map_number ,"display_order" => $display_order ));


        // Get the proper tempplate or view file based on type of search.

        $searchName = $artistsListObj->getSearchDescription();
        $this->storeSearchIds($request, $app, $artistData, $searchName);
        $topIncludes = $artistsListObj->getTopIncludes();
        $bottomIncludes = $artistsListObj->getBottomIncludes();
        if ($event =="favorites") {
            $topIncludes = "blocks/itinerary_description.html.twig";
        }

        if (strtolower($genre) == "all") {
            $top_link = 'blocks/listing_blurb.html.twig';
        }

        return $this-> generateListing ($request, $app, $artistsListObj, $artistData, $genre, $event, $map_number, $display_type, $display_order,
            $searchName, $typeLinks, $topIncludes, $bottomIncludes );




    }


    // Common rendering for all searches


    private function generateListing ($request, $app, $artistsListObj, $artistData, $genre, $event, $map_number, $display_type,
                                     $display_order, $searchName, $typeLinks, $top_include = null, $bottom_include= null)
    {


        // we should get the itinerary so we can populate the page with that information.

/*        $itin = \SOS\ItineraryCookie::get();
        We're doing this twice
        $itinArtistIds = array();
        if (!empty($itin)){
            $itinObj = new \SOSModels\Itinerary($app['pdo']);
            $itinArtistIds =  $itinObj->getItinerayArtistIDsFromKey($itin['share_key']);
        }
*/

        $searchForm = new SearchArtistForm();

        $form = $searchForm->getForm($app);

        // Check form if submitted


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                $returnPage = $searchForm->processFormData($formData, $app);
                if (!empty($returnPage)) {
                    return $returnPage;
                }
            }
        }


        $mapJson = '';
        $listName = $searchName;


        if ($display_type =='grid'){
            $viewlist = 'artists/artist_list_grid.html.twig';

        } elseif ($display_type =='map' ) {
            //$viewlist = 'artists/artist_list_map.html.twig';
            $viewlist = 'artists/artist_list.html.twig';
            $mapJson = $artistsListObj->listToJson($artistData, "web/artists/artist_directory_list/display=list/medium=All/event=all/map_num=", "artists/artist_profile/", 'target="_blank"');

            // rraman: hacky way of appending trolleys onto artists!...
            $trolleysListObj = new TrolleysList($app['pdo']);
            $trolleyItems = $trolleysListObj->getAll();
            $trolleyMapJson = $trolleysListObj->listToJson($trolleyItems/*, "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"'*/);
            $mapJson = json_decode($mapJson, true);
            $mapJson['markers'] = array_merge($mapJson['markers'], json_decode($trolleyMapJson, true)['markers']);
            $mapJson = json_encode($mapJson);

        } else { // "list" mode

            $viewlist = 'artists/artist_list.html.twig';
            // rraman: why is this here? map not displayed in "list" mode
            // $mapJson = $artistsListObj->listToJson($artistData, "web/artists/artist_directory_list/display=list/medium=All/event=all/map_num=", "artists/artist_profile/", 'target="_blank"');
        }

        $mapSettings = $this->getMapSettings();

        // get any map overrides (eg friday maps contain no trolley/ sponsors)..

        $mapOverrides = $artistsListObj->getMapOverrides();

        if (!empty ($mapOverrides)){
            foreach ($mapOverrides as $okey=>$ovalue){
                $mapSettings[$okey]=$ovalue;
            }
        }

        // show "you are here" on map-view
        $mapSettings['geolocation'] = true;


        $mediums = ArtistsGenreList::$column_name_to_genre_name;
        $genreList = explode(',',$genre);
        // get events
        $eventObj = new EventData($app['pdo'], $app['year']);
        $events = $eventObj->getAllEventsApprovedArtists();
        $eventDetails = $eventObj->getAllEvents();
       // $eventDescription = $eventObj->getEventDescription($event);

        // Get itinerary information (if any)
        $itin = ItineraryCookie::get();

        $itinArtists = array();
        if (!empty($itin)){
            $itinObj = new Itinerary($app['pdo']);
            $itinArtistIds =  $itinObj->getItinerayArtistIDsFromKey($itin['share_key']);
            if (!empty($itinArtistIds)){
                $itinArtists = $itinArtistIds;
            }
        }

        $thumb_cache ='';
        if(isset($app['cache_date_string'])){
            if (! empty($app['cache_date_string'])){
                $dateformat = $app['cache_date_string'];
                $thumb_cache ='?r='. Date($dateformat);
            }
        }


        return $app['twig']->render($viewlist, array('artist_data'=>$artistData,
            'genre'=>$genre,
            'event'=>$event,
            'map_number'=>$map_number,
            'display_type'=>$display_type,
            'display_order'=>$display_order,
            'map_json'=> $mapJson,
            'map_settings'=> $mapSettings,
            'map_settings_json'=> json_encode($mapSettings),
            'list_name'=>$listName,
            'main_menu' =>  $app['artistsMenu'],
            'type_link'=>$typeLinks ,
            'top_include'=> $top_include,
            'bottom_include'=>$bottom_include,
            'mediums'=>$mediums,
            'mediums_array' => $genreList,
            'search_form' => $form->createView(),
            'events'=>$events,
            'event_details' => $eventDetails,
            'itin_artists'=>$itinArtists,
            'thumb_cache'=>$thumb_cache
            ));

    }






    function showItinerary($type,  Request $request, Application $app) {
        $app['request'] = $request;

        //echo $id ."<br>";
        $listName = "My Itinerary";
        $artistsListObj = new ArtistsList($app['pdo']);

        if ($type =='map'){
            $artistsListObj->setSort('map_number, PublicLastName, PublicFirstName');
            $artistsListObj->setOnlyParticipating();
        }


        $itin = ItineraryCookie::get();
        $itinObj = new Itinerary($app['pdo']);

        // No Cookie, crete one
        if (empty($itin)) {
            $artistData = array();
        } else {

            $artistIDs = $itinObj->getItinerayArtistIDsFromKey($itin['share_key']);
            if (!empty($artistIDs)) {
                $artistData = $artistsListObj->getArtistsbyItinList($artistIDs);
            } else {
                $artistData = array();
            }
        }


        // generate urls for the search for list/grid/map
        $typeLinks = array();
        $typeLinks['list'] = $app['url_generator']->generate('artists_directory_itin', array('type'=> 'list'));
        $typeLinks['grid'] = $app['url_generator']->generate('artists_directory_itin', array('type'=> 'grid'));
        $typeLinks['map'] = $app['url_generator']->generate('artists_directory_itin', array('type'=> 'map'));

        $genre='All';
        $event='All';
        $map_number = 'All';
        $searchName = "Itinerary";

        $top_include = "blocks/itinerary_description.html.twig";
        $this->storeSearchIds($request, $app, $artistData, $listName);
        $display_order = "default";

        return $this-> generateListing ($request, $app, $artistsListObj, $artistData, $genre, $event, $map_number, $type,$display_order, $searchName, $typeLinks, $top_include );

    }





    private function getMapSettings(){

        $mapSettings = array('bounds'=>true,
            'showSponsors'=>false,
            'showExhibits'=>false,
            'showTrolley'=>false,
            //'showTrolley'=>true, // rraman: now hardcoded to true in JS lib
            'ajaxURI'=>'/web/artists/artist_directory/trolleyajax/',
            'initial_center'=>'42.391228,-71.101692',
            'zoom-extents'=>true,
            'showMapNumber'=> Globals::$showMapNumber);
        return $mapSettings;
    }

}
