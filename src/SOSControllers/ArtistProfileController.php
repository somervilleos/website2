<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;


class ArtistProfileController {

    public function bar($id, Request $request,  Application $app)
    {

        return new Response("Actor Action respose (bar)".$id);


    }


    public function getRandomThumb (Request $request, Application $app) {

        $app['request'] = $request;

        // get menu

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $randomArtists = $artistsListObj->getRandom(12);

        $thumb_dir='artist_files/artist_images/splash/';

        // check if image files exist..  Only show files that exist so collect artists that have images
        $imageCount = 0;
        $imageMax = 1;
        $artistArray = array();
        foreach ($randomArtists as $key=>$oneArtist ){
            if ($imageCount < $imageMax && file_exists($thumb_dir."/".$oneArtist['id'].".jpg" )){
                $file= $thumb_dir."/".$oneArtist['id'].".jpg";
                $stream = function () use ($file) {
                    readfile($file);
                };
                return $app->stream($stream, 200, array('Content-Type' => 'image/jpeg'));

            }
        }

       // https://stackoverflow.com/questions/17076738/how-to-return-an-image-to-an-https-request-with-php





        return null;

    }







    public function showProfile($id, Request $request, Application $app){

        $app['request'] = $request;
        // lets see if the session data made it to here.
        // we never know...
        $searchList =$app['session']->get('searchList');
        $searchName =$app['session']->get('searchName');
        $searchURL =$app['session']->get('searchURL');

        $key = null;
        $searchNav = array();

        // we have a list of items.  Find the location of the current artists.  generate the next/previous first/last links
        if (!empty($searchList) && count($searchList) >1) {
            $key = array_search($id, $searchList);

            $searchNav['first'] = $searchList[0];
            $searchNav['last'] = $searchList[count($searchList)-1];

            // if at beginning of list we don't have a previous

            if ($key!=0){
                $searchNav['previous'] =$searchList[$key-1];
            } else {
                $searchNav['previous'] = null;
            }


            // if at end of list we don't have a next

            if ($key < count($searchList)-1){
                $searchNav['next'] = $searchList[$key+1];
            } else {
                $searchNav['next'] = null;
            }

            // get random element from list

            if (count($searchList) > 1) {
                $searchNav['random'] =  $searchList[array_rand($searchList)];
            } else {
                $searchNav['random'] = null;
            }
            $k = $key+1;
            $searchNav['counter'] = "( {$k} of ".count($searchList) .')';
            $searchNav['url'] = $searchURL;
        }


        // Get itinerary information (if any)

        $itin = \SOS\ItineraryCookie::get();

        $itin_artist = false;
        if (!empty($itin)){
            $itinObj = new \SOSModels\Itinerary($app['pdo']);
            $itinArtistIds =  $itinObj->getItinerayArtistIDsFromKey($itin['share_key']);

            if (! empty( $itinArtistIds) &&   in_array($id, $itinArtistIds)){
                $itin_artist = true;
            }
        }



        // GEt Artist Informtaion


        $artistProfile = new \SOSModels\ArtistData($app['pdo'], $id);


        $profile = $artistProfile->getArtistInfo();
        $images =  $artistProfile->getArtistImagesData(false);
        $artistThumbnailData = $artistProfile->getArtistThumbnailData();

        $jsonImages = json_encode($images);


        $socialMediaObj = new \SOSModels\ProfileSocialMedia($app['pdo']);
        $socialMedia = $socialMediaObj->getSocailMedia($id);

        $genreAsArray = \SOS\artistsGenreList::getGenreArray($profile);


        if (empty($artistProfile->artistProfile)){

            $app['session']->getFlashBag()->add('danger', 'Artist Profile Not Found for artist:'.$id);
            return $app->redirect($app["url_generator"]->generate("artists_directory",  array()));


        }


        $artistData=null;

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $artistsListObj->setSort('map_number');
        $mapNumber = $profile['map_number'];

        // get a marker on the map even if numbers have not been assigned yet
        if (empty($mapNumber)) {
            $artists = $artistsListObj->getByMemberId($id);
        } else {
            $artists = $artistsListObj->getByMapNumber($mapNumber);
        }

        $mapJson = $artistsListObj->listToJson($artists, "web/artists/artist_directory_list/display=list/medium=All/event=all/map_num=",  "artists/artist_profile/" ,  'target="_blank"');


        $mapSettings = array('bounds'=>'false',
            'showMapNumber'=> \SOSModels\Globals::$showMapNumber,
            'showSponsors'=>false,
            'showExhibits'=>false,
            'showTrolley'=>false,
            'initial_zoom'=>16,
            'lock_initial_zoom' => true,
            'initial_center'=>$profile['lat'].','.$profile['lng'] );

        // rraman: hacky way of appending trolleys onto artists! (copied from artist-directory)...
        $trolleysListObj = new \SOSModels\TrolleysList($app['pdo']);
        $trolleyItems = $trolleysListObj->getAll();
        $trolleyMapJson = $trolleysListObj->listToJson($trolleyItems/*, "/artists/artist_list.php?searchType=mapnum&mapnum=",  "artists/artist_profile/" ,  'target="_blank"'*/);
        $mapJson = json_decode($mapJson, true);
        $mapJson['markers'] = array_merge($mapJson['markers'], json_decode($trolleyMapJson, true)['markers']);
        $mapJson = json_encode($mapJson);

        // rraman: i think this is dead code that can be deleted (does what the code above does)
        // merge artist and trolley markers
        /*
        $trolleysListObj = new \SOSModels\TrolleysList($app['pdo']);
        $items = $trolleysListObj->getAll();
        $trolleyJson = $trolleysListObj->listToJson($items);
        $mapJsonDecoded = json_decode($mapJson, true);
        $trolleyJsonDecoded = json_decode($trolleyJson, true);
        $newMapJson = ['markers' => []];
        if (!empty($mapJsonDecoded['markers'])) {
            $newMapJson['markers'] = $mapJsonDecoded['markers'];
        }
        if (!empty($trolleyJsonDecoded['markers'])) {
            $newMapJson['markers'] = array_merge($newMapJson['markers'], $trolleyJsonDecoded['markers']);
        }
        $mapJson = json_encode($newMapJson);
        */

        $mapSettings = array('bounds'=>'false',
            'showMapNumber'=> \SOSModels\Globals::$showMapNumber,
            'showSponsors'=>false,
            'showExhibits'=>false,
            // 'showTrolley'=>false, // rraman: hardcoded to true in JS lib
            'initial_zoom'=>16,
            'lock_initial_zoom' => true,
            'initial_center'=>$profile['lat'].','.$profile['lng'] );
 
        // establish that we want trolley markers to auto-update with ajax:
        $mapSettings['ajaxURI'] = '/artists/artist_directory/trolleyajax/';
	
	
	
        // get events
        $eventObj = new \SOSModels\EventData($app['pdo'], $app['year']);
        $events = $eventObj->getPublicEventListing($id, 'Y');



        return $app['twig']->render('artists/artist_profile.html.twig',array('map_json'=>$mapJson,
            'artist_data'=>$profile,
            'itin_artist' => $itin_artist,
            'image_data'=> $images,
            'json_image_data'=>$jsonImages,
            'thumbnail_data'=>$artistThumbnailData,
            'map_settings'=> $mapSettings,
            'map_settings_json'=> json_encode($mapSettings),
            'search_nav' => $searchNav,
            'social_data' => $socialMedia,
            'search_name' => $searchName,
            'genre_list' =>$genreAsArray,
            'events' => $events,
            'main_menu' => $app['artistsMenu']));

        //return new Response("Actor Action respose (bar)".$id);


    }


    /**
     * Return the logged in users profile
     *
     * @param Request $request
     * @param Application $app
     * @return string
     *
     *
     */

    public function showMyProfile( Request $request, Application $app){

        $app['session']->getFlashBag()->add('danger', 'This is your user profile');


        $userInfo = $app['session']->get('sos_user_info');

        if (is_null($userInfo)){
            return "User Not Logged in";
        }

        $member_id= $userInfo ['id'] ;
       // $app['session']->set('searchList')=null;

        // clear sessions, because this is the artists user profile
        $app['session']->remove('searchList');
        $app['session']->remove('searchName');
        $app['session']->remove('searchURL');

        // create path and redirect;
        return $app->redirect($app["url_generator"]->generate("artists_profile",  array('id' => $member_id)));



    }

}
