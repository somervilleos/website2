<?php

namespace SOSControllers;
use SOS\SponsorshipRateData;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

use Silex\Application;
use Symfony\Component\Form\FormError;
class SponsorController {




    // List current sponsorhips

    public function list (Request $request, Application $app) {
        $app['request'] = $request;
        $year = \SOSModels\Globals::$sos_ad_year;

        $sponsorObj = new \SOSModels\SponsorshipData($app['pdo'], $year);
        $sponsorData = $sponsorObj->getSponsorData();
        $sponsorCategories = $sponsorObj->getOrderArray();


        return $app['twig']->render('support/sponsor_list.html.twig',array(
            'main_menu' => '',
            'sponsor_data'=>$sponsorData,
            'sponsor_year'=>$year,
            'cateorgies'=>$sponsorCategories));

        //return new Response("Actor Action respose (bar)".$id);


    }



    // List current sponsorhips

    public function listFoodAndDrink (Request $request, Application $app) {
        $app['request'] = $request;
        $year = \SOSModels\Globals::$sos_ad_year;

        $sponsorObj = new \SOSModels\SponsorshipData($app['pdo'], $year);
        $sponsorData = $sponsorObj->getSponsorData('food');
        $sponsorCategories = $sponsorObj->getOrderArray();


        return $app['twig']->render('support/sponsor_list_food.html.twig',array(
            'main_menu' => '',
            'sponsor_year'=>$year,
            'sponsor_data'=>$sponsorData,
            'cateorgies'=>$sponsorCategories));

        //return new Response("Actor Action respose (bar)".$id);


    }




    // List types of sponsorhips available

    public function listing (Request $request, Application $app) {
        $app['request'] = $request;


        $sponsorObj = new SponsorshipRateData();
        $sponsorData = $sponsorObj->getRateData();

        //var_dump ($sponsorData);

        $columns = array('name'=>'Name',
            'cost_text'=>'Cost',
        'description'=>'Description');

        return $app['twig']->render('support/sponsor.html.twig',array( 'main_menu' => '',
            'sponsor_data'=>$sponsorData, 'columns_to_show'=>$columns));

        //return new Response("Actor Action respose (bar)".$id);

    }


    public function purchase (Request $request, Application $app) {
        $app['request'] = $request;

        $sponsorObj = new SponsorshipRateData();

        $formObj = new \SOSForms\SponsorshipForm($app);
        $form = $formObj->getForm($app, $sponsorObj);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $formData = $form->getData();


            $formStatus = $formObj->processFormData ( $app['pdo'], $formData,  $app, $sponsorObj ) ;

            if (is_array($formStatus) && isset ($formStatus['pid']) && isset ($formStatus['hid'])) {
                //$app->match('payment/{pid}/{hid}', "SOSControllers\\PaymentController::makePayment")->bind('make_payment');
                // redirect to payment page..

                return $app->redirect($app["url_generator"]->generate("make_payment", array('pid' => $formStatus['pid'], 'hid' => $formStatus['hid'])));
            } else {

                $app['session']->getFlashBag()->add('info','Trouble sending data to payments page. Contact webmaster@ somervilleopenstudios.org.');

            }


        }


        return $app['twig']->render('support/sponsor_purchase.html.twig', array( 'main_menu' => '','form'=> $form->createView()
           ));

    }

}