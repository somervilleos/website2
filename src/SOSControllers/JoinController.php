<?php

namespace SOSControllers;
//use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;


/*'sos_join_user_info'
    array (size=9)
      'id' => string '592' (length=3)
      'EmailAddress' => string 'acomjean2@gmail.com' (length=19)
      'epassword' => string '342b9c30e9464068c4dcf69ead0f98b1' (length=32)
      'password' => string '$2y$10$VPDxTIeDEUiODTNDafPkAOWXuxHSycTGVv9yDmiNZUMNoJFcvr9J2' (length=60)
      'Active' => string 'N' (length=1)
      'MemberType' => string '2' (length=1)
      'Expanded_Web' => string 'N' (length=1)
      'Pending' => string 'Y' (length=1)
      'Pending_Date' => string '2015-09-28 21:16:46' (length=19)
*/


class JoinController {


    //----------------
    // Home Page
    //----------------

    public function index ( Request $request, Application $app){

        $app['request'] = $request;
        $app['session']->set('sos_email_send', null);
        $app['session']->remove('sos_email_send');


        if (isset ($app['registration_closed'])  && $app['registration_closed'] == true ){
            return $app['twig']->render('join/join_index_closed.html.twig', array(
                'login_ok'=> false,
                'main_menu' =>  ''));
        }


        $app['session.storage.options'] = [
            'cookie_lifetime' => 3600
        ];


        $infoArray = array();
        $warningArray = array();
        $loginOK = false;

        $form = $app['form.factory']->createBuilder(FormType::class)
            // ->setAction('login')
            ->setMethod('POST')
            ->add('username', TextType::class,  array('label' => 'Email',
                'required'=> true,
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
                'data' => '' ))
            ->add('password',  PasswordType::class , array('label' => 'Password',
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),

                'required'=> true))
            ->add('submit', SubmitType::class)
            ->getForm()
        ;

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();
                // check password

                $LoginService = new \SOS\LoginService($app['pdo'], $app);


                $loginCheck = $LoginService->checkLogin($formData['username'], $formData['password']);


                $loginOK = $loginCheck->result;

                if (!$loginOK) {
                    sleep(2);
                    $message = "login incorrect.";
                    $app['session']->getFlashBag()->add('danger', $message);
                    $app['session']->remove('sos_join_user_info');

                } else {

                    if ($loginCheck->memberRow['Active']=="Y") {

                        $app['session']->getFlashBag()->add('info', 'LOGIN SUCCESSFUL - Account already active. ');
                        $app['session']->set('sos_join_user_info', $loginCheck->memberRow);

                        return "Your account is active, no need to register.";

                    } elseif ($loginCheck->memberRow['Active']=="N" && $loginCheck->memberRow['Pending'] == "Y") {

                        $app['session']->set('sos_join_user_info', $loginCheck->memberRow);

                        $message = "Your membership has been received and is pending.  You should hear back from us soon";
                        $app['session']->getFlashBag()->add('danger', $message);

                        //
                        return $app->redirect($app["url_generator"]->generate("join_payment_select"));


                        // Forward to the Next Page
                    } else {
                        //$infoArray[] = " LOGIN SUCCESSFULL ";
                        $app['session']->getFlashBag()->add('info', 'LOGIN SUCCESSFUL ');
                        $app['session']->set('sos_join_user_info', $loginCheck->memberRow);

                        //return $app->redirect($app["url_generator"]->generate("join_contact_info",  array('event_name' => '2019_fashion_show')));
                        return $app->redirect($app["url_generator"]->generate("join_contact_info"));
                    }
                    // http://stackoverflow.com/questions/29952025/silex-session-set-a-lifetime
                    //$app['session']->migrate(false, 3600);
                }
            } else {
                $app['session']->getFlashBag()->add('info', 'The form is bound, but not valid');
            }
        }


        return $app['twig']->render('join/join_index.html.twig',array(
            'login_ok'=> $loginOK,
            'main_menu' =>  '',
            'form'  => $form->createView()));

        //return new Response("Actor Action respose (bar)".$id);

    }
    public function index_late ( Request $request, Application $app){

        $app['request'] = $request;
        $app['session']->set('sos_email_send', null);
        $app['session']->remove('sos_email_send');



        $app['session.storage.options'] = [
            'cookie_lifetime' => 3600
        ];


        $infoArray = array();
        $warningArray = array();
        $loginOK = false;

        $form = $app['form.factory']->createBuilder(FormType::class)
            // ->setAction('login')
            ->setMethod('POST')
            ->add('username', TextType::class,  array('label' => 'Email',
                'required'=> true,
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
                'data' => '' ))
            ->add('password',  PasswordType::class , array('label' => 'Password',
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),

                'required'=> true))
            ->add('submit', SubmitType::class)
            ->getForm()
        ;

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();
                // check password

                $LoginService = new \SOS\LoginService($app['pdo'], $app);


                $loginCheck = $LoginService->checkLogin($formData['username'], $formData['password']);


                $loginOK = $loginCheck->result;

                if (!$loginOK) {
                    sleep(2);
                    $message = "login incorrect.";
                    $app['session']->getFlashBag()->add('danger', $message);
                    $app['session']->remove('sos_join_user_info');

                } else {

                    if ($loginCheck->memberRow['Active']=="Y") {

                        $app['session']->getFlashBag()->add('info', 'LOGIN SUCCESSFUL - Account already active. ');
                        $app['session']->set('sos_join_user_info', $loginCheck->memberRow);

                        return "Your account is active, no need to register.";

                    } elseif ($loginCheck->memberRow['Active']=="N" && $loginCheck->memberRow['Pending'] == "Y") {

                        $app['session']->set('sos_join_user_info', $loginCheck->memberRow);

                        $message = "Your membership has been received and is pending.  You should hear back from us soon";
                        $app['session']->getFlashBag()->add('danger', $message);

                        //
                        return $app->redirect($app["url_generator"]->generate("join_payment_select"));


                        // Forward to the Next Page
                    } else {
                        //$infoArray[] = " LOGIN SUCCESSFULL ";
                        $app['session']->getFlashBag()->add('info', 'LOGIN SUCCESSFUL ');
                        $app['session']->set('sos_join_user_info', $loginCheck->memberRow);

                        //return $app->redirect($app["url_generator"]->generate("join_contact_info",  array('event_name' => '2019_fashion_show')));
                        return $app->redirect($app["url_generator"]->generate("join_contact_info"));
                    }
                    // http://stackoverflow.com/questions/29952025/silex-session-set-a-lifetime
                    //$app['session']->migrate(false, 3600);
                }
            } else {
                $app['session']->getFlashBag()->add('info', 'The form is bound, but not valid');
            }
        }


        return $app['twig']->render('join/join_index.html.twig',array(
            'login_ok'=> $loginOK,
            'main_menu' =>  '',
            'form'  => $form->createView()));

        //return new Response("Actor Action respose (bar)".$id);

    }

    /**
     * ------------------------------------------------------------------------------------
     * 2 UPDATE CONTACT
     * ------------------------------------------------------------------------------------
     * @param Request $request
     * @param Application $app
     * @return string
     */


    public function updateContactInfo(Request $request, Application $app)
    {


        $app['request'] = $request;

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }
        $app['session']->set('sos_join_user_info', $userInfo);

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistMemberInfo();

        $profileDataToShow = array('first name' => $profile['FirstName'],
            'Last name' => $profile['LastName'],
            'Street address' => $profile['HomeAddressOne'],
            'Street address2' => $profile['HomeAddressTwo'],
            'City' => $profile['HomeCity'],
            'State' => $profile['HomeState'],
            'Zip code' => $profile['HomeZip'],
            'Phone' => $profile['Phone'],
            'email' => $profile['EmailAddress']
        );


        // get form

        $formObj = new \SOSForms\ContactInformationForm();
        $form = $formObj->getForm($app, $profile);
        $formStatus = false;

        // Check form if submitted

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                // Send user to form to paypal or check
                $formStatus = $formObj->processFormData($app['pdo'], $formData, $app, $userInfo['id']);

                if ($formStatus == true) {
                    // reload profile with new information (it was just updated!)

                    $profile = $artistData->getArtistMemberInfo();
                    $form = $formObj->getForm($app, $profile);

                    $profileDataToShow = array('first name' => $profile['FirstName'],
                        'Last name' => $profile['LastName'],
                        'Street address' => $profile['HomeAddressOne'],
                        'Street address2' => $profile['HomeAddressTwo'],
                        'City' => $profile['HomeCity'],
                        'State' => $profile['HomeState'],
                        'Zip code' => $profile['HomeZip'],
                        'Phone' => $profile['Phone'],
                        'email' => $profile['EmailAddress']
                    );

                    return $app->redirect($app["url_generator"]->generate("join_membership_select"));
                }
            }
        }
        return $app['twig']->render('/join/join_update_contact.html.twig', array('form' => $form->createView(), 'form_status' => $formStatus, 'profile_data' => $profileDataToShow));
    }

    /**
     *-------------------------------------------------------------------------------------
     *  2  Select Membership Type
     *-------------------------------------------------------------------------------------
     * @param Request $request
     * @param Application $app
     * @return string
     */


    public function selectMembership(Request $request, Application $app)
    {


        $app['request'] = $request;
        $menu = $app['artistMenu'];


        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }
        $app['session']->set('sos_join_user_info', $userInfo);

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();

        // Check form if submitted
        if (isset($_POST['selection']) && !empty($_POST['selection'])){

            $membershipSelected = $_POST['selection'];

            // get all available memberships:
            $levels  =  \SOSModels\MembershipLevels::$membershipLevels;

            if (array_key_exists($membershipSelected,$levels)){

                $memberTypetoStore = $levels[$membershipSelected]['memberTypetoStore'];
                $Participating = $levels[$membershipSelected]['participating'];
                $extraMembership = $levels[$membershipSelected]['extraMembership'];

                $sql = sprintf("update member set  Participating= :Participating, MemberType =:MemberType $extraMembership WHERE id= :id LIMIT 1");


                $stmt = $app['pdo']->prepare ($sql);

                $stmt->bindParam(':Participating', $Participating , \PDO::PARAM_STR);
                $stmt->bindParam(':MemberType', $memberTypetoStore , \PDO::PARAM_INT);
                $stmt->bindParam(':id', $userInfo['id'] , \PDO::PARAM_INT);

                $stmt->execute();

                // If location is set here, we don't allow users to select one, so set it and skip the location select page.

                if (isset($levels[$membershipSelected]['setLocationTo']) && ! empty ($levels[$membershipSelected]['setLocationTo'])    ){

                    // save this data and go to page after the location page.
                    $sql = 'UPDATE `profile` SET `location_id` = :location WHERE `profile`.`member_id` = :member_id';

                    $stmt = $app['pdo']->prepare ($sql);
                    $stmt->bindParam(':location', $levels[$membershipSelected]['setLocationTo']  , \PDO::PARAM_INT);
                    $stmt->bindParam(':member_id', $userInfo['id'] , \PDO::PARAM_INT);

                    $stmt->execute();
                    return $app->redirect($app["url_generator"]->generate("join_community_space"));
                }

                return $app->redirect($app["url_generator"]->generate("join_location_info"));

            }
            //return $app->redirect($app["url_generator"]->generate("join_membership_select"));

        }

        return $app['twig']->render('/join/join_select_membership.html.twig', array(
        ));
    }


    public function communitySpace (Request $request, Application $app) {
        $app['request'] = $request;
        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        $errors=0;
        $error_string='';
        if (isset($_POST['doEdit']))
        {

            $choices = array();
            $choices[]=$_POST['choice1'];
            $choices[]=$_POST['choice2'];
            $choices[]=$_POST['choice3'];

            if (in_array( "none",$choices)){
                $errors++;
                $error_string="Must select 3 space choices<br>";
            }

            $result = array_unique($choices);

            if (count($result)!=count($choices)){
                $errors++;
                $error_string="Please select 3 DIFFERENT space choices<br>";
            }

            if (!(isset($_POST['readit']))){
                $errors++;
                $error_string.="Please Read and Acknowledge the description<br>";
            }

            $choiceString =  "1. {$_POST['choice1']}, ";
            $choiceString .= "2. {$_POST['choice2']}, ";
            $choiceString .= "3. {$_POST['choice3']}, ";

            if (isset($_POST['outlet'])){
                $choiceString .= "4. Outlet requested ,";
            } else {
                $choiceString .= "4. No outlet requested ,";
            }


            if ($errors==0){


                $sql ="UPDATE member m SET m.community_space_info = :info WHERE m.id = :member_id";
                $dbo = $app['pdo'];

                $stmt = $app['pdo']->prepare ($sql);

                $stmt->bindParam(':info', $choiceString , \PDO::PARAM_STR);
                $stmt->bindParam(':member_id', $userInfo['id'] , \PDO::PARAM_INT);

                $stmt->execute();

                return $app->redirect($app["url_generator"]->generate("join_profile_info"));



            } else {
                $error_string="<div class=\"alert alert-error\"><b> There were errors, please fix </b><br>{$error_string} </div>";
            }
        }



        return $app['twig']->render('/join/join_community_space_select.html.twig', array('errorString' => $error_string

        ));


    }





    /**
     *-------------------------------------------------------------------------------------
     *  2  UPDATE LOCATION
     *-------------------------------------------------------------------------------------
     * @param Request $request
     * @param Application $app
     * @return string
     */


    public function updateLocationInfo(Request $request, Application $app)
    {


        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }
        $app['session']->set('sos_join_user_info', $userInfo);

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();
        $locationID = $profile ['location_id'];

        if (empty($locationID)) {
            $locationID = 0;
        }
        $locationObj = new \SOSModels\Location($app['pdo']);
        $locationInfo = $locationObj->getLocation($locationID);


        // get form
        $formObj = new \SOSForms\LocationForm($app['pdo'], $locationObj);
        $form = $formObj->getForm($app, $profile, $locationInfo);
        $formStatus = false;

        $locationDataCols = array(
            'building_name' => 'Building Name',
            'street_number' => 'Street Number',
            'street_name' => 'Street Name',
            'street_address' => 'Street Address',
            'address_details' => 'Address Details');


        // Check form if submitted

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                $formStatus2 = $formObj->checkFormData($formData);


                if ($formStatus2['data_ok'] == false) {
                    $app['session']->getFlashBag()->add('danger', "Please Fix :". $formStatus2['message']);
                } else {

                    $formObj->processFormData($userInfo['id'], $formData, $app);


                    // reload profile with new information
                    $profile = $artistData->getAllArtistMemberView();
                    $locationID = $profile ['location_id'];

                    //$locationInfo = $locationObj->getLocation($locationID);

                    return $app->redirect($app["url_generator"]->generate("join_profile_info"));

                }
            }
        }


        return $app['twig']->render('/join/join_update_location.html.twig', array(
            'form' => $form->createView(),
            'form_status' => $formStatus,
            'location_id' => $locationID,
            'location_data' => $profile,
            'location_data_cols' => $locationDataCols,
        ));
    }


    /**
     *-------------------------------------------------------------------------------------
     *  3 MAIN Artists information
     *-------------------------------------------------------------------------------------
     *
     * @param Request $request
     * @param Application $app
     * @return string
     */

    public function updateProfileInfo (Request $request, Application $app)
    {


        $app['request'] = $request;
        $menu = $app['artistMenu'];


        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');
        if (is_null($userInfo)) {
            return "User not logged in";
        }
        $app['session']->set('sos_join_user_info', $userInfo);


        // Get information on logged in artist
        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();



        $profileDataToShow = array('First Name' => $profile['PublicFirstName'],
            //   'artist_statement'=>$profile['artist_statement'],
            'Last Name' => $profile['PublicLastName'],
            'Business Name' => $profile['BusinessName'],
            'Short Description' => $profile['ShortDescription'],
            'Genres' => \SOS\artistsGenreList::getGenreText($profile),
            //    'Organization Description' => $profile['OrganizationDescription'],
            'Use images for promotion' => isset($profile['use_images_for_promotion']) ? $profile['use_images_for_promotion'] : 'N',
            //       'home studio' => isset($profile['home_studio']) ? $profile['home_studio'] : 'N',
            //          'Friday Night' => isset($profile['FridayNight']) ? $profile['FridayNight'] : 'N',
            'Fashion Info' => isset($profile['FashionInfo']) ? $profile['FashionInfo'] : 'N',
            'Artist Statement' => $profile['statement']
        );


        // get form

        $formObj = new \SOSForms\ProfileForm();
        $form = $formObj->getForm($app, $profile);
        $profileFreeze = \SOSModels\Globals::$profileFreeze;

        // Check form if submitted

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $formData = $form->getData();

                $errors = 0;

                if (count($formData['genreList']) > 3) {
                    $app['session']->getFlashBag()->add('danger', 'Not Saved. You can only choose 3 genres');

                    $error = new FormError("You can only choose 3 genres");
                    $form->get('genreList')->addError($error);
                } else {

                    $dbo = $app['pdo'];
                    // Send user to form to paypal or check
                    $formStatus = $formObj->processFormData($dbo, $profile, $formData, $app, $userInfo['id']);

                    $artistObj = new \SOSModels\ArtistsGenreList($dbo);
                    $ynGenreList = $artistObj->createGenreYNArray($formData['genreList'], $formData['other'], $dbo);

                    $results2 = $artistObj->updateGenreData($ynGenreList, $profile, $userInfo['id'], $dbo);


                    // reload profile with new information
                    $profile = $artistData->getAllArtistMemberView(true);
                    $form = $formObj->getForm($app, $profile);

                    $profileDataToShow = array('First Name' => $profile['PublicFirstName'],

                        //   'artist_statement'=>$profile['artist_statement'],
                        'Last Name' => $profile['PublicLastName'],
                        'Business Name' => $profile['BusinessName'],
                        'Short Description' => $profile['ShortDescription'],
                        'Genres' => \SOS\artistsGenreList::getGenreText($profile),
                        'Organization Description' => $profile['OrganizationDescription'],
                        'Use images for promotion' => isset($profile['use_images_for_promotion']) ? $profile['use_images_for_promotion'] : 'N',
                        'Home studio' => isset($profile['home_studio']) ? $profile['home_studio'] : 'N',
                        'Friday Night' => isset($profile['FridayNight']) ? $profile['FridayNight'] : 'N',
                        'Fashion Info' => isset($profile['FashionInfo']) ? $profile['FashionInfo'] : 'N',
                        'Artist Statement' => $profile['statement']

                    );
                    return $app->redirect($app["url_generator"]->generate("join_social_media"));

                }
            }
        }


        return $app['twig']->render('/join/join_update_profile.html.twig', array('profile_data' => $profileDataToShow,
            'profile_freeze' =>$profileFreeze,
            'form' => $form->createView()));
    }

    /**
     *
     * ----------------------------------------------------------------------------------
     * SOCIAL MEDIA
     *
     *
     * @param Request $request
     * @param Application $app
     * @return string
     *
     *
     *
     */



    public function updateSocialMedia (Request $request, Application $app)
    {


        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();

        // get form

        $formObj = new \SOSForms\ProfileSocialMedia();
        $form = $formObj->getForm($app, $profile);
        $formStatus = false;

        // get existing profile information

        $member_id = $userInfo['id'];
        $socialMediaObj = new \SOSModels\ProfileSocialMedia($app['pdo']);
        $socialMediaData = $socialMediaObj->getSocailMediaWithBlanks($member_id);
        $socialMediaColumns = $socialMediaObj->getSocialMediaColumn();


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();


                // Send data to form for update.
                //$formStatus = $formObj->processFormData ( $app['pdo'], $formData,  $app, $userInfo['id']) ;

                // well in this case since...time.
                // send straight to social media object...

                $results = $formObj->processFormData($app['pdo'], $formData, $app, $member_id);


                // reload profile with new information (it was just updated!)

                $profile = $artistData->getAllArtistMemberView();
                $form = $formObj->getForm($app, $profile);
                $socialMediaData = $socialMediaObj->getSocailMediaWithBlanks($member_id);

                return $app->redirect($app["url_generator"]->generate("join_images_info"));


                return $app['twig']->render('/join/join_update_social_media.html.twig', array(
                    'form_status' => $formStatus,
                    'form' => $form->createView(),
                    'profile_data' => $profile,
                    'social_data' => $socialMediaData,
                    'social_data_columns' => $socialMediaColumns));


            }
        }
        return $app['twig']->render('/join/join_update_social_media.html.twig', array(
            'form' => $form->createView(),
            'form_status' => $formStatus,
            'profile_data' => $profile,
            'social_data' => $socialMediaData,
            'social_data_columns' => $socialMediaColumns));
    }





    public function updateImages (Request $request, Application $app)
    {

        $imageSets = array(1 => array(1, 2, 3), 4 => array(4, 5, 6, 7));

        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);

        $profile = $artistData->getArtistInfoAllArists();
        $imageData = $artistData->getArtistImagesData();


        // build a bunch of forms.. One per image

        $formArray = array();
        foreach ($imageData as $oneImageData) {
            $formObj = new \SOSForms\ImageForm();
            $key = $oneImageData['ImageNumber'];
            $formArray[$key] = $formObj->getForm($app, $oneImageData);

        }

        $formStatus = false;
        $uploaded = false;

        // Check form if submitted
        // loop through forms and update as needed

        foreach ($formArray as $oneForm) {

            $oneForm->handleRequest($request);
            if ($oneForm->isSubmitted()) {
                if ($oneForm->isValid()) {
                    if ($uploaded == false) {

                        $formData = $oneForm->getData();
                        $uploaded = true;

                        // Send user to form to paypal or check
                        $file = $oneForm['attachment']->getData();
                        $formStatus = $formObj->processFormData($app['pdo'], $formData, $file, $app, $userInfo['id']);

                        // reload profile with new information (it was just updated!)
                        $imageData = $artistData->getArtistImagesData();

                    }
                }
            }
        }

        // regen forms
        $formArray = array();
        foreach ($imageData as $oneImageData) {
            $formObj = new \SOSForms\ImageForm();
            $key = $oneImageData['ImageNumber'];
            $formArray[$key] = $formObj->getForm($app, $oneImageData);

        }

        foreach ($formArray as $key => $oneForm) {
            $formArray[$key] = $oneForm->createView();

        }


        return $app['twig']->render('/join/join_update_images.html.twig', array(
            'form_array' => $formArray,
            'form_status' => $formStatus,
            'image_data' => $imageData,
            'profile_data' => $profile));
    }





    public function updateThumbnail (Request $request, Application $app)
    {

        $imageSets = array(1 => array(1, 2, 3), 4 => array(4, 5, 6, 7));

        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfoAllArists();
        $thumbData = $artistData->getArtistThumbnailData();
        $imageData = $artistData->getArtistImagesData();



        if (!empty($_POST['image']) ) {

            if ($_POST['image'] != 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAYAAACI7Fo9AAABCElEQVR4nO3BAQEAAACCIP+vbkhAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwadG3AAEtdlNaAAAAAElFTkSuQmCC') {
                $data = $_POST['image'];


                list($type, $data) = explode(';', $data);

                list(, $data) = explode(',', $data);


                //$data = base64_decode($data);

                $saveFilename = $thumbData['path'] . $userInfo['id'] . '.jpg';

                $imagej = imagecreatefrompng('data://image/png;base64,' . $data);
                imagejpeg($imagej, $saveFilename, 100);

                $app['session']->getFlashBag()->add('info', 'thumbnail updated');
                return "true";
            } else {
                $app['session']->getFlashBag()->add('danger', 'Not Saved. image is blank');
                return "false";
            }
        }
        if (array_key_exists('rotateCCW', $_POST)){

// Load

            $filename = $thumbData['path'] . $userInfo['id'] . '.jpg';
            $source = imagecreatefromjpeg($filename);
// Rotate
            $rotate = imagerotate($source,90, 0);


            imagejpeg($rotate, $filename, 100);


        }
        if (array_key_exists('rotateCW', $_POST)){

// Load

            $filename = $thumbData['path'] . $userInfo['id'] . '.jpg';
            $source = imagecreatefromjpeg($filename);
// Rotate
            $rotate = imagerotate($source, 270, 0);


            imagejpeg($rotate, $filename, 100);


        }

        return $app['twig']->render('/join/join_update_thumbnails.html.twig', array(
            //'form' => $form->createView(),
            //'form_status' => $formStatus,
            'thumb_data' => $thumbData,
            'profile_data' => $userInfo));

    }


    /**********************************************************************************
     * showSummary
     *
     * @param Request $request
     * @param Application $app
     * @return string|\Symfony\Component\HttpFoundation\RedirectResponse
     */



    public function showSummary (Request $request, Application $app)
    {

        //https://stackoverflow.com/questions/29952025/silex-session-set-a-lifetime
        //$app['session']->migrate(false, 3600);
        $app['session.storage.options'] = [
            'cookie_lifetime' => 3600
        ];
        $x = session_get_cookie_params();
        //print "session value  ";
        $donation = $app['session']->get('donation');

        if (empty($donation)){
            $donation=0;
        }
        $form = $app['form.factory']->createBuilder(FormType::class)

            ->add('state', ChoiceType::class, array(
                'choices' => array ("not today"=> "0","$5" => "5", "$10"=>"10", "other"=>"other"),
                'data' => '0',
                'expanded' => true,

                'label' => 'consider adding a donation'
            ))

        ->add('other_amount', NumberType::class, array(
            'attr' => array('style'=> "width:200px"),
            'required'   => false,
        ))
            ->add('submit', SubmitType::class, [
                'label' => 'Add Donation',
            ])
            ->getForm();

        // Process donation in line...

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                if ($formData['state'] != "other"){
                    $donation = $formData['state'];
                } else {
                    $donation = intval($formData['other_amount']);
                }
                // set session
                if (empty($donation)){
                    $donation =0;
                }
                $app['session']->set('donation', $donation);

            }
        }





                $app['request'] = $request;

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        $app['session']->set('sos_join_user_info', $userInfo);

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();

        $columns = array(
            'FirstName' => 'First Name',
            'LastName' => 'Last Name',
            'HomeAddressOne' => 'AddressOne',
            'HomeAddressTwo' => 'AddressTwo',
            'HomeCity' => 'City',
            'HomeState' => 'State',
            'HomeZip' => 'Zip Code',
            'Phone' => 'Phone',
            'EmailAddress' => 'Email Address',

            'gap' => 'gap',
            'ShortDescription' => 'Short Art Description',
            'PublicLastName' => 'Public Last Name',
            'PublicFirstName' => 'Public First Name',
            'BusinessName' => 'Business Name'
        );


        // based on profile get the member type.

        $memberType =  $profile['MemberType'];
        $levels  =  \SOSModels\MembershipLevels::$membershipLevels;
        if (!array_key_exists($memberType,$levels)) {

            print "Something went wrong.   Please try again or contact website@somervilleopenstuios.org";
            exit();
        }

        if ($profile['community_space'] == "Y" && $memberType == 2){
            $memberType=3;
        }



        $membership = array('memnumber'=> $memberType, 'type' => $levels[$memberType]['name'], 'cost' => $levels[$memberType]['cost'] ,'donation'=> $donation, 'total'=>$levels[$memberType]['cost'] + $donation );



        return $app['twig']->render('/join/join_summary.html.twig', array(
                'form' => $form->createView(),

                'profile_data' => $profile,
                'profile_data_columns' => $columns,
                'membership' => $membership
            )
        );
    }


    /********************************************************
     * PAYMENT Page
     *
     * Mark Regstration as Pending.
     *
     *
     * @param Request $request
     * @param Application $app
     * @return string|\Symfony\Component\HttpFoundation\RedirectResponse
     */



    public function payment (Request $request, Application $app)
    {

        //https://stackoverflow.com/questions/29952025/silex-session-set-a-lifetime
        //$app['session']->migrate(false, 3600);
        $app['session.storage.options'] = [
            'cookie_lifetime' => 3600
        ];

        $app['request'] = $request;



        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        if ( empty ( $userInfo['id'])){
            return "User not logged in.";

        }

        $app['session']->set('sos_join_user_info', $userInfo);

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();

        // We've made it here, mark the membership as pending.
        $Pending_Date = date ("Y-m-d H:i:s");
        $memberType =  $profile['MemberType'];
        $participating =  $profile['Participating'];
        $communitySpace = $profile['community_space'];


        $levels  =  \SOSModels\MembershipLevels::$membershipLevels;

        if (!array_key_exists($memberType,$levels)) {

            print "Something went wrong.   Please try again or contact website@somervilleopenstuios.org";
            exit();
        }

        if ($profile['community_space'] == "Y" && $memberType == 2){
            $memberType=3;
        }

        $cost =$levels[$memberType]['cost'];
        $membershipDescription =$levels[$memberType]['name'];
        $donation = $app['session']->get('donation');

        $membership = array('type' => $levels[$memberType]['name'], 'cost' => $levels[$memberType]['cost'] ,'donation'=> $donation, 'total'=>$levels[$memberType]['cost'] + $donation );
        $sql = sprintf("update member set Pending= 'Y', Pending_Date= :Pending_Date WHERE id= :id LIMIT 1");

        $stmt = $app['pdo']->prepare ($sql);

        $stmt->bindParam(':Pending_Date', $Pending_Date , \PDO::PARAM_STR);
        $stmt->bindParam(':id', $userInfo['id'] , \PDO::PARAM_INT);

        $stmt->execute();


        //----------------------------------------------------------------------------------
        // Send email

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();

        $emailObj = new \SOS\EmailService();

        $cString = '';
        if ($communitySpace =='Y'){
            $cString = " Your membership includes Community Space. ";
        }

        $pString ='';
        if ($participating =='N'){
            $pString = " Your membership does not include the SOS event in May.  If thats wrong please contact membership. 
            
            ";
        }



        $message = <<<EOF

Thank you for registering for SOS {$app['registration_year'] }.  {$cString}

{$pString}

Once payment is received and your membership is approved you'll receive notification from us and you'll be able to log into your account and update your profile.  Notification should happen within a week of registering.

This is the email address [{$profile['EmailAddress']}] we use to contact you.  Please make sure it's one you check regularly. Don't miss important announcements!

Volunteer!  SOS runs on Volunteer Contributions.


Thank you, 




If you had any problems during the registration process, please contact
webmaster@somervilleopenstudios.org




EOF;



        // Block email if sent again.

        //$app['session']->remove('sos_email_send');

        $emailSentFlag = $app['session']->get('sos_email_send');

       if ($emailSentFlag != true) {

            $emailObj->sendEmail($profile['EmailAddress'],
                "{$app['registration_year'] } Somerville Open Studios Membership", $message, 'webmaster@somervilleopenstudios.org');
       }

        $app['session']->set('sos_email_send', true);


        $form = $app['form.factory']->createBuilder(FormType::class)
            ->setMethod('POST')
            ->add('submit', SubmitType::class, [
                'label' => 'Finish - Pay Online (preferred)',
                'attr'=> array('class'=>'btn-lg')
            ])
            ->add('pay_by_mail', SubmitType::class, [
                'label' => 'Finish - Pay By Mail (see note above)',
                'attr'=> array('class'=>'btn-lg')
            ])

            ->getForm();




        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                $postdata = $_POST['form'];
                if (array_key_exists("pay_by_mail", $postdata)){
                    $paymentType =  "pay_by_mail";
                    $paymentText = "[mail]";
                } else {
                    $paymentType =  "pay_by_paypal";
                    $paymentText = "";
                }


                $cString = "";
                if ($communitySpace =='Y'){
                    $cString = " + Com. Space ";
                }
                //---------------------------------------------------------------
                // lets create a payment record so that the payment can be made.

                $typePurchased = "Somerville Open Studios Membership #  {} ";

                $paymentObj = new \SOSModels\Payments($app['pdo']);
                $name = $profile['FirstName'] . ' ' . $profile['LastName'];
                $email = $profile['EmailAddress'];

                $zeropadMID = str_pad($userInfo['id'] , 4, "0", STR_PAD_LEFT);

                $blankPayment = $paymentObj->get_blank_payment($app['registration_year']);
                $blankPayment['item_description'] = " mid#" .$zeropadMID .'  [' . $memberType .'] '.$app['registration_year'].' '. $levels[$memberType]['paymentText'] . ' ' .$paymentText;

                if (empty($donation)){
                    $donation = 0;
                } else if ($donation > 0){
                    $blankPayment['item_description'] .= "+ {$donation} donation";
                }

                $blankPayment['payer_name'] = $name;
                $blankPayment['transaction_amount'] = $cost + $donation;
                $blankPayment['donation_amount'] = $donation;
                $blankPayment['payer_email'] = $email;
                $blankPayment['financial_year']= $app['registration_year'];
                $blankPayment['member_id'] =  $userInfo['id'];
                $blankPayment['type'] = 'membership';


                $paymentID = $paymentObj->add_payment($blankPayment);

                $hash = $paymentObj->getPaymentHash($paymentID);

                if (! empty ($paymentID) && ! empty ($hash)) {
                    //$app->match('payment/{pid}/{hid}', "SOSControllers\\PaymentController::makePayment")->bind('make_payment');
                    // redirect to payment page..
                    if ($paymentType=="pay_by_mail"){
                        return $app->redirect($app["url_generator"]->generate("join_pay_by_mail", array()));
                    } else {
                        return $app->redirect($app["url_generator"]->generate("make_payment", array('pid' => $paymentID, 'hid' => $hash)));
                    }
                } else {

                    $app['session']->getFlashBag()->add('info','Trouble storing payments info. Contact webmaster@ somervilleopenstudios.org.');

                }

            }
        }

        $profileData = null;

        //var_dump ($profile);

        return $app['twig']->render('/join/join_payment.html.twig', array(
                'form' => $form->createView(),
                'profile_data' => $profileData,
                'profile_data_columns' => $profileData,
                'membership' => $membership
            )
        );
    }


    /**----------------------------------------------------------------------------------------
     *  Financial Aid
     *
     * @param Request $request
     * @param Application $app
     * @return string
     */

    public function updateFinAid (Request $request, Application $app)
    {

        $app['request'] = $request;
        $menu = $app['artistMenu'];

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }


        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();
// Set the financial Aid flag to true when the form is visited

        $sql ="UPDATE member t SET t.financial_aid = 'Y' WHERE t.id = :member_id";
        $stmt = $app['pdo']->prepare ($sql);
        $stmt->bindParam(':member_id', $userInfo['id'] , \PDO::PARAM_INT);

        $stmt->execute();






        // get form

        $formObj = new \SOSForms\FinAidForm();
        $form = $formObj->getForm($app);
        $formProcessed = false;
        // get existing profile information

        $member_id = $userInfo['id'];


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                $formProcessed = true;


                $results = $formObj->processFormData($app['pdo'], $member_id, $formData, $app);
                $cost=0;

                // Make a Dummy payment

                $paymentObj = new \SOSModels\Payments($app['pdo']);
                $name = $profile['FirstName'] . ' ' . $profile['LastName'];
                $email = $profile['EmailAddress'];
                $zeropadMID = str_pad($userInfo['id'] , 4, "0", STR_PAD_LEFT);

                $blankPayment = $paymentObj->get_blank_payment($app['registration_year']);
                $blankPayment['item_description'] = "f mid#" . $zeropadMID .' '.$app['registration_year'] .' Somerville Open Studios membership ';
                $blankPayment['payer_name'] = $name;
                $blankPayment['transaction_amount'] = $cost;
                $blankPayment['donation_amount'] = 0;
                $blankPayment['payer_email'] = $email;
                $blankPayment['member_id'] =  $userInfo['id'];
                $blankPayment['type'] = 'membership';


                $paymentID = $paymentObj->add_payment($blankPayment);



                return $app['twig']->render('/join/join_fin_aid.html.twig', array(
                    'formProcessed'=> $formProcessed,
                    'form' => $form->createView(),
                ));


            }
        }
        return $app['twig']->render('/join/join_fin_aid.html.twig', array(
            'formProcessed'=> $formProcessed,
            'form' => $form->createView(),
        ));
    }


    public function joinPayByMail(Request $request, Application $app) {

        $app['request'] = $request;
        return $app['twig']->render('/join/join_pay_by_mail.html.twig', array(
        ));
    }


    public function joinNewUser(Request $request, Application $app) {

        $app['request'] = $request;

        $loginStatus = "";

        $form = $app['form.factory']->createBuilder(FormType::class)
            // ->setAction('login')
            ->setMethod('POST')
            ->add('username', TextType::class, array('label' => 'email address',
                'required' => true,
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2))),
                'data' => ''))
            ->add('password', PasswordType::class, array('label' => 'Password',
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 8))),
                'required' => true))
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                // check if email address is in database already
                $sql = "select id, EmailAddress from member where EmailAddress = :email";
                $stmt = $app['pdo']->prepare ($sql);
                $stmt->bindParam(':email', $formData['username'] , \PDO::PARAM_STR);
                $stmt->execute();
                $memberData = $stmt->fetchAll();

                if ( count($memberData) >0){
                  //  $app['session']->getFlashBag()->add('danger','Email Address ('. htmlspecialchars($formData['username'])   .') is  Already IN USE! please choose another, or reset your password');

                    $loginStatus = "duplicate";


                } else {


                    $LoginService = new \SOS\LoginService($app['pdo'], $app);

                    $ePass = $LoginService->generatePW($formData['password']);
                    $sql = "INSERT INTO member (FirstName, LastName, HomeAddressOne, HomeAddressTwo, HomeCity, HomeState, HomeZip, Phone, EmailAddress, Active, MemberType, Participating, Pending, Pending_Date, Expanded_Web, approval_date, reminder_email, epassword, password, epassword_reset, member_year, reset_key, reset_date, payment_id, checked, community_space, community_space_info, financial_aid, FridayNight, FashionInfo, BusinessNameOption, fashion_show, inside_out, first_look_show, art_week, temp_login, year_ok) VALUES ('', '', '', null, null, null, null, null, :email, 'N', 2, 'N', 'N', null, 'N', null, 'N', null, :pw, null, null, null, null, null, null, null, null, null, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, null, null, 0)";


                    $stmt =  $app['pdo']->prepare  ($sql);
                    $stmt->bindParam(':pw', $ePass , \PDO::PARAM_STR);
                    $stmt->bindParam(':email', $formData['username'] , \PDO::PARAM_STR);
                    $stmt->execute();
                    $member_id = $app['pdo']->lastInsertId();


                    $sql = "INSERT INTO profile (member_id, location_id, HandicapAccessible, Website, Books, Collage, Drawing, Fiber, Multimedia, Mixed_Media, Painting, Photography, Pottery, Furniture, Glass, Graphic, Installation, Jewelry, Printmaking, Sculpture, Video, Other_Media, Other, ShortDescription, PublicLastName, PublicFirstName, BusinessName, OrganizationDescription, address_details, use_images_for_promotion, home_studio, outdoor_display) VALUES (:mid, null, DEFAULT, null, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, null, null, null, null, null, '', DEFAULT, DEFAULT, DEFAULT, 'N')";

		    $sql="INSERT INTO `profile` (`profile_id`, `member_id`, `location_id`, `HandicapAccessible`, `Website`, `Books`, `Collage`, `Drawing`, `Fiber`, `Multimedia`, `Mixed_Media`, `Painting`, `Photography`, `Pottery`, `Furniture`, `Glass`, `Graphic`, `Installation`, `Jewelry`, `Printmaking`, `Sculpture`, `Video`, `Other_Media`, `Other`, `ShortDescription`, `PublicLastName`, `PublicFirstName`, `BusinessName`, `OrganizationDescription`, `address_details`, `outdoor_display`, `use_images_for_promotion`, `home_studio`) VALUES (NULL, :mid, NULL, 'N', NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, '', '', 'N', 'N', 'N'); ";

                    $stmt =  $app['pdo']->prepare  ($sql);
                    $stmt->bindParam(':mid', $member_id , \PDO::PARAM_STR);
                    $stmt->execute();

                    $sql = "INSERT INTO statement (member_id, statement, approved, bio) VALUES (:mid, null, DEFAULT, 0xEFBBBF62)";
                    $stmt =  $app['pdo']->prepare  ($sql);
                    $stmt->bindParam(':mid', $member_id , \PDO::PARAM_STR);
                    $stmt->execute();

                    $app['session']->set('sos_join_user_info', array('id'=>$member_id));
                    return $app->redirect($app["url_generator"]->generate("join_contact_info"));

                }
            }
        }
        return $app['twig']->render('/join/join_new_user.html.twig', array(
            'form' => $form->createView(),
            'loginStatus'=> $loginStatus,
            'login_ok'=>false
        ));

    }


    public function updateThumbnails(Request $request, Application $app) {


        $app['request'] = $request;

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // Get information on logged in artist

        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getArtistInfoAllArists();
        $thumbData = $artistData->getArtistThumbnailData();
        $imageData = $artistData->getArtistImagesData();


        if (!empty($_POST['image'])) {

            if ($_POST['image'] != 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAYAAACI7Fo9AAABCElEQVR4nO3BAQEAAACCIP+vbkhAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwadG3AAEtdlNaAAAAAElFTkSuQmCC') {
                $data = $_POST['image'];


                list($type, $data) = explode(';', $data);

                list(, $data) = explode(',', $data);


                //$data = base64_decode($data);

                $saveFilename = $thumbData['path'] . $userInfo['id'] . '.jpg';

                $imagej = imagecreatefrompng('data://image/png;base64,' . $data);
                imagejpeg($imagej, $saveFilename, 100);

                $app['session']->getFlashBag()->add('info', 'thumbnail updated');
                return "true";
            } else {
                $app['session']->getFlashBag()->add('danger', 'Not Saved. image is blank');
                return "false";
            }
        }
    }


    public function thanks (Request $request, Application $app) {
        $app['request'] = $request;

        return $app['twig']->render('/join/join_thank_you.html.twig', array(
            'login_ok'=>false
        ));


    }


    /**
     *
     * WHEN REGISTERING after you've already registered this page shows up.
     *
     * @param Request$request
     * @param Application $app
     * @return string
     */



    public function joinPaymentSelect ( Request $request, Application $app){

        $app['request'] = $request;

        //check if user logged in, if not redirect to login page
        $userInfo = $app['session']->get('sos_join_user_info');

        if (is_null($userInfo)) {
            return "User not logged in";
        }

        // lets get the user_data


        $artistData = new \SOSModels\ArtistData($app['pdo'], $userInfo['id']);
        $profile = $artistData->getAllArtistMemberView();


        $sql ="    SELECT * FROM `payments` WHERE `id` = :member_id AND `payment_confirmed` = 'N' AND `financial_year` =:year ORDER BY `payment_id` DESC limit 1";

        $stmt = $app['pdo']->prepare($sql);
        $stmt->bindValue(":member_id", $userInfo['id'], \PDO::PARAM_STR);
        $stmt->bindValue(":year", $app['registration_year'], \PDO::PARAM_STR);

        $stmt->execute();
        $paymentsDB = $stmt->fetchAll(\PDO::FETCH_ASSOC);


        if (!empty ($paymentsDB) ){

            $paymentsObj = new \SOSModels\Payments($app['pdo']);
            foreach ( $paymentsDB as $key=> $onePayment){
                // generate hashes for payment pages
                $hid = $paymentsObj->generateHash($onePayment);
                $paymentsDB[$key]['hid'] = $hid;
            }
        }

        $app['request'] = $request;


        return $app['twig']->render('/join/join_register_again.html.twig', array(
            'login_ok'=>false,
            'payments' =>$paymentsDB,
            'userData' => $profile
        ));


    }








 public function ogout( Request $request, Application $app){

        // http://api.symfony.com/master/Symfony/Component/HttpFoundation/Session/Session.html#remove%28%29

        $userId =$app['session']->get('sos_join_user_info');
        $app['session']->remove('sos_join_user_info');

        return "logged out";

    }


    //----------------
    // RESET REQUEST
    //----------------


    public function reset_request( Request $request, Application $app){
        $app['request'] = $request;
        $infoArray = array();
        $warningArray = array();
        $resetOK = false;

        $formObj = new \SOSForms\PasswordResetForm();
        $form = $formObj->getFormReset($app);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();
                // check password


                $formObj-> processResetForm($app, $formData);


            } else {
                //$warningArray[] = 'Form could not be processed, please try again';
                $app['session']->getFlashBag()->add('Form could not be processed, please try again');
                //$app['session']->getFlashBag()->add('info', 'The form is bound, but not valid');
            }
        }


        return $app['twig']->render('for_artists/password_reset_request.html.twig',array( 'reset_ok'=> $resetOK, 'main_menu' =>  '', 'form'  => $form->createView()));

        //return new Response("Actor Action respose (bar)".$id);

    }

}