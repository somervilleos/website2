<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/2/15
 * Time: 12:36 AM
 */

namespace SOSControllers;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class ForArtistsController {


    public function showPromotional (Request $request, Application $app) {

        $menu = $app['artistMenu'];
        $app['request'] = $request;

        $imageDir = 'web/images/graphics/';

        $promofiles = array('cover web'=>array('file'=>'2019SOS_CoverWeb.jpg','thumb'=>'2019SOS_CoverWeb.jpg'),
                        'postcard back'=>array('file'=>'2019SOSpostcard4x6back.jpg','thumb'=>'2019SOSpostcard4x6back.jpg'),
            'postcard front'=>array('file'=>'2019SOSpostcard4x6front.jpg','thumb'=>'2019SOSpostcard4x6front.jpg'),
            'site marker'=>array('file'=>'2019SOSsiteMarkerOL.pdf','thumb'=>'2019SOSsiteMarkerOL.png' ),
            'web baanner'=>array('file'=>'2019SOSwebannerWart.jpg','thumb'=>'2019SOSwebannerWart.jpg'),
            'logo color'=>array('file'=>'SOSLogoBlackYellow2019.jpg','thumb'=>'SOSLogoBlackYellow2019.jpg'),
            'logo'=>array('file'=>'2019SOSlogoBlackw.jpg','thumb'=>'2019SOSlogoBlackw.jpg')
    );



        return $app['twig']->render('/for_artists/promotion_materials.html.twig',array('promo_files'=> $promofiles,
                'side_menu' => $menu,
                'image_dir'=> $imageDir
            )
        );
    }

}