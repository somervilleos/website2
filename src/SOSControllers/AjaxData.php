<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/3/15
 * Time: 12:56 AM
 */

namespace SOSControllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;



class AjaxData {

    private $sos_dbo;





    public function completeStreetLocation($string,  Request $request, Application $app){

        $locationObj = new \SOSModels\Location($app['pdo']);
        $result = $locationObj->completeStreetName($string);

        $resultArray = [];
        foreach ($result as $oneResult){
            $resultArray[]= $oneResult['street_name'];
        }


        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        $response->setEncodingOptions(JSON_NUMERIC_CHECK);
        $response->setData($resultArray);

        return $response;



    }



    public function getSponsorJson( Request $request, Application $app){

        $sponsorObj = new \SOSModels\SponsorshipData($app['pdo'], \SOSModels\Globals::$sos_ad_year);

        $result = $sponsorObj->getSponsorData("all");



        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        $response->setEncodingOptions(JSON_NUMERIC_CHECK);
        $response->setData($result);

        return $response;


    }

    //----------------------------------------------
    // Get All Member Info
    //----------------------------------------------
// /data/get_all_members/

    public function getAllMemberInfo( Request $request, Application $app){



        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);

        $artistList = $artistsListObj->getAll();

        // This is a little wonky.   Loop through and get array of Genre names for each
        $artistsGenreObj = new \SOSModels\ArtistsGenreList ($app['pdo']);






        foreach ($artistList as $key=>$oneArtist){
            //print ($oneArtist['member_id'] . "<br>");


            $genresString = $artistsGenreObj->getGenreText($oneArtist);
            $artistList[$key]['genres_string'] = $genresString;
            $genresString = str_replace(' ', '', $genresString);
            $artistList[$key]['genres_array'] = explode(',', $genresString);


            $artistsDataObj = new \SOSModels\ArtistData($app['pdo'], $oneArtist['member_id']);
            $imageData = $artistsDataObj->getArtistImagesData(false);
            $thumbData = $artistsDataObj->getArtistThumbnailData();

            $artistList[$key]['image_data'] = $imageData;
            $artistList[$key]['thumb_data'] = $thumbData;



        }
/*
        print "<pre>";
        var_dump ($_SERVER);
        var_dump ($artistList);
        print "</pre>";
*/


        $metadata= array();
        $metadata['base_url'] = $_SERVER['HTTP_HOST'];

        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        $response->setEncodingOptions(JSON_NUMERIC_CHECK);
        $response->setData(array('meta_data'=>$metadata,  'artist_data'=>$artistList));

        return $response;
    }




    //----------------------------------------------
    // Get Thumbnail Info
    //----------------------------------------------

    public function getMemberRandomThumbnails( $count, Request $request, Application $app){

        $artistsListObj = new \SOSModels\ArtistsList($app['pdo']);
        $randomArtists = $artistsListObj->getRandom(12);

        $thumb_dir='artist_files/artist_images/splash/';

        // check if image files exist..  Only show files that exist so collect artists that have images
        $imageCount = 0;
        $imageMax = $count;
        $artistArray = array();


        foreach ($randomArtists as $key=>$oneArtist ){
            if ($imageCount < $imageMax && file_exists($thumb_dir."/".$oneArtist['id'].".jpg" )){
                $artistArray[]= $oneArtist;
                $imageCount++;
            }
        }






        $response = new \Symfony\Component\HttpFoundation\JsonResponse();
        $response->setEncodingOptions(JSON_NUMERIC_CHECK);
        $response->setData($artistArray);

        return $response;


    }



}