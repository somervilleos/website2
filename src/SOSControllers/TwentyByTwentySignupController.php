<?php

namespace SOSControllers;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Application;
use Symfony\Component\Form\FormError;


class TwentyByTwentySignupController {



    public function signup ( Request $request, Application $app){
        $supportMenu = array('SUPPORT'=>'support.home','SPONSOR' =>'support.sponsor','VOLUNTEER'=>'volunteer.signup','DONATE'=>'support.donate');

        $app['request'] = $request;

        //echo $id ."<br>";
        //  $artistProfile = new \SOSModels\ArtistData($app['pdo'], $id);

        //$profile = $artistProfile->getArtistInfo();
        //$images =  $artistProfile->getArtistImagesData();
        //$artistData=null;

        $form = \SOSForms\TwentyByTwentyForm::getFrom($app);


        // Check form if submitted


        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $formData = $form->getData();

                // Send user to form to paypal or check

                $formResult = \SOSForms\TwentyByTwentyForm::processFromData ( $app['pdo'], $formData,  $app);

                // create payment and forward to it.
                $paymentObj = new \SOSModels\Payments($app['pdo']);
                /*$blankpayment = array (
                    'member_id'=>'202020',   //member_id
                    'date_posted'=> $currentDate,
                    'type'=> '',
                    'transaction_date'=> $currentDate,
                    'transaction_amount'=> 0,
                    'payment_info'=> '',
                    'payment_confirmed'=> 'N',
                    'item_description'=> '',
                    'payment_type'=> '',
                    'payer_name'=> '',
                    'payer_email'=> '',
                    'donation_amount'=> '',
                    'financial_year'=> $year,
                    'pay_later'=> 'N'


                );
*/

                $name = $formData['first_name'].' '. $formData['last_name'];
                $email = $formData['email'];

                $blankPayment = $paymentObj->get_blank_payment();
                $blankPayment['item_description']= "20x20 registration - (".$formData['email'] .")" ;
                $blankPayment['payer_name']= $name;
                $blankPayment['transaction_amount'] = 20;

                $paymentid = $paymentObj->add_payment($blankPayment);


                $hash = $paymentObj->getPaymentHash($paymentid);

                $url = $app['url_generator']->generate('make_payment', array('pid'=> $paymentid , 'hid'=>$hash));

                // send email
                $emailObj = new \SOS\EmailService();

                $messageToSend = <<<EOT

Dear {$name},

Thank you for registering for Somerville Open Studios  Twenty by Twenty Fundraiser 2018 .
It will all take place on Saturday, March 31, 2018, 1-5 pm, at Mad Oyster Studios, 2 Bradley Street, Somerville, MA.

Questions let us know : twentyby20@somervilleopenstudios.org


Christina Tedesco


EOT;

                $subject = "Twenty by Twenty Fundraiser 2018";
                $from = "twentyby20@somervilleopenstudios.org";
                $emailObj->sendEmail($email, $subject, $messageToSend, $from);


                return $app->redirect($url);
                //return $app['twig']->render('/support/payment_confirm20.html.twig',array('main_menu' => $supportMenu, 'formdata'=>$formData));


            }
        }

        $supportMenu ='';
        return $app['twig']->render('/support/twenty_by_twenty.html.twig',array('main_menu' => $supportMenu, 'form'=>$form->createView()));
    }


}