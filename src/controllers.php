<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

#use SOSControllers;
#use SOSControllers\VolunteerController;
//$app->register(new Silex\Provider\SessionServiceProvider());

// HOMEPAGE
$app->match('/',  "SOSControllers\\HomePage::index")->bind('homepage');



$userInfo = $app['session']->get('sos_user_info');
if (!is_null($userInfo)){
    //var_dump ($userInfo);
    $app['username']= $userInfo ['EmailAddress'] ;
}

//------------------------------
// JOIN
//------------------------------


$app->match('/join',  "SOSControllers\\JoinController::index")->bind('join_homepage');
//$app->match('/join_late',  "SOSControllers\\JoinController::index_late")->bind('join_late_homepage');
$app->match('/join_late24',  "SOSControllers\\JoinController::index_late")->bind('join_late_homepage');


$app->match('/join_contact_info',  "SOSControllers\\JoinController::updateContactInfo")->bind('join_contact_info');
$app->match('/join_membership_select',  "SOSControllers\\JoinController::selectMembership")->bind('join_membership_select');
$app->match('/join_location_info',  "SOSControllers\\JoinController::updateLocationInfo")->bind('join_location_info');

$app->match('/join_community_space',  "SOSControllers\\JoinController::communitySpace")->bind('join_community_space');


$app->match('/join_profile_info',  "SOSControllers\\JoinController::updateProfileInfo")->bind('join_profile_info');
$app->match('/join_social_media',  "SOSControllers\\JoinController::updateSocialMedia")->bind('join_social_media');
$app->match('/join_images_upload',  "SOSControllers\\JoinController::updateImages")->bind('join_images_info');

$app->match('/join_images_thumbnail_info',  "SOSControllers\\JoinController::updateThumbnail")->bind('join_thumbnail_location_info');
$app->match('/join_preferences',  "SOSControllers\\JoinController::joinPreferences")->bind('join_preferences');

$app->match('/join_summary',  "SOSControllers\\JoinController::showSummary")->bind('join_summary');

$app->match('/join_payment',  "SOSControllers\\JoinController::payment")->bind('join_payment');
$app->match('/join_payment/',  "SOSControllers\\JoinController::payment")->bind('\|
join_payment');   


$app->match('/join_financial_aid',  "SOSControllers\\JoinController::updateFinAid")->bind('join_financial_aid');
$app->match('/join_new_user',  "SOSControllers\\JoinController::joinNewUser")->bind('join_new_user');
$app->match('/join_update_thumbnails',  "SOSControllers\\JoinController::updateThumbnails")->bind('join_update_thumbnail');

$app->match('/thank_you',  "SOSControllers\\JoinController::thanks")->bind('thanks');
$app->match('/join_payment_select',  "SOSControllers\\JoinController::joinPaymentSelect")->bind('join_payment_select');

$app->match('/join_pay_by_mail',  "SOSControllers\\JoinController::joinPayByMail")->bind('join_pay_by_mail');

$app->match('/iframe5',  "SOSControllers\\HomePage::iframe5")->bind('iframe5');
$app->match('/iframe5slick',  "SOSControllers\\HomePage::iframe5slick")->bind('iframe5slick');

$app->match('/refund2020',  "SOSControllers\\RefundPage1::refund")->bind('refund2020_o');
$app->match('/refund2020/{member_id}/{hid}',  "SOSControllers\\RefundPage::refund")->bind('refund2020');


$app->match('/fashion',  function (Request $request) use ($app) {
    return $app->redirect($app["url_generator"]->generate("show_event_page",  array('event_name' => '2019_fashion_show')));
})->bind('fashion_short_link');



$app->match('/example',function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('example_sidebar.html.twig');
})->bind('example');


//--------------------
// API / Ajax Data


$app->match('api/get_street_names/{string}',  "SOSControllers\\AjaxData::completeStreetLocation")->bind('api_get_street_names');
$app->match('web/api/get_street_names/{string}',  "SOSControllers\\AjaxData::completeStreetLocation")->bind('web_api_get_street_names');

//----------------------------
// Event Pages
//----------------------------
//'EVENTS'=>'visit.events'
$visitMenu = array('HOME'=> 'homepage', 'Visitor'=>"visit.home",'Artist Directory'=>'artists_directory','Getting Around'=>'visit.directions_parking','Visitor FAQS'=>'visit.faq', 'Other Art'=>'visit.see_other_art' ,'Calendar'=>'visit.calendar');


$app->match('/event/{event_name}',  "SOSControllers\\EventsController::showEventPage")->bind('show_event_page');



//----------------------------
// Visitor Pages
//----------------------------
// Visit sidebar menu



$visit = $app['controllers_factory'];

$visit->get('/', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;

    return $app['twig']->render('visit/index.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.home');

$visit->get('/artists_list', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;

    return $app['twig']->render('visit/artist_list.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.artist_listing');

$visit->get('/events', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;

    return $app['twig']->render('visit/visitor_events.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.events');

$visit->get('/FAQ', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;
    return $app['twig']->render('visit/faq.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.faq');



$visit->get('/directions_parking', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;
    return $app['twig']->render('visit/directions_parking.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.directions_parking');



$visit->get('/visitor_calendar', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;
    return $app['twig']->render('visit/calendar.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.calendar');

$visit->get('/see_other_art', function (Request $request) use ($app, $visitMenu) {
    $app['request'] = $request;
    return $app['twig']->render('visit/see_other_art.html.twig',array('side_menu' => $visitMenu));
})->bind('visit.see_other_art');


$app->mount('/visit', $visit);



//---------------------------
// Artist Profile/ Artist Directory
//---------------------------
$app['artistsMenu'] = array("ARTISTS DIRECTORY"=>'artists_directory');

$app->match('artists/get_random_thumb', "SOSControllers\\ArtistProfileController::getRandomThumb")->bind('get_random_thumb');



$app->match('artists/artist_profile/{id}', "SOSControllers\\ArtistProfileController::showProfile")->bind('artists_profile');
$app->match('artists/artist_directory', "SOSControllers\\ArtistsDirectoryHomeController::generatePage")->bind('artists_directory');


//random
$app->match('artists/artist_profile_random', "SOSControllers\\ArtistsDirectoryController::randomProfile")->bind('artists_profile_random');




// NEW !!
//  http://127.0.0.1:8888/artists/home/display=list/medium=Collage/event=All/map_num=All


$app->match('artists/home/display={display_type}/medium={genre}/event={event}/map_num={map_number}/type={type}', "SOSControllers\\ArtistsDirectoryController_2::home")
    ->bind('artists_directory_list_new');
/*
 *
    ->value('display_type', '10')
    ->value('genre', '10')
    ->value('event', '10')
    ->value('map_numbe3r', '10')
 *
 *
 */
//  http://127.0.0.1:8888/artists/home/display=list/medium=Collage/event=All/map_num=All

$app->get('/trolley', function (Request $request) use ($app) {
        return $app->redirect('artists/artist_directory/trolley/');
})->bind('trolley_tracker343');




$app->get('/ARAM/{pageName}', function ($pageName) {
    print "$pageName";
})
    ->value('pageName', 'index');





$app->match('artists/artist_directory/random/{type}', "SOSControllers\\ArtistsDirectoryController::showRandom")->bind('artists_directory_random');

$app->match('artists/artist_directory/search/{search}/{display_type}', "SOSControllers\\ArtistsDirectoryController::showListbySearchTerm")
    ->value('display_type', 'list')
    ->bind('artists_directory_search');


$app->match('artists/artist_directory_list/display={display_type}/medium={genre}/event={event}/map_num={map_number}/display_order={display_order}', "SOSControllers\\ArtistsDirectoryController::showArtists3")->bind('artists_directory_list');

//{# Not used ?
$app->match('artists/artist_directory_list2/display={display_type}/medium={genre}/event={event}/map_num={map_number}', "SOSControllers\\ArtistsDirectoryController::showArtists22")->bind('artists_directory_list2');


// rraman: experimentation with trolley map
$app->match('artists/artist_directory/trolley/', function (Request $request) use ($app) {
    return $app->redirect('/web/artists/artist_directory_list/display=map/medium=All/event=1030/map_num=All/display_order=default?mapFilter=TROLLEYS');
})->bind('artists_directory_list_trolley');

//)

$app->match('artists/artist_directory/trolleylog/', "SOSControllers\\ArtistsDirectoryController::trolleyLog")->bind('artists_directory_list_trolleylog');
$app->match('artists/artist_directory/trolleyajax/', "SOSControllers\\ArtistsDirectoryController::trolleyAjax")->bind('artists_directory_list_trolleyajax');

$app->match('artists/artist_directory/itinerary/{type}', "SOSControllers\\ArtistsDirectoryController::showItinerary")->bind('artists_directory_itin');



// Itinerary

$app->match('artists/artist_directory/manage_itinerary', "SOSControllers\\ItineraryController::manage")->bind('manage_itin');


$app->match('artists/artist_directory/set_itinerary/{share_key}/{edit_key}', "SOSControllers\\ItineraryController::set")->bind('set_itinerary');



$app->match('/itin/command={command}&id={id}',  "SOSControllers\\ItineraryController::process")->bind('itin_process');



$app->match('/itin_remove/command={command}&id={idToAdd}',  "SOSControllers\\ItineraryController::add")->bind('itin_remove');





//---------------------------
// Artist Profile/ Artist Directory
//---------------------------
$app['artistsMenu'] = array("ARTISTS DIRECTORY"=>'artists_directory');

$app->match('artists/artist_profile2/{id}', "SOSControllers\\ArtistProfileController::showProfile")->bind('artists_profile2');







//----------------------------
// FOR Artists
//----------------------------
//'REGISTRATION'=>'artists.registration'



if (!is_null($userInfo)){

    $app['artistMenu']= array('General Information'=>'for_artists.home',
        'My Profile'=>'artists.my_profile',
        'Events'=>"artists.events",
        ' + Virtual Gallery Info' => 'virtual_gallery_info',
        ' + Live Streaming Info' => 'live_stream_info',
        ' + Locavore Show' => 'locavore_home_show',
        ' + Inside Out Show' =>'inside_out_show',
        ' + First Look Show"' =>'first_look_show',
        ' + Fashion Show' =>'fashion_show',
        'Artist Tips' =>'artist_tips',
        ' + Home Artist Tips' =>'artist_tips_home',
        ' + Neighbor Letter' =>'artist_tips_letter',
        'Promotional Materials' =>'promotion_materials',
        'Calendar'=>'artists.calendar',
        'Logout'=>'login',
        'Community Space'=>'community_space',
        'Show My Profile'=>'for_artists.show_my_profile');

} else {
    $app['artistMenu']= array(

        'LOGIN'=>'login',
        'General Information'=>'for_artists.home',
        'My Profile'=>'artists.my_profile',
        'Events'=>"artists.events",
        ' + Virtual Gallery Info' => 'virtual_gallery_info',
        ' + Live Streaming Info' => 'live_stream_info',
  //      ' + Locavore Show' => 'locavore_home_show',
        ' + Inside Out Show' =>'inside_out_show',
//        ' + First Look Show' =>'first_look_show',
        ' + Small Works @ Bow Str' =>'small_works_at_bow_market_show',
        ' + Fashion Show' =>'fashion_show',
        'Artist Tips' =>'artist_tips',
        ' + Home Artist Tips' =>'artist_tips_home',
        ' + Neighbor Letter' =>'artist_tips_letter',
        'Promotional Materials' =>'promotion_materials',
        'Calendar'=>'artists.calendar',

        'Community Space'=>'community_space');
}

$artist = $app['controllers_factory'];
$artist->get('/', function (Request $request) use ($app){

    $userInfo =$app['session']->get('sos_user_info');
    $app['request'] = $request;

    //$tempMenu = \SOSModels\Menu::$artistMenu;

    return $app['twig']->render('for_artists/index.html.twig',array('side_menu' => $app['artistMenu'], 'user_info'=>$userInfo));
})->bind('for_artists.home');


$artist->match('/login', "SOSControllers\\LoginController::login")->bind('login');
$artist->match('/login/{uid}/{hash}', "SOSControllers\\LoginController::autologin")->bind('autologin');
$artist->match('/logout', "SOSControllers\\LoginController::logout")->bind('logout');
$artist->match('/update_password?uid={uid}&hid={hash}', "SOSControllers\\LoginController::passwordUpdate")->bind('password_update');
$artist->match('/update_password/{uid}/{hash}', "SOSControllers\\LoginController::passwordUpdate")->bind('password_update');
$artist->match('/reset_request', "SOSControllers\\LoginController::reset_request")->bind('password_reset_request');
$artist->match('/my_profile', "SOSControllers\\ProfileUpdateController::my_profile")->bind('artists.my_profile');
$artist->match('/show_my_profile', "SOSControllers\\ArtistProfileController::showMyProfile")->bind('for_artists.show_my_profile');

$artist->match('/promotion_materials', "SOSControllers\\ForArtistsController::showPromotional")->bind('promotion_materials');

$artist->match('/update_contact_information', "SOSControllers\\ProfileUpdateController::updateContactInfo")->bind('update_contact_info');


//updates


$artist->match('/update_event_selection', "SOSControllers\\ProfileEventPage::eventUpdate")->bind('event_selection');
$artist->match('/update_location', "SOSControllers\\ProfileUpdateController::updateLocation")->bind('update_location');
$artist->match('/update_artist_statement', "SOSControllers\\ProfileUpdateController::updateArtistStatement")->bind('update_artist_statement');

$artist->match('/update_profile', "SOSControllers\\ProfileUpdateController::updateProfileInfo")->bind('update_profile_info');
$artist->match('/update_artist_info', "SOSControllers\\ProfileUpdateController::updateArtistInfo")->bind('update_artist_info');
$artist->match('/update_social_media', "SOSControllers\\ProfileUpdateController::updateSocialMedia")->bind('update_social_media');

$artist->match('/update_images', "SOSControllers\\ProfileUpdateController::updateImages")->bind('update_images');
$artist->match('/update_thumbnails', "SOSControllers\\ProfileUpdateController::updateThumbnails")->bind('update_thumbnails');

$artist->get('/registration_thank_you', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/registration_thank_you.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.registration_thank_you');


$artist->get('/registration', function () use ($app) {
    return $app['twig']->render('for_artists.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.registration');

$artist->get('/events', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/events.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.events');

$artist->get('/calendar', function (Request $request) use ($app) {
    $app['request'] = $request;

    return $app['twig']->render('/for_artists/artist_calendar.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.calendar');


$artist->get('/community_space', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/community_space.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('community_space');

$artist->get('/fashion_show', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/show_fashion.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('fashion_show');


$artist->get('/first_look_show', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/show_first_look.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('first_look_show');

$artist->get('/small_works_at_bow_market_show', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/show_bow_street_market.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('small_works_at_bow_market_show');


$artist->get('/locavore_home_show', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/show_locavore_home.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('locavore_home_show');


$artist->get('/inside_out_show', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/show_inside_out.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('inside_out_show');


$artist->get('/virtual_gallery_info', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/virtual_gallery_info.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('virtual_gallery_info');


$artist->get('/live_stream_info', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/live_stream_info.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('live_stream_info');





$artist->get('/FAQ', function () use ($app) {
    return $app['twig']->render('for_artists.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artists.faq');


$artist->get('/artist_tips', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/artist_tips.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artist_tips');


$artist->get('/artist_tips_home', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/artist_tips_home.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artist_tips_home');


$artist->get('/artist_tips_letter', function (Request $request) use ($app) {
    $app['request'] = $request;
    return $app['twig']->render('/for_artists/artist_tips_letter.html.twig',array('side_menu' => $app['artistMenu']));
})->bind('artist_tips_letter');



$app->mount('/for_artists', $artist);

//----------------------------
// Support
//----------------------------

$support = $app['controllers_factory'];
$supportMenu = array('HOME'=> 'homepage','SUPPORT'=>'support.home','SPONSOR' =>'support.sponsor','VOLUNTEER'=>'volunteer.signup','DONATE'=>'support.donate');

$support->get('/', function (Request $request) use ($app, $supportMenu){
    $app['request'] = $request;
    return $app['twig']->render('/support/supporters.html.twig',array('side_menu' => $supportMenu));
})->bind('support.home');

$support->get('/sponsor',  "SOSControllers\\SponsorController::listing")->bind('support.sponsor');
$support->match('/sponsor_purchase',  "SOSControllers\\SponsorController::purchase")->bind('support.sponsor_purchase');
$support->match('/sponsor_list',  "SOSControllers\\SponsorController::list")->bind('support.sponsor_list');
$support->match('/sponsor_list_simple',  "SOSControllers\\SponsorController::list_simple")->bind('support.sponsor_list_simple');


$support->match('/sponsor_list_food',  "SOSControllers\\SponsorController::listFoodAndDrink")->bind('support.sponsor_list_food');



$support->match('/donate',"SOSControllers\\DonationController::donate")->bind('support.donate');






$app->mount('/support', $support);


$app->match('/data/random_thubmnails/{count}',"SOSControllers\\AjaxData::getMemberRandomThumbnails")->bind('getMemberRandomThumbnails.ajax_data');

$app->match('/data/get_all_members/',"SOSControllers\\AjaxData::getAllMemberInfo")->bind('getall_members.ajax_data');


$app->match('/data/ads_json',"SOSControllers\\AjaxData::getSponsorJson")->bind('support.ajax_data');






//----------------------------
// About
//----------------------------

$about = $app['controllers_factory'];
$app['aboutMenu'] = array('HOME'=> 'homepage', 'ABOUT SOS'=>'about.home','PRESS INFO'=>'about.press_info','NEWS'=>'about.in_the_news', 'SOS IN EDUCATION'=> 'about.education' ,  'HISTORY'=> 'about.history', 'BUDGET'=>'about.sos_budget' ,'CONTACT US'=>'about.contact');

$about->get('/', function (Request $request) use ($app){
    $app['request'] = $request;

    return $app['twig']->render('about/about.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.home');


$about->get('/sos_history', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/history.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.history');

$about->get('/sos_budget', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/sos_budget.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.sos_budget');

$about->get('/sos_in_education', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/education.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.education');

$about->get('/press_info', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/press_info.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.press_info');

$about->get('/contact', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/contact.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.contact');


$about->get('/2020_board_call', function (Request $request) use ($app){
    $app['request'] = $request;
    return $app['twig']->render('about/2020_board_call.html.twig',array('side_menu' => $app['aboutMenu'] ));
})->bind('about.2020_board_call');





/*

$about->get('/in_the_news', function () use ($app, $aboutMenu){
    return $app['twig']->render('about/in_the_news.html.twig',array('side_menu' => $aboutMenu));
})->bind('about.in_the_news');
*/

$about->get('/in_the_news',"SOSControllers\\InTheNewsController::listnews")->bind('about.in_the_news');

$app->mount('/about', $about);

//----------------------------
// Volunteer
//----------------------------

$volunteer = $app['controllers_factory'];

$app['volunteerMenu'] = array('HOME'=> 'homepage','ABOUT VOLUNTEERING'=>'volunteer.home', 'VOLUNTEER'=>'volunteer.signup');

$volunteer->get('/', function (Request $request) use ($app){
    $app['request'] = $request;

    return $app['twig']->render('volunteer/index.html.twig',array('side_menu' =>$app['volunteerMenu']));
})->bind('volunteer.home');


$volunteer->get('/thanks', function (Request $request) use ($app){
    $app['request'] = $request;

    return $app['twig']->render('volunteer/thanks.html.twig',array('side_menu' =>$app['volunteerMenu']));
})->bind('volunteer.thanks');


$volunteer->match('/signup/', "SOSControllers\\VolunteerController::signup")->bind('volunteer.signup') ;

$app->mount('/volunteer', $volunteer);


//--------------------------------------
// Paymnet

$app->match('payment/{pid}/{hid}', "SOSControllers\\PaymentController::makePayment")->bind('make_payment');

//--------------------------------------
// 2020 Market


$app->match('/2020_art_market', "SOSControllers\\ArtistMarketController::artMarket")->bind('about.2020_art_market');



//-----------------------------------------
// Admin
$app->match('admin_twenty', "SOSControllers\\AdminTwentyController::showTwenty")->bind('twentyAdmin');

$app->error(function (Exception $e, Request $request, $code) use ($app) {

    if ($app['debug']) {
       return;
    }


    $theCode = substr($code, 0, 3);

    switch ($theCode) {
        case 404:
            $message = 'The requested page could not be found. <p> <a href="/"> try looking from the home page</a>';
            break;
        default:
            $message = 'We are sorry, but something went wrong.';
    }

    // someday we'll make a good 404 page.. but for now..
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($message, $code);
    //return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});



return $app;
