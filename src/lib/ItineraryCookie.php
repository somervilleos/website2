<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/10/15
 * Time: 8:50 PM
 */
//use Zend\Crypt\Password\Bcrypt;
//use Silex\Application;


// https://stackoverflow.com/questions/14649645/resize-image-in-php


namespace SOS;

/*
* File: SimpleImage.php
* Author: Simon Jarvis
* Copyright: 2006 Simon Jarvis
* Date: 08/11/06
* Link: http://www.white-hat-web-design.co.uk/blog/resizing-images-with-php/
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details:
* http://www.gnu.org/licenses/gpl.html
* gist.github.com/arrowmedia/7863973
*/


class ItineraryCookie {

    const CK_DOMAIN = ".somervilleopenstudios.org";
    //const CK_DOMAIN = "localhost";
    //const CK_DOMAIN = null;



    static function get() {
        if (isset($_COOKIE["itin_r"]) && isset($_COOKIE["itin_e"] )){

            return  array('share_key'=>$_COOKIE['itin_r'], 'edit_key'=>$_COOKIE['itin_e']);
        }

        return null;
    }


    static function create($editKey, $shareKey){
        $expire = time() + 60 * 60 * 24 * 110;
        setcookie("itin_e", $editKey, $expire,'/' ,self::CK_DOMAIN);
        setcookie("itin_r", $shareKey , $expire,'/',self::CK_DOMAIN);

    }

    static function clear(){
        $expire = time() -3600;
        setcookie("itin_e", '', $expire,'/',self::CK_DOMAIN);
        setcookie("itin_r", '' , $expire,'/',self::CK_DOMAIN);

    }

}


