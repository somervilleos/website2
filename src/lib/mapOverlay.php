<?php
// rraman: carried this class over from 2018... but it ended up not being used (logic copied directly into map_js.html.twig)

namespace SOS;

class mapOverlay {


// Locations of things like trolley routes and

private $pathToImages;



// constructor.

// gets the artists lists information and stores in class private variable
// $artistsList

// other functions used to return list as html or json....

   function __construct($path_to_images) {

       // get the arguments
       $this->pathToImages = $path_to_images;


   }

   private function jsonify($rows) {
     $json = '{"markers":['; // start json
     foreach ($rows as $oneRow){
        $json .= json_encode($oneRow) .',' ;
     }
     $json .= ']}';  // End json
     return ($json);
   }

   //---------------------
   // Trolley Information
   //---------------------
  // returns a array of Lat/Long locations of the trolley stops.
  public function getTrolleyStops(){

// push the  lat and long onto an array
// rraman: i generated this by exporting Nick's map (https://www.google.com/maps/d/u/2/edit?mid=1qUaVCjif_KlHcqRINhm79kLYxH1lHot9&ll=42.39096125117538%2C-71.10752516600792&z=14) to a "KML" file
    $temp= array();
    $temp[] = array("map_icon"=>"trolley_icon_n.png", "content" =>"A). Joy Street Studios. 86 Joy St.", "longitude"=>"-71.0868407","latitude"=>"42.3784311");
    $temp[] = array("map_icon"=>"trolley_icon_w.png", "content" =>"B). Mudflat Pottery. Bus stop at Broadway @ Indiana Ave.", "longitude"=>"-71.0825035","latitude"=>"42.3873985");
    $temp[] = array("map_icon"=>"trolley_icon_w.png", "content" =>"C). Mad Oyster Studio. Bus stop at Pearl St. @ Bradley St.", "longitude"=>"-71.0949033","latitude"=>"42.3879362");
    $temp[] = array("map_icon"=>"trolley_icon_w.png", "content" =>"D). Vernon Street. Opposite 20 Vernon St.", "longitude"=>"-71.1023223","latitude"=>"42.3915258");
    $temp[] = array("map_icon"=>"trolley_icon_w.png", "content" =>"E). Highland Ave. @ Conwell St. Bus stop before fire station", "longitude"=>"-71.1107442","latitude"=>"42.3915091");
    $temp[] = array("map_icon"=>"trolley_icon_w.png", "content" =>"F). Shuttle-bus stop across from Davis T and Somerville Theater", "longitude"=>"-71.122835","latitude"=>"42.396932");
    $temp[] = array("map_icon"=>"trolley_icon_e.png", "content" =>"G). Powderhouse Blvd. opposite the Nave Gallery", "longitude"=>"-71.1256613","latitude"=>"42.4053219");
    $temp[] = array("map_icon"=>"trolley_icon_e.png", "content" =>"H). Kidder @ Liberty (far side of intersection)", "longitude"=>"-71.1166125","latitude"=>"42.3984985");
    $temp[] = array("map_icon"=>"trolley_icon_e.png", "content" =>"I). Highland Ave @ Willow Ave. bus stop", "longitude"=>"-71.1166662","latitude"=>"42.3939704");
    $temp[] = array("map_icon"=>"trolley_icon_e.png", "content" =>"J). Arts at the Armory", "longitude"=>"-71.1061753","latitude"=>"42.38938");
    $temp[] = array("map_icon"=>"trolley_icon_e.png", "content" =>"K). Somerville Museum. Avon St. @ Central St.", "longitude"=>"-71.1045646","latitude"=>"42.3864766");
    $temp[] = array("map_icon"=>"trolley_icon_s.png", "content" =>"L). Dane St. @ Tyler St. Near Artisan's Asylum", "longitude"=>"-71.1041291","latitude"=>"42.3813169");
    $temp[] = array("map_icon"=>"trolley_icon_e.png", "content" =>"M). Washington St. @ Perry St. Bus Stop", "longitude"=>"-71.1015434","latitude"=>"42.3793565");
    $temp[] = array("map_icon"=>"trolley_icon_e.png", "content" =>"N). Union Square bus stop in front of Mandarin Chinese", "longitude"=>"-71.0953551","latitude"=>"42.3795013");
    $temp[] = array("map_icon"=>"trolley_icon_n.png", "content" =>"O). Brickbottom Studios very end of Joy St. at Fitchburg St.", "longitude"=>"-71.0844698","latitude"=>"42.3758192");

    foreach ($temp as &$oneRow) {
       $oneRow['title'] = 'Trolley Stop';
    }

    return $this->jsonify($temp);
}

public function getInfoBooths() {
    // rraman: still valid for 2019
    $temp = array();
   //info booth cancelled for Sunday because of rain
   // $temp[] = array("title"=>"Info Booth", "map_icon"=>"info.png", "content" =>"Info Booth Davis Square.  Get info Sat + Sun here","longitude"=> "-71.122523","latitude"=>"42.396831");
    return $this->jsonify($temp);
}

public function getParkingLots() {
    // rraman: in 2019, we have 3 lots
    $temp = array();
    $temp[] = array(    "map_icon"=>"parking.png", "content" =>"Parking: West Somerville Neighborhood School - Raymond Ave","longitude"=>"-71.126819","latitude"=>"42.406664");
    //$temp[] = array(    "map_icon"=>"parking.png", "content" =>"Parking: Tufts Administration Building, 169 Holland St.", "longitude"=>"-71.125773","latitude"=>"42.400877");
    //$temp[] = array(    "map_icon"=>"parking.png", "content" =>"Parking: John F. Kennedy School School - Elm & Cherry st", "longitude"=>"-71.116154","latitude"=>"42.389388");
    $temp[] = array(    "map_icon"=>"parking.png", "content" =>"Parking: City Hall/ Somerville High / Library - 93 Highland Ave","longitude"=>"-71.096805","latitude"=>"42.386592");
    $temp[] = array(    "map_icon"=>"parking.png", "content" =>"Parking: Arthur D. Healey School, 5 Meacham St, Behind School", "longitude"=>"-71.094887","latitude"=>"42.397170");

    foreach ($temp as &$oneRow) {
       $oneRow['title'] = 'Parking Lot';
    }

    return $this->jsonify($temp);
}


public function getTrolleyRoute(){


//2019 route:
// rraman: i generated this by exporting Nick's map (https://www.google.com/maps/d/u/2/edit?mid=1qUaVCjif_KlHcqRINhm79kLYxH1lHot9&ll=42.39096125117538%2C-71.10752516600792&z=14) to a "KML" file
$route = '

 [
new google.maps.LatLng(42.37837, -71.08679),
new google.maps.LatLng(42.37844, -71.08685),
new google.maps.LatLng(42.37857, -71.08692),
new google.maps.LatLng(42.37867, -71.08697),
new google.maps.LatLng(42.37879, -71.08701),
new google.maps.LatLng(42.37895, -71.08708),
new google.maps.LatLng(42.37904, -71.08713),
new google.maps.LatLng(42.37917, -71.08721),
new google.maps.LatLng(42.37929, -71.08731),
new google.maps.LatLng(42.37973, -71.08771),
new google.maps.LatLng(42.38053, -71.08846),
new google.maps.LatLng(42.38067, -71.08858),
new google.maps.LatLng(42.38061, -71.08875),
new google.maps.LatLng(42.3806, -71.08878),
new google.maps.LatLng(42.38059, -71.08888),
new google.maps.LatLng(42.38059, -71.08891),
new google.maps.LatLng(42.38059, -71.08894),
new google.maps.LatLng(42.38044, -71.08936),
new google.maps.LatLng(42.38041, -71.08943),
new google.maps.LatLng(42.38076, -71.08964),
new google.maps.LatLng(42.38085, -71.08968),
new google.maps.LatLng(42.38109, -71.08978),
new google.maps.LatLng(42.38135, -71.08987),
new google.maps.LatLng(42.38154, -71.08993),
new google.maps.LatLng(42.38165, -71.08999),
new google.maps.LatLng(42.38179, -71.09006),
new google.maps.LatLng(42.38193, -71.09014),
new google.maps.LatLng(42.38205, -71.09024),
new google.maps.LatLng(42.38218, -71.09034),
new google.maps.LatLng(42.38243, -71.09025),
new google.maps.LatLng(42.38248, -71.09022),
new google.maps.LatLng(42.38267, -71.09013),
new google.maps.LatLng(42.38285, -71.09003),
new google.maps.LatLng(42.38303, -71.08993),
new google.maps.LatLng(42.38325, -71.08981),
new google.maps.LatLng(42.38335, -71.08974),
new google.maps.LatLng(42.38368, -71.08952),
new google.maps.LatLng(42.38393, -71.08933),
new google.maps.LatLng(42.38422, -71.08911),
new google.maps.LatLng(42.3843, -71.08902),
new google.maps.LatLng(42.38446, -71.08886),
new google.maps.LatLng(42.38465, -71.08869),
new google.maps.LatLng(42.38484, -71.08852),
new google.maps.LatLng(42.38526, -71.08814),
new google.maps.LatLng(42.3854, -71.08803),
new google.maps.LatLng(42.3857, -71.08783),
new google.maps.LatLng(42.38544, -71.08702),
new google.maps.LatLng(42.38515, -71.08607),
new google.maps.LatLng(42.38494, -71.0854),
new google.maps.LatLng(42.38456, -71.08419),
new google.maps.LatLng(42.38531, -71.08367),
new google.maps.LatLng(42.38568, -71.08339),
new google.maps.LatLng(42.38623, -71.083),
new google.maps.LatLng(42.38705, -71.08245),
new google.maps.LatLng(42.38719, -71.08236),
new google.maps.LatLng(42.38727, -71.0823),
new google.maps.LatLng(42.38739, -71.08269),
new google.maps.LatLng(42.38762, -71.08338),
new google.maps.LatLng(42.38774, -71.08371),
new google.maps.LatLng(42.38776, -71.08374),
new google.maps.LatLng(42.38786, -71.08404),
new google.maps.LatLng(42.38815, -71.08469),
new google.maps.LatLng(42.3883, -71.08499),
new google.maps.LatLng(42.38846, -71.08532),
new google.maps.LatLng(42.38856, -71.08553),
new google.maps.LatLng(42.38874, -71.08594),
new google.maps.LatLng(42.38907, -71.08667),
new google.maps.LatLng(42.3892, -71.08693),
new google.maps.LatLng(42.38941, -71.08741),
new google.maps.LatLng(42.38944, -71.08746),
new google.maps.LatLng(42.38956, -71.08773),
new google.maps.LatLng(42.38973, -71.08808),
new google.maps.LatLng(42.38982, -71.08825),
new google.maps.LatLng(42.3897, -71.08838),
new google.maps.LatLng(42.38955, -71.08853),
new google.maps.LatLng(42.38927, -71.08883),
new google.maps.LatLng(42.38902, -71.08908),
new google.maps.LatLng(42.38885, -71.08924),
new google.maps.LatLng(42.38856, -71.08948),
new google.maps.LatLng(42.3882, -71.08972),
new google.maps.LatLng(42.3878, -71.08996),
new google.maps.LatLng(42.38727, -71.09028),
new google.maps.LatLng(42.38695, -71.09045),
new google.maps.LatLng(42.38656, -71.09063),
new google.maps.LatLng(42.38662, -71.09083),
new google.maps.LatLng(42.38681, -71.09141),
new google.maps.LatLng(42.38699, -71.092),
new google.maps.LatLng(42.38704, -71.09219),
new google.maps.LatLng(42.38714, -71.09248),
new google.maps.LatLng(42.38735, -71.09317),
new google.maps.LatLng(42.38754, -71.09375),
new google.maps.LatLng(42.38762, -71.094),
new google.maps.LatLng(42.3878, -71.09459),
new google.maps.LatLng(42.38785, -71.09473),
new google.maps.LatLng(42.38805, -71.0954),
new google.maps.LatLng(42.38814, -71.0957),
new google.maps.LatLng(42.38815, -71.09574),
new google.maps.LatLng(42.38821, -71.0959),
new google.maps.LatLng(42.38823, -71.096),
new google.maps.LatLng(42.38825, -71.09609),
new google.maps.LatLng(42.3883, -71.0963),
new google.maps.LatLng(42.38879, -71.09682),
new google.maps.LatLng(42.38912, -71.09716),
new google.maps.LatLng(42.38948, -71.0975),
new google.maps.LatLng(42.38984, -71.09786),
new google.maps.LatLng(42.38994, -71.09798),
new google.maps.LatLng(42.39044, -71.09849),
new google.maps.LatLng(42.39088, -71.09895),
new google.maps.LatLng(42.39133, -71.0994),
new google.maps.LatLng(42.3919, -71.09999),
new google.maps.LatLng(42.39226, -71.10037),
new google.maps.LatLng(42.39248, -71.10059),
new google.maps.LatLng(42.39194, -71.10095),
new google.maps.LatLng(42.39146, -71.10129),
new google.maps.LatLng(42.39115, -71.10151),
new google.maps.LatLng(42.39117, -71.1016),
new google.maps.LatLng(42.39117, -71.10162),
new google.maps.LatLng(42.39118, -71.10164),
new google.maps.LatLng(42.3912, -71.10169),
new google.maps.LatLng(42.39133, -71.10199),
new google.maps.LatLng(42.39139, -71.1021),
new google.maps.LatLng(42.39167, -71.10268),
new google.maps.LatLng(42.392, -71.10336),
new google.maps.LatLng(42.39213, -71.1036),
new google.maps.LatLng(42.39216, -71.10365),
new google.maps.LatLng(42.39219, -71.10371),
new google.maps.LatLng(42.39236, -71.10397),
new google.maps.LatLng(42.3927, -71.10448),
new google.maps.LatLng(42.39308, -71.10503),
new google.maps.LatLng(42.39352, -71.10565),
new google.maps.LatLng(42.39329, -71.10581),
new google.maps.LatLng(42.39307, -71.10596),
new google.maps.LatLng(42.39302, -71.10601),
new google.maps.LatLng(42.39295, -71.10606),
new google.maps.LatLng(42.39282, -71.10614),
new google.maps.LatLng(42.39276, -71.10618),
new google.maps.LatLng(42.3927, -71.10623),
new google.maps.LatLng(42.39245, -71.10639),
new google.maps.LatLng(42.39226, -71.10653),
new google.maps.LatLng(42.39208, -71.10666),
new google.maps.LatLng(42.3919, -71.10678),
new google.maps.LatLng(42.39139, -71.10714),
new google.maps.LatLng(42.39081, -71.10754),
new google.maps.LatLng(42.3902, -71.10795),
new google.maps.LatLng(42.39054, -71.10871),
new google.maps.LatLng(42.39064, -71.10891),
new google.maps.LatLng(42.39071, -71.10909),
new google.maps.LatLng(42.39077, -71.1092),
new google.maps.LatLng(42.39079, -71.10925),
new google.maps.LatLng(42.39086, -71.10941),
new google.maps.LatLng(42.39122, -71.1102),
new google.maps.LatLng(42.39153, -71.1109),
new google.maps.LatLng(42.39187, -71.11165),
new google.maps.LatLng(42.39221, -71.11241),
new google.maps.LatLng(42.39254, -71.11316),
new google.maps.LatLng(42.3929, -71.114),
new google.maps.LatLng(42.39329, -71.1149),
new google.maps.LatLng(42.39357, -71.11555),
new google.maps.LatLng(42.39385, -71.11619),
new google.maps.LatLng(42.39411, -71.11681),
new google.maps.LatLng(42.39452, -71.11775),
new google.maps.LatLng(42.3949, -71.1186),
new google.maps.LatLng(42.39518, -71.11923),
new google.maps.LatLng(42.3952, -71.11925),
new google.maps.LatLng(42.39522, -71.11927),
new google.maps.LatLng(42.39527, -71.11937),
new google.maps.LatLng(42.39531, -71.11947),
new google.maps.LatLng(42.39534, -71.11956),
new google.maps.LatLng(42.39535, -71.11959),
new google.maps.LatLng(42.39536, -71.11961),
new google.maps.LatLng(42.39537, -71.11968),
new google.maps.LatLng(42.39563, -71.12033),
new google.maps.LatLng(42.39571, -71.1205),
new google.maps.LatLng(42.39597, -71.1211),
new google.maps.LatLng(42.39628, -71.12182),
new google.maps.LatLng(42.39634, -71.12196),
new google.maps.LatLng(42.39648, -71.12229),
new google.maps.LatLng(42.39658, -71.12249),
new google.maps.LatLng(42.39663, -71.12258),
new google.maps.LatLng(42.39671, -71.12267),
new google.maps.LatLng(42.39673, -71.12272),
new google.maps.LatLng(42.39703, -71.12295),
new google.maps.LatLng(42.39747, -71.12331),
new google.maps.LatLng(42.39755, -71.12337),
new google.maps.LatLng(42.39772, -71.12351),
new google.maps.LatLng(42.39789, -71.12365),
new google.maps.LatLng(42.39822, -71.12391),
new google.maps.LatLng(42.39853, -71.12416),
new google.maps.LatLng(42.3986, -71.12422),
new google.maps.LatLng(42.39905, -71.12458),
new google.maps.LatLng(42.39918, -71.12469),
new google.maps.LatLng(42.39985, -71.12524),
new google.maps.LatLng(42.40043, -71.12571),
new google.maps.LatLng(42.40078, -71.12598),
new google.maps.LatLng(42.40105, -71.12618),
new google.maps.LatLng(42.40113, -71.12622),
new google.maps.LatLng(42.40131, -71.12631),
new google.maps.LatLng(42.40141, -71.12635),
new google.maps.LatLng(42.40142, -71.12636),
new google.maps.LatLng(42.40156, -71.12642),
new google.maps.LatLng(42.40177, -71.12651),
new google.maps.LatLng(42.40242, -71.12679),
new google.maps.LatLng(42.40267, -71.1269),
new google.maps.LatLng(42.40288, -71.12699),
new google.maps.LatLng(42.40304, -71.12704),
new google.maps.LatLng(42.40324, -71.12715),
new google.maps.LatLng(42.4033, -71.12728),
new google.maps.LatLng(42.40412, -71.12673),
new google.maps.LatLng(42.40434, -71.12658),
new google.maps.LatLng(42.40481, -71.12628),
new google.maps.LatLng(42.40488, -71.12624),
new google.maps.LatLng(42.4051, -71.1261),
new google.maps.LatLng(42.40543, -71.12585),
new google.maps.LatLng(42.40537, -71.12569),
new google.maps.LatLng(42.40528, -71.12547),
new google.maps.LatLng(42.40496, -71.12469),
new google.maps.LatLng(42.40446, -71.12347),
new google.maps.LatLng(42.40429, -71.1231),
new google.maps.LatLng(42.40414, -71.12276),
new google.maps.LatLng(42.40405, -71.12257),
new google.maps.LatLng(42.40394, -71.12233),
new google.maps.LatLng(42.40384, -71.12214),
new google.maps.LatLng(42.40378, -71.12203),
new google.maps.LatLng(42.4037, -71.12186),
new google.maps.LatLng(42.40362, -71.12171),
new google.maps.LatLng(42.40352, -71.12153),
new google.maps.LatLng(42.40349, -71.12148),
new google.maps.LatLng(42.40347, -71.12144),
new google.maps.LatLng(42.40344, -71.1214),
new google.maps.LatLng(42.40342, -71.12136),
new google.maps.LatLng(42.40336, -71.12128),
new google.maps.LatLng(42.40329, -71.12119),
new google.maps.LatLng(42.40318, -71.12106),
new google.maps.LatLng(42.40305, -71.12091),
new google.maps.LatLng(42.40294, -71.12079),
new google.maps.LatLng(42.40286, -71.12069),
new google.maps.LatLng(42.40277, -71.12058),
new google.maps.LatLng(42.40271, -71.12049),
new google.maps.LatLng(42.40266, -71.12041),
new google.maps.LatLng(42.4026, -71.12033),
new google.maps.LatLng(42.40253, -71.12021),
new google.maps.LatLng(42.40246, -71.12009),
new google.maps.LatLng(42.40238, -71.11992),
new google.maps.LatLng(42.40231, -71.11977),
new google.maps.LatLng(42.40227, -71.11968),
new google.maps.LatLng(42.40225, -71.11962),
new google.maps.LatLng(42.40223, -71.11958),
new google.maps.LatLng(42.40217, -71.11942),
new google.maps.LatLng(42.40214, -71.11934),
new google.maps.LatLng(42.40212, -71.11926),
new google.maps.LatLng(42.40209, -71.11918),
new google.maps.LatLng(42.40207, -71.1191),
new google.maps.LatLng(42.40203, -71.11893),
new google.maps.LatLng(42.40199, -71.11876),
new google.maps.LatLng(42.40196, -71.11863),
new google.maps.LatLng(42.40194, -71.11855),
new google.maps.LatLng(42.40192, -71.11847),
new google.maps.LatLng(42.4019, -71.11839),
new google.maps.LatLng(42.40187, -71.1183),
new google.maps.LatLng(42.40182, -71.11817),
new google.maps.LatLng(42.40177, -71.11805),
new google.maps.LatLng(42.40173, -71.11795),
new google.maps.LatLng(42.40166, -71.11783),
new google.maps.LatLng(42.4016, -71.11772),
new google.maps.LatLng(42.40151, -71.11758),
new google.maps.LatLng(42.40147, -71.11753),
new google.maps.LatLng(42.40118, -71.11711),
new google.maps.LatLng(42.40111, -71.11711),
new google.maps.LatLng(42.40108, -71.11711),
new google.maps.LatLng(42.40104, -71.1171),
new google.maps.LatLng(42.40103, -71.11709),
new google.maps.LatLng(42.40101, -71.11707),
new google.maps.LatLng(42.40099, -71.11706),
new google.maps.LatLng(42.40097, -71.11702),
new google.maps.LatLng(42.40093, -71.11692),
new google.maps.LatLng(42.40066, -71.11714),
new google.maps.LatLng(42.40058, -71.11724),
new google.maps.LatLng(42.40023, -71.11772),
new google.maps.LatLng(42.40023, -71.11773),
new google.maps.LatLng(42.40013, -71.11786),
new google.maps.LatLng(42.39989, -71.1182),
new google.maps.LatLng(42.39977, -71.11836),
new google.maps.LatLng(42.39968, -71.1185),
new google.maps.LatLng(42.3996, -71.11863),
new google.maps.LatLng(42.39952, -71.11876),
new google.maps.LatLng(42.39944, -71.1189),
new google.maps.LatLng(42.39941, -71.11886),
new google.maps.LatLng(42.3994, -71.11884),
new google.maps.LatLng(42.39938, -71.11881),
new google.maps.LatLng(42.39925, -71.11847),
new google.maps.LatLng(42.39891, -71.11758),
new google.maps.LatLng(42.39857, -71.1167),
new google.maps.LatLng(42.39828, -71.11593),
new google.maps.LatLng(42.398, -71.11516),
new google.maps.LatLng(42.39773, -71.1144),
new google.maps.LatLng(42.39771, -71.11435),
new google.maps.LatLng(42.39766, -71.11438),
new google.maps.LatLng(42.39689, -71.11491),
new google.maps.LatLng(42.39629, -71.11533),
new google.maps.LatLng(42.39576, -71.11568),
new google.maps.LatLng(42.39569, -71.11572),
new google.maps.LatLng(42.39564, -71.11576),
new google.maps.LatLng(42.39526, -71.11603),
new google.maps.LatLng(42.39525, -71.11603),
new google.maps.LatLng(42.39525, -71.11604),
new google.maps.LatLng(42.39485, -71.11631),
new google.maps.LatLng(42.39463, -71.11647),
new google.maps.LatLng(42.39411, -71.11681),
new google.maps.LatLng(42.39385, -71.11619),
new google.maps.LatLng(42.39357, -71.11555),
new google.maps.LatLng(42.39329, -71.1149),
new google.maps.LatLng(42.3929, -71.114),
new google.maps.LatLng(42.39254, -71.11316),
new google.maps.LatLng(42.39221, -71.11241),
new google.maps.LatLng(42.39187, -71.11165),
new google.maps.LatLng(42.39153, -71.1109),
new google.maps.LatLng(42.39122, -71.1102),
new google.maps.LatLng(42.39086, -71.10941),
new google.maps.LatLng(42.39079, -71.10925),
new google.maps.LatLng(42.39077, -71.1092),
new google.maps.LatLng(42.39071, -71.10909),
new google.maps.LatLng(42.39064, -71.10891),
new google.maps.LatLng(42.39054, -71.10871),
new google.maps.LatLng(42.3902, -71.10795),
new google.maps.LatLng(42.3899, -71.10728),
new google.maps.LatLng(42.38968, -71.1068),
new google.maps.LatLng(42.38942, -71.10621),
new google.maps.LatLng(42.38931, -71.10597),
new google.maps.LatLng(42.38923, -71.10576),
new google.maps.LatLng(42.38918, -71.10565),
new google.maps.LatLng(42.38914, -71.10553),
new google.maps.LatLng(42.38903, -71.10523),
new google.maps.LatLng(42.38871, -71.10429),
new google.maps.LatLng(42.38842, -71.10344),
new google.maps.LatLng(42.38787, -71.10381),
new google.maps.LatLng(42.38781, -71.10385),
new google.maps.LatLng(42.38735, -71.10417),
new google.maps.LatLng(42.38716, -71.1043),
new google.maps.LatLng(42.38677, -71.10458),
new google.maps.LatLng(42.38662, -71.1047),
new google.maps.LatLng(42.38633, -71.10388),
new google.maps.LatLng(42.38598, -71.10294),
new google.maps.LatLng(42.3857, -71.10216),
new google.maps.LatLng(42.38534, -71.10117),
new google.maps.LatLng(42.38512, -71.10057),
new google.maps.LatLng(42.38492, -71.10003),
new google.maps.LatLng(42.38461, -71.10024),
new google.maps.LatLng(42.38437, -71.1004),
new google.maps.LatLng(42.38377, -71.10083),
new google.maps.LatLng(42.38319, -71.10121),
new google.maps.LatLng(42.38254, -71.10167),
new google.maps.LatLng(42.382, -71.10202),
new google.maps.LatLng(42.38225, -71.10283),
new google.maps.LatLng(42.38244, -71.10341),
new google.maps.LatLng(42.38219, -71.10356),
new google.maps.LatLng(42.38188, -71.10375),
new google.maps.LatLng(42.38158, -71.10394),
new google.maps.LatLng(42.38145, -71.10401),
new google.maps.LatLng(42.38115, -71.10419),
new google.maps.LatLng(42.38086, -71.10436),
new google.maps.LatLng(42.38048, -71.10459),
new google.maps.LatLng(42.38042, -71.10461),
new google.maps.LatLng(42.38037, -71.1046),
new google.maps.LatLng(42.38009, -71.10451),
new google.maps.LatLng(42.3799, -71.10444),
new google.maps.LatLng(42.37955, -71.10431),
new google.maps.LatLng(42.37884, -71.10405),
new google.maps.LatLng(42.37898, -71.10347),
new google.maps.LatLng(42.37899, -71.10342),
new google.maps.LatLng(42.37901, -71.10337),
new google.maps.LatLng(42.37917, -71.10281),
new google.maps.LatLng(42.37921, -71.10266),
new google.maps.LatLng(42.37924, -71.10249),
new google.maps.LatLng(42.37927, -71.10234),
new google.maps.LatLng(42.3793, -71.10214),
new google.maps.LatLng(42.37942, -71.10144),
new google.maps.LatLng(42.37943, -71.10125),
new google.maps.LatLng(42.37943, -71.10109),
new google.maps.LatLng(42.37944, -71.10104),
new google.maps.LatLng(42.37944, -71.10095),
new google.maps.LatLng(42.37944, -71.1008),
new google.maps.LatLng(42.37943, -71.10064),
new google.maps.LatLng(42.37942, -71.10052),
new google.maps.LatLng(42.37941, -71.10043),
new google.maps.LatLng(42.3794, -71.10035),
new google.maps.LatLng(42.37937, -71.10015),
new google.maps.LatLng(42.37935, -71.10007),
new google.maps.LatLng(42.37934, -71.09999),
new google.maps.LatLng(42.37933, -71.09984),
new google.maps.LatLng(42.37932, -71.09969),
new google.maps.LatLng(42.37932, -71.09953),
new google.maps.LatLng(42.37932, -71.09949),
new google.maps.LatLng(42.37933, -71.09936),
new google.maps.LatLng(42.37939, -71.09886),
new google.maps.LatLng(42.37942, -71.0986),
new google.maps.LatLng(42.37949, -71.09799),
new google.maps.LatLng(42.37951, -71.09769),
new google.maps.LatLng(42.37954, -71.09727),
new google.maps.LatLng(42.37956, -71.0971),
new google.maps.LatLng(42.37957, -71.09702),
new google.maps.LatLng(42.37957, -71.09696),
new google.maps.LatLng(42.37963, -71.09645),
new google.maps.LatLng(42.37965, -71.0963),
new google.maps.LatLng(42.37967, -71.09603),
new google.maps.LatLng(42.37967, -71.09575),
new google.maps.LatLng(42.37966, -71.0957),
new google.maps.LatLng(42.37966, -71.09566),
new google.maps.LatLng(42.37965, -71.09561),
new google.maps.LatLng(42.3796, -71.0955),
new google.maps.LatLng(42.37956, -71.09538),
new google.maps.LatLng(42.37952, -71.09527),
new google.maps.LatLng(42.3793, -71.09467),
new google.maps.LatLng(42.37918, -71.09434),
new google.maps.LatLng(42.37909, -71.09412),
new google.maps.LatLng(42.37887, -71.09348),
new google.maps.LatLng(42.37873, -71.0931),
new google.maps.LatLng(42.37871, -71.09304),
new google.maps.LatLng(42.37865, -71.09289),
new google.maps.LatLng(42.37854, -71.09259),
new google.maps.LatLng(42.3783, -71.0919),
new google.maps.LatLng(42.37827, -71.09182),
new google.maps.LatLng(42.37798, -71.091),
new google.maps.LatLng(42.37784, -71.09063),
new google.maps.LatLng(42.37768, -71.09019),
new google.maps.LatLng(42.37763, -71.09004),
new google.maps.LatLng(42.37751, -71.08977),
new google.maps.LatLng(42.37725, -71.08922),
new google.maps.LatLng(42.37717, -71.08906),
new google.maps.LatLng(42.37709, -71.08889),
new google.maps.LatLng(42.37693, -71.08859),
new google.maps.LatLng(42.37683, -71.08838),
new google.maps.LatLng(42.37679, -71.08836),
new google.maps.LatLng(42.37675, -71.08833),
new google.maps.LatLng(42.37671, -71.08827),
new google.maps.LatLng(42.37668, -71.08821),
new google.maps.LatLng(42.37666, -71.08817),
new google.maps.LatLng(42.37623, -71.08727),
new google.maps.LatLng(42.37616, -71.08713),
new google.maps.LatLng(42.37607, -71.08698),
new google.maps.LatLng(42.37599, -71.08687),
new google.maps.LatLng(42.37595, -71.08682),
new google.maps.LatLng(42.37589, -71.08676),
new google.maps.LatLng(42.37578, -71.08663),
new google.maps.LatLng(42.37576, -71.08661),
new google.maps.LatLng(42.37574, -71.08659),
new google.maps.LatLng(42.3757, -71.08653),
new google.maps.LatLng(42.37568, -71.08649),
new google.maps.LatLng(42.37565, -71.08644),
new google.maps.LatLng(42.37563, -71.08638),
new google.maps.LatLng(42.37561, -71.08631),
new google.maps.LatLng(42.37559, -71.08606),
new google.maps.LatLng(42.37558, -71.08601),
new google.maps.LatLng(42.37555, -71.0857),
new google.maps.LatLng(42.37555, -71.08565),
new google.maps.LatLng(42.37555, -71.08564),
new google.maps.LatLng(42.37556, -71.0856),
new google.maps.LatLng(42.37557, -71.08551),
new google.maps.LatLng(42.37562, -71.08532),
new google.maps.LatLng(42.37555, -71.08524),
new google.maps.LatLng(42.37553, -71.08522),
new google.maps.LatLng(42.37552, -71.0852),
new google.maps.LatLng(42.37551, -71.08516),
new google.maps.LatLng(42.37551, -71.08514),
new google.maps.LatLng(42.37552, -71.08512),
new google.maps.LatLng(42.37552, -71.08509),
new google.maps.LatLng(42.37555, -71.08503),
new google.maps.LatLng(42.37586, -71.08444),
new google.maps.LatLng(42.37788, -71.08633),
new google.maps.LatLng(42.37813, -71.08659),
new google.maps.LatLng(42.37831, -71.08674),
new google.maps.LatLng(42.3784, -71.08681)

];
';

return $route;


}

   //---------------------
   // Event Information
   //---------------------



  public function getExhibits(){
    $temp= array();

    // 2018 Event locations

    $temp[] = array("latitude"=>"42.38692", "longitude"=>"-71.10476",
        "title"=> "Somerville Museum - SOS First Look Show",
        "content" =>'<b>SOS First Look Show</b> <br>Somerville Museum<br> 1 Westwood Rd<br> <a target="_blank" href="http://www.somervilleopenstudios.org/visit#first_look">Details</a>',
        "color"=>"red",
        "map_icon"=>"number_59.png");

    $temp[] = array("latitude"=>"42.39577", "longitude"=>"-71.12187",
        "title"=> "Volunteer Show",
        "content" =>'<b>Volunteer Show</b><br>Diesel Cafe <br>257 Elm Street<br> <a target="_blank" href="http://www.somervilleopenstudios.org/visit/#volunteer">Details</a>',
          "color"=>"red",
        "map_icon"=>"number_12.png");

    $temp[] = array("latitude"=>"42.396329", "longitude"=>"-71.122538",
        "title"=> "Inside Out Gallery",
        "content" =>'<b>Inside Out Gallery</b><br>CVS Window | One Davis Square<br> <a target="_blank" href="http://www.somervilleopenstudios.org/visit/#inside_out">Details</a>',
           "color"=>"red",
        "map_icon"=>"symbol_blank.png");

    $temp[] = array("latitude"=>"42.394255", "longitude"=>"-71.116942",
        "title"=> "Cracked: The Nut Show",
        "content" =>'<b>Cracked: The Nut Show</b><br>Q\'s Nuts<br>349 Highland Ave.<br> <a target="_blank" href="http://www.somervilleopenstudios.org/visit/#nut">Details</a>',
          "color"=>"red",
        "map_icon"=>"number_17.png");

    return $this->jsonify($temp);
  }

} // class

?>