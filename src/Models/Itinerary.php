<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 3/15/16
 * Time: 10:33 PM
 */
namespace SOSModels;

class Itinerary {

    


    const DIGITCOUNTS = 4;
    public $debug =false;
    private $sos_dbo;

    public function __construct(\PDO $dbo) {
        $this->sos_dbo = $dbo;
    }


    // with the share ket and the edit key.  See if we can look at a row.

    public function checkRowExists ($itinData){


        $share_key   = $itinData['share_key'];
        $edit_key   = $itinData['edit_key'];

        $sql='SELECT * from itinerary where edit_key =:edit_key  and share_key = :share_key';

        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':edit_key', $edit_key , \PDO::PARAM_STR);
        $stmt->bindParam (':share_key', $share_key, \PDO::PARAM_STR);

        $r = $stmt->execute();
        if ($r){
            $results = $stmt->fetchAll(\PDO::FETCH_OBJ);
        } else {
            return null;
        }

        return $results;

    }





    private function generateRandomString($length = 4)
    {
        $characters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }



    // returns a list of artist IDS as an array

    public function getItinerayArtistIDsFromKey ($key){

        // get the last 3 characters.  those are the itineray id
        //

        $itinery_id = substr($key, -self::DIGITCOUNTS);
        $share_key   = $key;
        $itinID = ltrim($itinery_id, '0');

        if ($this->debug){
            echo "$itinID<br>";
            echo "$share_key<br>";
        }

        $sql='SELECT * from itinerary where itin_id =:id  and share_key = :share_key';

        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':id', $itinID , \PDO::PARAM_STR);
        $stmt->bindParam (':share_key', $share_key, \PDO::PARAM_STR);

        $r = $stmt->execute();
        if ($r){
            $results = $stmt->fetchAll(\PDO::FETCH_OBJ);
        } else {
            return null;
        }
        if ($this->debug){
            echo "results<br>";
            var_dump($results);
        }

        if (count($results) > 0 ){
            $js = json_decode($results[0]->locations);
        } else {
            return null;
        }

        return $js;

    }



    public function getArtistListFromKey($key){

        $results='';
        $currentItin = $this->getItinerayArtistIDsFromKey($key);

        if (is_null($currentItin)){

        }

        return $results;
    }

    /**
     * @param $artistID
     * @param $editKey  echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
     * @param $shareKey
     * @return array|mixed|null
     *
     */


    public function addArtistToItinerary ($artistID,  $editKey, $shareKey){


        $len = strlen($editKey);
        $itineryID = substr($editKey, -self::DIGITCOUNTS);
        $itinID = ltrim($itineryID, '0');

        $results = $this->getItinerayArtistIDsFromKey($shareKey);



        if (is_null($results)){
            return null;
        }


        // check if allready added
        if (in_array($artistID, $results)){
            return $this->getItinerayArtistIDsFromKey($shareKey);
        }


        $results[]=$artistID;
        $js = json_encode($results);

        $sql = " UPDATE `itinerary` SET `locations` = :js WHERE `itin_id` = :id and `edit_key`=:edit_key; ";
        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':id', $itinID , \PDO::PARAM_STR);
        $stmt->bindParam (':js', $js,\PDO::PARAM_STR);
        $stmt->bindParam (':edit_key', $editKey,\PDO::PARAM_STR);



        if ($stmt->execute()){


            $results = $this->getItinerayArtistIDsFromKey($shareKey);

        } else {
            if (\SOSModels\Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            return null;
        }

        return $results;

    }


    /**
     * @param $artistID
     * @param $editKey
     * @param $shareKey
     * @return array|mixed|null
     */


    public function removeArtistFromItinerary ($artistID,  $editKey, $shareKey ){

        $itineryID = substr($editKey, -self::DIGITCOUNTS);
        $itinID = ltrim($itineryID, '0');

        $results = $this->getItinerayArtistIDsFromKey($shareKey);

        if (is_null($results)){
            return null;
        }

        // remove
        if (in_array($artistID, $results)){
            $results = array_merge(array_diff($results, array($artistID)));
        }

        $js = json_encode($results);

        $sql = " UPDATE `itinerary` SET `locations` = :js WHERE `itin_id` = :id and `edit_key`=:edit_key; ";
        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':id', $itinID ,\PDO::PARAM_STR);
        $stmt->bindParam (':js', $js,\PDO::PARAM_STR);
        $stmt->bindParam (':edit_key', $editKey,\PDO::PARAM_STR);

        $r = $stmt->execute();


        if ($stmt->execute()){
            $results = $this->getItinerayArtistIDsFromKey($shareKey);
        } else {

            if (\SOSModels\Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            return null;
        }

        return $results;
    }


    /*
     * Add the first element to the itinerary.  This creates the keys and inserts the row.
     *
     *
     *
     */

    public function addFirstArtistToItinerary ($artistID){

        $number1 = $this->generateRandomString();
        $number2 = $this->generateRandomString();


        $results=array($artistID);
        $js = json_encode($results);
        $sql ='INSERT INTO `itinerary` ( `edit_key`, `share_key`, `locations`) VALUES (:edit_key, :share_key, :js )';

        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':edit_key', $number1 ,\PDO::PARAM_STR);
        $stmt->bindParam (':share_key', $number2 ,\PDO::PARAM_STR);
        $stmt->bindParam (':js', $js,\PDO::PARAM_STR);


        $r = $stmt->execute();

        if (!$r){
            if (\SOSModels\Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }

            return null;
        }

        $x = $this->sos_dbo->lastInsertId();

        $end = str_pad($x, 4, '0', STR_PAD_LEFT);
        $number1 = $number1 . $end;
        $number2 = $number2 . $end;


        $sql ='Update `itinerary` set `edit_key` = :edit_key, `share_key` = :share_key, `locations` =:js  WHERE `itin_id`= :itin_id';

        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':itin_id', $x ,\PDO::PARAM_STR);
        $stmt->bindParam (':edit_key', $number1 ,\PDO::PARAM_STR);
        $stmt->bindParam (':share_key', $number2 ,\PDO::PARAM_STR);
        $stmt->bindParam (':js', $js,\PDO::PARAM_STR);

        $r = $stmt->execute();

        if (!$r){
            if (\SOSModels\Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }

            return null;
        }

        $returnVal = [];
        $returnVal['shareKey'] = $number2;
        $returnVal['editKey']= $number1;
        $returnVal['itinID'] = $x;


        return $returnVal;
    }



    public function getDetailsFromShareKey($key){

        $itinery_id = substr($key, -self::DIGITCOUNTS);
        $share_key   = $key;
        $itinID = ltrim($itinery_id, '0');

        $sql='SELECT * from itinerary where itin_id =:id  and share_key = :share_key';

        $stmt = $this->sos_dbo->prepare ($sql);
        $stmt->bindParam (':id', $itinID ,\PDO::PARAM_STR);
        $stmt->bindParam (':share_key', $share_key,\PDO::PARAM_STR);

        $r = $stmt->execute();
        if ($r){
            $results = $stmt->fetch(\PDO::FETCH_ASSOC);
        } else {
            return null;
        }

        return $results;

    }

    /*

    */
}