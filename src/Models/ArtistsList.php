<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/1/15
 * Time: 11:59 PM
 */

namespace SOSModels;
use Random\Engine\PcgOneseq128XslRr64;
use Silex\Application;


class ArtistsList
{
    public  $column_syn_and_name_to_genre_name = array(
        'books' => 'Books',
        'paper' => 'Books',
        'collage' => 'Collage',
        'drawing' => 'Drawing',
        'fiber' => 'Fiber',
        'textiles' => 'Fiber',
        'furniture' => 'Furniture',
        'glass' => 'Glass',
        'mosaics' => 'Glass',
        'graphic' => 'Graphic',
        'graphics' => 'Graphic',
        'installation' => 'Installation',
        'jewelry' => 'Jewelry',
        'beads' => 'Jewelry',
        'mixed-media' => 'Mixed-Media',
        'multimedia' => 'Mixed-Media',
        'painting' => 'Painting',
        'photography' => 'Photography',
        'photos' => 'Photography',
        'pottery' => 'Pottery',
        'ceramics' => 'Pottery',
        'printmaking' => 'Printmaking',
        'prints' => 'Printmaking',
        'sculpture' => 'Sculpture',
        'video' => 'Video',
        'other' => 'Other');

    private $sos_dbo;
    private $sort;
    private $lookupView;
    private $textSearchName = '';
    private $messages;
    private $mapOverrides;
    private $topIncludes='';
    private $bottomIncludes='';

    public function __construct(\PDO $dbo)
    {
        $this->sos_dbo = $dbo;
        $this->sort = "PublicLastName, PublicFirstName";
        $this->lookupView = 'current_active_artists_view';
        $this->messages = array();
        $this->mapOverrides = array();
        $this->publicCols = ["id","Participating","Active", "`location_id`","`profile_id`","`member_id`","`HandicapAccessible`","`Website`","`Books`","`Collage`","`Drawing`","`Fiber`","`Multimedia`","`Mixed_Media`","`Painting`","`Photography`","`Pottery`",
            "`Furniture`","`Glass`","`Graphic`","`Installation`","`Jewelry`","`Printmaking`","`Sculpture`","`Video`","`Other_Media`","`Other`","`ShortDescription`","`PublicLastName`","`PublicFirstName`","`BusinessName`",
            "`BusinessNameSortKey`","`OrganizationDescription`","`address_details`","`use_images_for_promotion`","`home_studio`","`handicap_accessible`","`building_name`","`street_address`","`street_number`",
            "`street_name`","`city`","`state`","`zip`","`notes`","`lat`","`lng`","`map_number`","`map_coordinates`"];

        $this->publicColsStr = implode(',', $this->publicCols);

    }

    public function setSort($col)
    {
        if ($col == "random"){
            $this->sort = " RAND()";
        } else {
            $this->sort = $col;
        }
    }


    public function getMessages(){
        return $this->messages;

    }

    public function getSearchDescription(){
        return $this->textSearchName;

    }


    public function getMapOverrides(){
        return $this->mapOverrides;
    }

    public function getTopIncludes(){
        return $this->topIncludes;
    }

    public function getBottomIncludes(){
        return $this->bottomIncludes;
    }


    //resest session
    // for some reason this will log out a user...

    public static function resetSearchIds (Application $app){


        $app['session']->set('searchList', null);
        $app['session']->set('searchName', null);
        $app['session']->set('searchURL', null);

        $app['session']->remove('searchList');
        $app['session']->remove('searchName');
        $app['session']->remove('searchURL');


    }



    public function setAllArtisys()
    {
        $this->lookupView = 'current_active_artists_view';
    }


    public function setOnlyParticipating()
    {
        $this->lookupView = 'current_participating_artists_view';
    }

    //--------------------------------------------------------------
    // Get All
    //

    public function getAll() {
        $this->textSearchName = 'Showing All Arists';
        return $this->getArtistListMuliti("All","All","All","and");

    }





    //--------------------------------------------------------------
    // Get By Random
    //

    public function getRandom($number) {
        $this->textSearchName = 'Showing All Arists: Random Order';

        $sql = "SELECT {$this->publicColsStr} from {$this->lookupView} ORDER BY RAND() Limit ?";

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(1, $number, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        //shuffle ($all_rows);
        return $all_rows;

    }

    //--------------------------------------------------------------
    // Get By Map Number
    //

    public function getByMapNumber($mapNumber) {
        $mapNumber = (int)$mapNumber;
        if (! is_int($mapNumber)) {
            // don't allow access to arbitrary columns of database table!
            print 'Not integer.  map numbner must be numeric';
        }

        $sql = "SELECT {$this->publicColsStr} FROM current_participating_artists_view where map_number = :mapnum ORDER BY $this->sort";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(':mapnum', $mapNumber, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;
    }

    //--------------------------------------------------------------
    // Get By Member ID
    //

    public function getByMemberId($memberId) {
        $memberId = (int)$memberId;
        if (! is_int($memberId)) {
            // don't allow access to arbitrary columns of database table!
            print 'Not integer.  member ID must be numeric';
        }

        $sql = "SELECT {$this->publicColsStr} FROM current_participating_artists_view where member_id = :memberId ORDER BY $this->sort";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(':memberId', $memberId, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;
    }

    //--------------------------------------------------------------
    // Get Search Term

    public function getBySearchTerm($search) {

        $mediums = $this->column_syn_and_name_to_genre_name;


        $results = array();
        $nameList = preg_split('/\s+/', $search);



        if (empty($nameList)) {
            return array();
        }

        $nameList = array_slice($nameList,0,8);


        foreach ($nameList as $onePatern) {

            $pattern0 = $this->sos_dbo->quote("%" . $onePatern . "%");
            $pattern0start = $this->sos_dbo->quote("" . $onePatern . "%");
            $u1 = $this->sos_dbo->quote($onePatern);

            // Check for medium:
            if (array_key_exists($onePatern, $mediums)){
                $mediumCol = $mediums[$onePatern];
                $mediumString = "UNION
                (SELECT member_id, '170' as mycount  from current_active_artists_view
      where $mediumCol = 'Y')
";
            } else {
                $mediumString = '';
            }

            $sql = <<<EOL
      SELECT * from {$this->lookupView}  cav
join 
(select member_id,sum( mycount) as search_score from 
(
(SELECT member_id, '1000' as mycount  from current_active_artists_view
      where PublicFirstName = $u1 or PublicLastName= $u1 or BusinessName = $u1)
      $mediumString
      UNION
      (SELECT member_id, '800' as mycount  from current_active_artists_view
      where PublicFirstName like $pattern0start  or PublicLastName like $pattern0start  or BusinessName like $pattern0start)
      UNION
      (SELECT member_id, '300' as mycount  from current_active_artists_view
      where PublicFirstName like $pattern0  or PublicLastName like $pattern0  or BusinessName like $pattern0)
      UNION
      (SELECT member_id, '150' as mycount  from current_active_artists_view
      where building_name like $pattern0 )
      UNION
      (SELECT member_id, '100' as mycount  from current_active_artists_view
      where street_address like $pattern0 )
      UNION
      (SELECT member_id, '200' as mycount  from current_active_artists_view
      where shortDescription like $pattern0 )
) a group by member_id ) b
on b.member_id=cav.member_id order by search_score DESC, cav.publicLastName ASC, cav.publicFirstName ASC ;
EOL;


            // todo remove sort because we have it.

            //$sql .= "ORDER BY $this->sort";
            $stmt = $this->sos_dbo->prepare($sql);
            //$stmt->bindValue(1, $number, \PDO::PARAM_INT);
            $stmt->execute();
            $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC | \PDO::FETCH_GROUP);


            // $results
            if (!empty($all_rows)) {
                foreach ($all_rows as $id=>$dataArray){
                    $data=$dataArray[0];

                    // add scores if they already exists
                    if (array_key_exists($id, $results)){
                        $results[$id]['search_score'] += $data['search_score'];
                    } else {
                        $results[$id]= $data;
                        $results[$id]['id']= $id;
                    }
                }
            }

        }

        // sort the results.  The templates display in order
        usort($results, fn($a, $b) => $b['search_score'] <=> $a['search_score']);

        return $results;

    }

    /*
     *
     * $query = $wpdb->prepare(
  "SELECT post_title from $wpdb->posts
  WHERE post_title LIKE %s",
  "%" . $wpdb->esc_like( $myTitle ) . "%"
);
     *
     *
     *
     */

    public function getBySearchTerm_old($search)
    {



        $nameList = preg_split('/\s+/', $search);
        $pattern0 = $this->sos_dbo->quote("%" . $nameList[0] . "%");
        if (isset ($nameList[1]) && $nameList[1] != "") {
            $pattern1 = $this->sos_dbo->quote("%" . $nameList[1] . "%");

            $sql = <<<EOL
     SELECT * FROM {$this->lookupView} where
        PublicFirstName like $pattern0
     or PublicFirstName like $pattern1
     or PublicLastName like $pattern0
     or PublicLastName like $pattern1
     or building_name  like $pattern0
     or building_name  like $pattern1
     or Website  like $pattern0
     or Website  like $pattern1
     or street_address  like $pattern0
     or street_address  like $pattern1
     or address_details  like $pattern0
     or address_details  like $pattern1
     or ShortDescription  like $pattern0
     or ShortDescription  like $pattern1
     or BusinessName  like $pattern0
     or BusinessName  like $pattern1

EOL;

        } else {
            $sql = <<<EOL
      SELECT * from {$this->lookupView} where
        PublicFirstName like $pattern0
     or PublicLastName like $pattern0
     or building_name  like $pattern0
     or Website  like $pattern0
     or street_address  like $pattern0
     or address_details  like $pattern0
     or ShortDescription  like $pattern0
     or BusinessName  like $pattern0

EOL;
        }

        $sql .= "ORDER BY $this->sort";
        $stmt = $this->sos_dbo->prepare($sql);
        //$stmt->bindValue(1, $number, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;


    }




    public function getArtistListMuliti ($genre, $events, $mapNumbers, $glue, $orderBy = null)
    {

        $localSearchName ='';

        if (!empty($orderBy)){


        }
        //print "<h3> Multi</h3>";
        // so any of these values can contain the word "All" this might mess up if a search is for "all" but we'll let it go.

        // glue can is and:or/
        // its always "and events" and "and "map"


        $whereClauses = array();
        //-----------------------------------------------
        // Genre Or Mediums
        //-----------------------------------------------

        $genreSql = '';
        if (strtolower($genre) != "all") {
            $genreValues = explode(",", $genre);
            $genreQueries = array();

            // we have a fixed volabulary for genres so...

            $names = array();

            foreach ($genreValues as $oneElement) {
                $oneElement = trim($oneElement);

                if (!isset(\SOSModels\ArtistsGenreList::$column_name_to_genre_name[$oneElement])) {
                    $errors[] = "Genre not found $oneElement";
                } else {
                    $names[] = $oneElement;
                    $genreQueries[] = " `$oneElement` = 'Y' ";
                }
            }
            if (!empty($genreQueries)) {
                $genreSql = "(" . implode("or", $genreQueries) . ")";
                $whereClauses[]= $genreSql;
            }

            $localSearchName .= " Medium: ". implode(", ", $names);

        }


        //-----------------------------------------------
        // Events
        //-----------------------------------------------


        // build events list ('form name' => 'Column name')
        $validEvents = array('friday'=>'fridayNight', 'fashion'=>'fashion_show', 'first_look'=>'first_look_show',
            'accessible'=>'HandicapAccessible');

        $names = array();

        $eventlc = strtolower($events);
        /*
        if (! empty($events) && strtolower ($events) != "all"){

            $eventValues = explode(",", $events);

            $eventsQueries = array();

            foreach ($eventValues as $oneElement) {
                $oneElement = trim($oneElement);
                $names[] = $oneElement;

                // we have a fixed volabulary for events, so check
                if (array_key_exists($oneElement, $validEvents)){

                    if (!empty($validEvents[$oneElement])){
                        $eventsQueries[]= " `{$validEvents[$oneElement]}` = 'Y' ";

                        if ($validEvents[$oneElement]=='fridayNight' ){
                            $this->mapOverrides['showTrolley'] = false;
                            $this->mapOverrides['showSponsors'] = false;
                            $this->topIncludes = 'blocks/listing_blurb_friday.html.twig';

                        }
                        if ($oneElement=='accessible' ){
                           $this->setOnlyParticipating();
                        }
                    }

                }
            }


            if (!empty($eventsQueries)) {

                $eventSql = "(" . implode("or", $eventsQueries) . ")";
                $whereClauses[]= $eventSql;
                $localSearchName .= " Event/Feature: ". implode(", ", $names);
            }
        }

        */

        // Use New Events Table:
        if (! empty($events) && strtolower ($events) != "all") {
            $eventObj = new \SOSModels\EventData($this->sos_dbo, 2021);
            $eventDetails = $eventObj->getMembersByEventIDMini($events ,"Y");

            // build where clause
            $eventMembers = [];
            if (!empty ($eventDetails)) {
                foreach ($eventDetails as $eventDetail) {
                    $eventMembers[] = $this->sos_dbo->quote($eventDetail['member_id']);
                }
            }
            if (!empty ($eventMembers)) {
                $whereClauses[] = ' id in (' . implode(',', $eventMembers) . ')';
            }

        }



        //-----------------------------------------------
        // Map Number
        //-----------------------------------------------

        $mapSql = '';
        $names = array();

        if (!empty($mapNumbers) && strtolower ($mapNumbers) != "all"){

            $mapValues = explode(",", $mapNumbers);
            $mapsQueries = array();

            foreach ($mapValues as $oneElement) {
                $oneElement = trim($oneElement);

                // check if int
                if (filter_var($oneElement, FILTER_VALIDATE_INT) !== false){
                    $mapsQueries[]= $this->sos_dbo->quote($oneElement);
                    $names[] = $oneElement;
                }
            }

            if (!empty($mapsQueries)) {
                $this->setOnlyParticipating();
                $mapSql = " map_number in (" . implode(',', $mapsQueries) . ")";
                $whereClauses[]= $mapSql;
                $localSearchName .= " Map : ". implode(", ", $names);
            }
        }

        if (empty($localSearchName)) {
            $localSearchName = '';
        }

        if (($eventlc == 'all') && (strtolower($mapNumbers) == 'all') && (strtolower($genre) == 'all')) {
            $this->topIncludes = 'blocks/listing_blurb.html.twig';
        } else if (strtolower($events)=="favorites"){

            $this->topIncludes = "blocks/itinerary_description.html.twig";
        }

        // Build Where.

        $whereClauseSql = '';
        if (! empty ($whereClauses)){
            $whereClauseSql = "WHERE ". implode(' and ',$whereClauses);

        }


        $sql = "SELECT {$this->publicColsStr} from {$this->lookupView} {$whereClauseSql} ORDER BY $this->sort";


        $stmt = $this->sos_dbo->prepare($sql);
        //$stmt->bindValue(1, $number, \PDO::PARAM_INT);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        //var_dump ($all_rows);


        $this->textSearchName = $localSearchName;

        return $all_rows;
    }



    //--------------------------------------------------------------
    // listToJson
    //
    // list to json format for maps

    //--------------------------------------------------------------
    // listToJson
    //
    // list to json format for maps

    public function listToJson ($participants, $mapnum_url, $profile_url, $attrs=null ){

        //public function getParticipantsMapDataJson (){



        $json = '{"markers":['; // start json

        $markerArray = array();  // array indexed by map numbers.

        foreach ($participants as $oneRow) {
            if ($oneRow['Participating'] = 'Y') {
                $mapNumber = htmlentities($oneRow['map_number']);


                $address_plus2 = htmlentities($oneRow['street_address']) . " , Somerville MA ";
                $address_plus3 = str_replace(" ", "+", $address_plus2);
                $address_plus = str_replace("#", " ", $address_plus3);

                $google_line = '<a ' . $attrs . '  href="http://maps.google.com/maps?f=q&hl=en&geocode=&q=' . $address_plus . '"> <em> [Directions (Google)] </em></a>';
                $business_name = $oneRow['BusinessName'];
                if ($business_name != '') {
                    $business_name = " (" . htmlentities($business_name) . ")";
                }
                $link_to_profile = '<a ' . $attrs . ' href="/' . $profile_url . htmlentities($oneRow['id']) . '"> ' . htmlentities($oneRow['PublicFirstName']) . ' ' . htmlentities($oneRow['PublicLastName']) . '</a>' . $business_name;


                if (!array_key_exists($mapNumber, $markerArray)) {
                    $temp_array = array();
                    $mapnum_link = "<a " . $attrs . "  href=\"/{$mapnum_url}{$oneRow['map_number']}/display_order=default\"><b>Map Number:  #{$oneRow['map_number']}</b></a>";
                    $street_address = $oneRow['street_address'];
                    if ($oneRow['building_name'] != '') {
                        $street_address = $oneRow['building_name'] . ", " . $street_address;
                    }
                    $temp_array['latitude'] = htmlentities($oneRow['lat']);
                    $temp_array['longitude'] = htmlentities($oneRow['lng']);
                    $temp_array['title'] = htmlentities($street_address);
                    $temp_array['content'] = "$mapnum_link<br><b><em>" . htmlentities($street_address) . "</em></b><br>" . $google_line . '<br>' . $link_to_profile;
                    $temp_array['map_number'] = htmlentities($oneRow['map_number']);
                    $temp_array['map_icon'] = "";

                    $markerArray[$mapNumber] = $temp_array;

                } else {


                    $markerArray[$mapNumber]['content'] .= ',<br>' . $link_to_profile;
                }

// If the map number is duplicate do nothing (if listing all artists names in the comment do that here....

            }
        }

        $json = '{"markers":['; // start json

        $isFirst = true;
        foreach ($markerArray as $i => $oneRow){
            if (!$isFirst) {
                $json .= ',';
            }
            $isFirst = false;
            $json .= json_encode($oneRow);
        }

        $json .= ']}';  // End json


        return ($json);

    }


    //---------------------------------------------
    // Itineray

    public function getClosestLocations($type, $lat, $long, $fridayNight="N"){

        global $sos_dbo;

        if ($fridayNight=='Y'){
            $sql = "SELECT `lat` ,  `lng` ,  SQRT( POW(( `lat` - :lat) , 2 ) + POW( ( `lng` - :long  ), 2 ) ) AS distance, id as mid, Active, id as id, Participating, MemberType, BusinessNameOption, BusinessName, FridayNight, Expanded_Web, publicFirstName, publicLastName, building_name, street_address, address_details, ShortDescription, Website, map_number  FROM current_participating_artists_view WHERE FridayNight='Y' ORDER BY distance limit 80";
            //var_dump ($sql);

        } else {
            $sql = "SELECT `lat` ,  `lng` ,  SQRT( POW(( `lat` - :lat) , 2 ) + POW( ( `lng` - :long  ), 2 ) ) AS distance, id as mid, Active, id as id, Participating, MemberType, BusinessNameOption, BusinessName, FridayNight, Expanded_Web, publicFirstName, publicLastName, building_name, street_address, address_details, ShortDescription, Website, map_number  FROM current_participating_artists_view ORDER BY distance limit 80";


        }

        $stmt = $sos_dbo->prepare ($sql);
        $stmt->bindParam(':lat', $lat , PDO::PARAM_INT);
        $stmt->bindParam(':long', $long , PDO::PARAM_INT);

        $stmt->execute();
        $all_rows= $stmt->fetchAll(PDO::FETCH_ASSOC);


        /* function onPositionUpdate(position)
           {
           var lat = position.coords.latitude;
           var lng = position.coords.longitude;
           alert("Current position: " + lat + " " + lng);
           }

           if(navigator.geolocation)
           navigator.geolocation.getCurrentPosition(onPositionUpdate);
           else
           alert("navigator.geolocation is not available");
        */

        //    echo $type;

        if ($type=="mobile_table"){
            $toreturn =($this->getParticipantsAsMobileTable ($all_rows));
        } elseif ($type=="mobile_map_json"){
            $toreturn= ($this->getParticipantsMapDataJsonMobile ($all_rows));

        }

        return $toreturn;
    }



    public function getClosestLocationsItin($type, $lat, $long,  $arrayOfIds, $fridayNight="N"){

        global $sos_dbo;

        $whereArray=array();
        foreach ($arrayOfIds as $oneID){
            $oID= htmlentities($oneID);
            $whereArray[] = " id = {$oID}";
        }
        $where =  "(". implode(" or ", $whereArray) .")";


        if ($lat==0 && $long==0) {


            if ($fridayNight=='Y'){

                $sql = "SELECT * FROM current_participating_artists_view WHERE {$where} and FridayNight='Y'";


            } else {
                $sql = "SELECT `lat` ,  `lng` ,  id as mid, Active, id as id, Participating, MemberType, BusinessNameOption, BusinessName, FridayNight, Expanded_Web, PublicFirstName, PublicLastName, building_name, street_address, address_details, ShortDescription, Website, map_number FROM current_participating_artists_view WHERE {$where}";

            }
            $stmt = $sos_dbo->prepare ($sql);

        } else {   // USE LAT LONG


            if ($fridayNight=='Y'){

                $sql = "SELECT `lat` ,  `lng` ,  SQRT( POW(( `lat` - :lat) , 2 ) + POW( ( `lng` - :long  ), 2 ) ) AS distance, id as mid, Active, id as id, Participating, MemberType, BusinessNameOption, BusinessName, FridayNight, Expanded_Web, PublicFirstName, PublicLastName, building_name, street_address, address_details, ShortDescription, Website, map_number FROM  current_participating_artists_view WHERE {$where} and FridayNight='Y' ORDER BY distance limit 80";

            } else {
                $sql = "SELECT `lat` ,  `lng` ,  SQRT( POW(( `lat` - :lat) , 2 ) + POW( ( `lng` - :long  ), 2 ) ) AS distance, id as mid, Active, id as id, Participating, MemberType, BusinessNameOption, BusinessName, FridayNight, Expanded_Web, PublicFirstName, PublicLastName, building_name, street_address, address_details, ShortDescription, Website, map_number FROM current_participating_artists_view WHERE {$where} ORDER BY distance limit 80";


            }

//echo "<br>$sql</br>";

            $stmt = $sos_dbo->prepare ($sql);
            $stmt->bindParam(':lat', $lat , PDO::PARAM_INT);
            $stmt->bindParam(':long', $long , PDO::PARAM_INT);

        }



        $stmt->execute();
        $all_rows= $stmt->fetchAll(PDO::FETCH_ASSOC);


        /* function onPositionUpdate(position)
           {
           var lat = position.coords.latitude;
           var lng = position.coords.longitude;
           alert("Current position: " + lat + " " + lng);
           }

           if(navigator.geolocation)
           navigator.geolocation.getCurrentPosition(onPositionUpdate);
           else
           alert("navigator.geolocation is not available");
        */

        //    echo $type;

        if ($type=="mobile_table"){
            $toreturn =($this->getParticipantsAsMobileTable ($all_rows));
        } elseif ($type=="mobile_map_json"){
            $toreturn= ($this->getParticipantsMapDataJsonMobile ($all_rows));

        }

        return $toreturn;
    }


    public function getArtistsbyItinList($arrayOfIds, $fridayNight = "N") {

        $this->topIncludes = "blocks/itinerary_description.html.twig";
        $where = \SOS\SQLHelper::arrayToQuotedString($this->sos_dbo, $arrayOfIds);

        if ($fridayNight == 'Y') {

            $sql = "SELECT   * FROM current_active_artists_view WHERE id in ({$where}) and FridayNight='Y'";


        } else {

            $sql = "SELECT  * FROM current_active_artists_view WHERE  id in ({$where}) ORDER BY $this->sort";

        }

        $stmt = $this->sos_dbo->prepare($sql);

        if ($stmt->execute()) {
            $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        } else {

            $all_rows = array();

            if (\SOSModels\Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }

        }

        return $all_rows;
    }




}