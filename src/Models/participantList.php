<?php

namespace SOSModels;

// This is from the older SOS site.. But it generates a list of artists as JSON... So we keep.
// Probably overkill, but we keep.

class participantList {

    private $participantList = null;

    function __construct($sql, $mapnum_url, $profile_url, $attrs=null) {
        $this->sql = $sql;
        // SQL statement to generate the participant list
        $this->mapnum_url = $mapnum_url;
        // URL for a "by map number" listing page.  Map number will be appended to this
        $this->profile_url = $profile_url;
        // URL for an artist profile page.  Artist ID will be appended to this
        $this->attrs = $attrs;
        // additional attributes (such as target="_blank") to add to <a> tags
    }


    //-----------------------------
    // get participants

    public function getParticipants (){


        if (is_null($this->participantList)){

            global $sos_dbo;  // get datasource from global pool
            $stmt = $sos_dbo->prepare ($this->sql);
            $stmt->execute();
            // store away so it doesn't need to be retreived again
            $this->participantList =  $stmt->fetchAll();
        }

        return $this->participantList;
    }


    //------------------------------------------------
    public function getParticipantsMapDataJson (){

        $participants = $this->getParticipants();


        $json = '{"markers":['; // start json

        $markerArray = array();  // array indexed by map numbers.

        foreach ($participants as $oneRow){

            $mapNumber= htmlentities($oneRow['map_number']);




            $address_plus2= htmlentities($oneRow['street_address']) ." , Somerville MA ";
            $address_plus3 = str_replace(" ", "+",$address_plus2 );
            $address_plus  = str_replace("#", " ",$address_plus3 );

            $google_line= '<a '. $this->attrs . '  href="http://maps.google.com/maps?f=q&hl=en&geocode=&q='.$address_plus .'"> <em> [Directions (Google)] </em></a>';
            $business_name = $oneRow['BusinessName'];
            if ($business_name != '') {
                $business_name = " (".htmlentities($business_name).")";
            }
            $link_to_profile = '<a ' . $this->attrs . ' href="'.$this->profile_url. htmlentities($oneRow['id']).'"> '.htmlentities($oneRow['PublicFirstName']).' ' .htmlentities($oneRow['PublicLastName']).'</a>'.$business_name;


            if (!array_key_exists($mapNumber,$markerArray )){
                $temp_array= array();
                $mapnum_link = "<a " . $this->attrs ."  href=\"{$this->mapnum_url}{$oneRow['map_number']}\"><b>Map Number:  #{$oneRow['map_number']}</b></a>";
                $street_address = $oneRow['street_address'];
                if ($oneRow['building_name'] != '') {
                    $street_address = $oneRow['building_name'] . ", " . $street_address;
                }
                $temp_array['latitude'] = htmlentities($oneRow['lat']);
                $temp_array['longitude'] = htmlentities($oneRow['lng']);
                $temp_array['title'] = htmlentities($street_address);
                $temp_array['content'] = "$mapnum_link<br><b><em>".htmlentities($street_address)."</em></b><br>".$google_line.'<br>'.$link_to_profile ;
                $temp_array['map_number']= htmlentities($oneRow['map_number']);
                $temp_array['map_icon'] ="";

                $markerArray[$mapNumber]=$temp_array;

            } else {


                $markerArray[$mapNumber]['content'] .= ',<br>'.$link_to_profile  ;
            }

// If the map number is duplicate do nothing (if listing all artists names in the comment do that here....

        }




        $json = '{"markers":['; // start json

        foreach ($markerArray as $oneRow){

            $json .= json_encode($oneRow) .',' ;

        }

        $json .= ']}';  // End json


        return ($json);
    }



    //------------------------------------------------
    public function getParticipantsAsTable (){

        $sos_thumb_path_base = "../artist_files/artist_images/splash/";
        $sos_image_hoster = "http://images.somervilleopenstudios.org";


        $thumb_path = "{$sos_image_hoster}{$sos_thumb_path_base}";

        $participants = $this->getParticipants();


        $html = '    
    
    


    ';
        $html .= "
    <script type=\"text/javascript\">    
      $(document).ready(function( ) {
        $('table.sortable').tablesorter({
        widgets:['zebra']
        });
        
        var rows =  $('table.artists_table tbody tr');
        
        
      //hightlight on mouse over
      rows.mouseover(function(){
           $(this).addClass('artists_table_highlight');
      });

      //remove hightlight on mouse over
      rows.mouseout(function(){
        $(this).removeClass('artists_table_highlight');
      });
        
        
      });
</script>
";


        $html .= '<table border=0 class="artists_table sortable">
    
      <thead>
          <tr>
            <th scope="col">&nbsp </th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Address</th>
            <th scope="col">Description</th>
            <th scope="col">Map<br>Number</th>
          </tr>
          </thead>
          <tbody>
     ';






        foreach ($participants as $oneRow){


            $html .= "<tr>";
// Get image


            $temp_filename = artistsImageClass::getSplashImage($oneRow['id']);

            $html.= '<td> <img src = "'.$temp_filename  .'" height="50" width = "50" alt="'. htmlentities($oneRow['PublicFirstName']) .'"></td>';




//rest of row

            $hrefStart = "<a href=\"". $this->profile_url . htmlentities($oneRow['id'])  ."\">";
            $mapnum_link = "<a href=\"{$this->mapnum_url}{$oneRow['map_number']}\">{$oneRow['map_number']}</a>";

            $html .= "<td>{$hrefStart}".htmlentities($oneRow['PublicFirstName'])."</a> </td><td>{$hrefStart}".htmlentities($oneRow['PublicLastName'])." </a></td>";
            $html .= "<td>".htmlentities($oneRow['BusinessName'])."<br/> ".htmlentities($oneRow['building_name'])." <br> ".htmlentities($oneRow['street_address'])." ".htmlentities($oneRow['address_details'])."</td>";
            $html .= "<td>".htmlentities($oneRow['ShortDescription'])." </td>";
            $html .= "<td>$mapnum_link</td>";

            $html .= "</a></tr>";
        }
        $html .= "</tbody></table>";

        return ($html);
    }



}

?>