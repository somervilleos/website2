<?php

namespace SOSModels;

// This is from the older SOS site.. But it generates a list of artists as JSON... So we keep.
// Probably overkill, but we keep.

use \SOS\SQLHelper;

class ProfileSocialMedia
{

    public $profileUpdateFields; // fields able to be updated by this class

// constructor.
    public function __construct(\PDO $dbo)
    {
        $this->sos_dbo = $dbo;


        //
        // these are the only field that can be updated by this class.
        // The index key is the table that can be updated
        // in this case only one table is involved, but for commonality..


        $this->profileUpdateFields = array('profile_social_media'=>array(
            'artist_statement',
            'HandicapAccessible',
            'ShortDescription',
            'PublicLastName',
            'PublicFirstName',
            'BusinessName',
            'OrganizationDescription',
            'address_details',
            'use_images_for_promotion',
            'home_studio'),
        'member'=>array(
            'FashionInfo',
            'FridayNight'
        )
        );


        $this->tableToWhereColumn = array ('profile'=>'member_id', 'member'=>'id');
    }


    public function getSocialMediaColumn (){

        $socialMediaColumns = array('Short Name' => 'short_name',
            'Link' => 'url',
            'User Name' => 'user_name'
        //,'Image' => 'image_name'

        );

        return  $socialMediaColumns;
    }

    public function getSocailMediaByType ($type) {


    }




    public function getSocailMediaWithBlanks ($member_id){

       $sql = <<<EOF
          SELECT * FROM `profile_social_media`
           RIGHT JOIN `social_media_templates` 
           ON profile_social_media.short_name = social_media_templates.short_name 
           and profile_social_media.member_id = :member_id ;
EOF;
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":member_id", $member_id, \PDO::PARAM_STR);
        // run the insert query
        if ($stmt->execute()) {
            $socialMediaData = $stmt->fetchAll(\PDO::FETCH_ASSOC );
        } else {

            if (\SOSModels\Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            $socialMediaData = array();
        }


        $socialMediaData = $this->buildURLs($socialMediaData,false);
        return $socialMediaData;
    }


    public function getSocailMedia ($member_id){

        $sql = <<<EOF
            SELECT psm.short_name, member_id, user_name ,url_full ,
            social_media_id, 
            url_template ,
            image_name ,
            description, 
            social_media_templates.short_name ,
            social_media_templates.display_text,
            social_media_templates.fa_name
                         
            FROM `profile_social_media` psm
                  LEFT JOIN `social_media_templates`
                    ON psm.short_name  = social_media_templates.short_name
            WHERE psm.member_id = :member_id
            
            ORDER BY `order`;

EOF;
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":member_id", $member_id, \PDO::PARAM_STR);
        // run the  query
        if ($stmt->execute()) {
            $socialMediaData = $stmt->fetchAll(\PDO::FETCH_ASSOC|\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE);
        } else {

            if (\SOSModels\Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            $socialMediaData = array();
        }

        $socialMediaData = $this->buildURLs($socialMediaData);
        return $socialMediaData;
    }



    public function buildURLs ($socialMediaData, $remove_blanks = true){

        if (empty($socialMediaData)){

            return null;

        }


        foreach ($socialMediaData as $key => $oneMedia){

            // do we need to position

            if (!empty ($oneMedia['user_name'])  && strpos($oneMedia['url_template'], '{}') !== false) {

                //add http:// if its not there
                $weblinks = ['website', 'other', 'online_shop'];

                if (in_array($oneMedia['short_name'], $weblinks)  && (strpos($oneMedia['user_name'], 'http') !== 0 )){

                    $socialMediaData[$key] ['url'] = 'https://'. $oneMedia['user_name'];
                } else {
                    $socialMediaData[$key] ['url'] = str_replace('{}', $oneMedia['user_name'], $oneMedia['url_template']);
                }
            } else {
                if ($remove_blanks){
                    unset ($socialMediaData[$key]);
                } else {
                    $socialMediaData[$key] ['url'] = null;
                }
            }
        }

        return $socialMediaData;
    }



    public function updateFromArray($newData, $member_id)
    {

        $status = array("status" => "passed", 'info_messages' => array(), 'error_messages'=>array());

        $sql = "REPLACE INTO `profile_social_media` (`member_id`, `short_name`, `user_name`) VALUES 
         (:member_id, :type, :value)";




        // $fieldNames = array ('profile_id', 'member_id', 'location_id', 'HandicapAccessible', 'ShortDescription', 'PublicLastName', 'PublicFirstName', 'BusinessName', 'BusinessNameSortKey', 'OrganizationDescription', 'address_details', 'use_images_for_promotion', 'home_studio');
        foreach ( $newData as  $type=>$value) {


            // do some sanity checks.
            if (substr($value,0,1)==="@"){
                $status['info_messages'][] = "Removed @ from begining of user name.  Check link for $type";
                $value2 = substr($value,1);
            } else {
                $value2= $value;
            }
            if (substr($value,0,4)==="http" && ($type!="website" && $type!= "other" && $type!= "online_shop" && $type!= "mastodon")){
                $status['error_messages'][] = "http is not a valid username. Use just the username for $type";
                continue;
            }


            // bind the input fields to the sql
            $stmt = $this->sos_dbo ->prepare ($sql);
            $stmt->bindParam(':member_id', $member_id , \PDO::PARAM_STR);
            $stmt->bindParam(':type', $type , \PDO::PARAM_STR);
            $stmt->bindParam(':value', $value2, \PDO::PARAM_STR);

            // run the insert query
            if ($stmt->execute()) {
                if (!empty($value)) {
                    $status['info_messages'][] = "+++ Updated OK: $type";
                }
            } else {
                $status['status'] = "failed";
                $status['error_messages'][] = "+++ Updated Failed";
                if (\SOSModels\Globals::$sql_debug) {
                    echo "\nPDO::errorInfo():\n";
                    print_r($stmt->errorInfo());
                }
            }
        }

        return $status;

    }


}