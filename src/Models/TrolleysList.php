<?php
/**
 * User: rraman
 */

namespace SOSModels;
use Silex\Application;


class TrolleysList
{

    private $sos_dbo;
    private $sort;
    private $lookupView;

    public function __construct(\PDO $dbo)
    {
        $this->sos_dbo = $dbo;
        $this->sort = "map_number";
        $this->lookupView = 'trolley_info';
    }

//     public function setSort($col)
//     {
//         $this->sort = $col;
//     }


    //resest session
    // for some reason this will log out a user...

//     public static function resetSearchIds (Application $app){
//
//
//         $app['session']->set('searchList', null);
//         $app['session']->set('searchName', null);
//         $app['session']->set('searchURL', null);
//
//         $app['session']->remove('searchList');
//         $app['session']->remove('searchName');
//         $app['session']->remove('searchURL');
//
//
//     }





//     public function setOnlyParticipating()
//     {
//         $this->lookupView = 'current_participating_artists_view';
//     }

    //--------------------------------------------------------------
    // Get All
    //

    public function getAll() {

        $sql = "SELECT * from {$this->lookupView} WHERE `lat` BETWEEN 42.37 AND 42.42 AND `lng` BETWEEN -71.14 AND -71.07 ORDER BY $this->sort";

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $all_rows;

    }

    public function update($num, $lat, $lng) {
        $sql = "UPDATE `trolley_info` SET `lat` = :lat, `lng` = :lng, `modified` = NOW() WHERE `map_number` = :num;";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(':lat', $lat, \PDO::PARAM_STR);
        $stmt->bindValue(':lng', $lng, \PDO::PARAM_STR);
        $stmt->bindValue(':num', $num, \PDO::PARAM_STR);
        $stmt->execute();
    }

    public function updateJson() {
        $sql = "SELECT * from {$this->lookupView} WHERE type='trolley' ORDER BY $this->sort";

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->execute();
        $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $resp = [];
        foreach ($all_rows as $oneRow) {
            $resp[$oneRow['description']] = [
                'lat' => $oneRow['lat'],
                'lng' => $oneRow['lng'],
                'modified' => $oneRow['modified']
            ];
        }

        return json_encode($resp);
    }

    //--------------------------------------------------------------
    // Get By Random
    //

//     public function getRandom($number) {
//         $sql = "SELECT * from {$this->lookupView} ORDER BY RAND() Limit ?";
//
//         $stmt = $this->sos_dbo->prepare($sql);
//         $stmt->bindValue(1, $number, \PDO::PARAM_INT);
//         $stmt->execute();
//         $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//         //shuffle ($all_rows);
//         return $all_rows;
//
//     }

    //--------------------------------------------------------------
    // Get By Map Number
    //

//     public function getByMapNumber($mapNumber) {
//         $mapNumber = (int)$mapNumber;
//         if (! is_int($mapNumber)) {
//             // don't allow access to arbitrary columns of database table!
//             print 'Not integer.  map numbner must be numeric';
//         }
//
//         $sql = "SELECT * FROM current_participating_artists_view where map_number = :mapnum ORDER BY $this->sort";
//         $stmt = $this->sos_dbo->prepare($sql);
//         $stmt->bindValue(':mapnum', $mapNumber, \PDO::PARAM_INT);
//         $stmt->execute();
//         $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//         return $all_rows;
//     }

    //--------------------------------------------------------------
    // Get By Letter
    //

//     public function getByLetter($letter) {
//         $pattern = $this->sos_dbo->quote($letter . "%");
//
//         // remove the from return order for business names
//
//         $the_pattern = $this->sos_dbo->quote("The " . $letter . "%");
//
//         $sql = "(SELECT *, PublicLastName as sortKey "
//             . "FROM current_active_artists_view WHERE PublicLastName like $pattern ) UNION ";
//
//         $sql .= "(SELECT *, TRIM(LEADING 'The ' FROM BusinessName) as sortKey "
//             . "FROM current_active_artists_view "
//             . "WHERE (BusinessName like $pattern OR BusinessName like $the_pattern) "
//             . "HAVING sortKey like $pattern) " ;
//         $sql .= "ORDER BY sortKey, PublicFirstName";
//
//         $stmt = $this->sos_dbo->prepare($sql);
//         $stmt->execute();
//         $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//         return $all_rows;
//     }
    //--------------------------------------------------------------
    // Get list by Genre (aka Medium)
    //

//     public function getByGenre($genre)  {
//
//         if (! isset(\SOSModels\ArtistsGenreList::$column_name_to_genre_name[$genre] )) {
//             // don't allow access to arbitrary columns of database table!
//             return 'No Such type';
//         }
//
//         $sql = "SELECT * FROM {$this->lookupView} where `$genre` = 'Y' ORDER BY $this->sort";
//         $stmt = $this->sos_dbo->prepare($sql);
//         //$stmt->bindValue(1, $number, \PDO::PARAM_INT);
//         $stmt->execute();
//         $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//         return $all_rows;
//     }

    //--------------------------------------------------------------
    // Get Search Term

//     public function getBySearchTerm($search)
//     {
//
//         $nameList = preg_split('/\s+/', $search);
//         $pattern0 = $this->sos_dbo->quote("%" . $nameList{0} . "%");
//         if (isset ($nameList{1}) && $nameList{1} != "") {
//             $pattern1 = $this->sos_dbo->quote("%" . $nameList{1} . "%");
//             $sql = <<<EOL
//      SELECT * FROM current_active_artists_view where
//         PublicFirstName like $pattern0
//      or PublicFirstName like $pattern1
//      or PublicLastName like $pattern0
//      or PublicLastName like $pattern1
//      or building_name  like $pattern0
//      or building_name  like $pattern1
//      or Website  like $pattern0
//      or Website  like $pattern1
//      or street_address  like $pattern0
//      or street_address  like $pattern1
//      or address_details  like $pattern0
//      or address_details  like $pattern1
//      or ShortDescription  like $pattern0
//      or ShortDescription  like $pattern1
//      or BusinessName  like $pattern0
//      or BusinessName  like $pattern1
//
// EOL;
//         } else {
//             $sql = <<<EOL
//       SELECT * from current_active_artists_view where
//         PublicFirstName like $pattern0
//      or PublicLastName like $pattern0
//      or building_name  like $pattern0
//      or Website  like $pattern0
//      or street_address  like $pattern0
//      or address_details  like $pattern0
//      or ShortDescription  like $pattern0
//      or BusinessName  like $pattern0
//
// EOL;
//         }
//
//         $sql .= "ORDER BY $this->sort";
//         $stmt = $this->sos_dbo->prepare($sql);
//         //$stmt->bindValue(1, $number, \PDO::PARAM_INT);
//         $stmt->execute();
//         $all_rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//
//         return $all_rows;
//
//
//     }




//     public function getArtistListMuliti ($genre, $events, $map_number, $orderby, $glue)
//     {
//
//         // so any of these values can contain the word "All" this might mess up if a search is for "all" but we'll let it go.
//
//         // glue can is and:or/
//         // its always "and events" and "and "map"
//
//         $genreSql = '';
//         if ($genre != "All") {
//             $genreValues = explode(",", $genre);
//             $genreArray = array();
//             var_dump ($genreValues);
//             // we have a fixed volabulary for genres so...
//
//             foreach ($genreValues as $oneElement) {
//                 $oneElement = trim($oneElement);
//                 if (!isset(\SOSModels\ArtistsGenreList::$column_name_to_genre_name[$oneElement])) {
//                     $errors[] = "Genre not found $oneElement";
//                 } else {
//                     echo "adding $oneElement<br>";
//                     $genreArray[] = " `$oneElement` = 'Y' ";
//                 }
//             }
//             $genreSql = implode("or", $genreArray);
//         }
//
//
//         // build events list
//         $validEvents = array('friday'=>'fridayNight', 'fashion'=>'inside_out', 'first_look'=>'first_look_show');
//
//
//         $eventsSql ='';
//         if (! empty($events)){
//
//             $eventValues = explode(",", $events);
//             $genreArray = array();
//             var_dump ($eventValues);
//             // we have a fixed volabulary for genres so...
//             $eventsQueries = array();
//
//             foreach ($eventValues as $oneElement) {
//                 $oneElement = trim($oneElement);
//                 if (array_key_exists($oneElement, $validEvents)){
//
//                     if (!empty($validEvents[$oneElement])){
//                         $eventsQueries[]= " `$oneElement` = 'Y' ";
//                     } else if ($validEvents == 'itinerary'){
//
//
//                     } else {
//
//                     }
//
//                 }
//             }
//
//             $eventSql = implode("or", $genreArray);
//
//         }
//
//
//
//
//     var_dump ($genre, $genreSql);
//
//
//
//
//     }





    //--------------------------------------------------------------
    // listToJson
    //
    // list to json format for maps

    public function listToJson ($items/*, $mapnum_url, $profile_url, $attrs=null */){

        //public function getParticipantsMapDataJson (){



            $json = '{"markers":['; // start json

            $markerArray = array();  // array indexed by map numbers.

            foreach ($items as $oneRow){

                $mapNumber= htmlentities($oneRow['map_number']);




                //$address_plus2= htmlentities($oneRow['street_address']) ." , Somerville MA ";
                //$address_plus3 = str_replace(" ", "+",$address_plus2 );
                //$address_plus  = str_replace("#", " ",$address_plus3 );

                //$google_line= '<a '. $attrs . '  href="http://maps.google.com/maps?f=q&hl=en&geocode=&q='.$address_plus .'"> <em> [Directions (Google)] </em></a>';
                //$business_name = $oneRow['BusinessName'];
                //if ($business_name != '') {
                //    $business_name = " (".htmlentities($business_name).")";
                //}
                //$link_to_profile = '<a ' . $attrs . ' href="/'.$profile_url. htmlentities($oneRow['id']).'"> '.htmlentities($oneRow['PublicFirstName']).' ' .htmlentities($oneRow['PublicLastName']).'</a>'.$business_name;


                //if (!array_key_exists($mapNumber,$markerArray )){
                    $temp_array= array();
                    //$mapnum_link = "<a " . $attrs ."  href=\"/{$mapnum_url}{$oneRow['map_number']}\"><b>Map Number:  #{$oneRow['map_number']}</b></a>";
                    //$street_address = $oneRow['street_address'];
                    //if ($oneRow['building_name'] != '') {
                    //    $street_address = $oneRow['building_name'] . ", " . $street_address;
                    //}
                    $temp_array['latitude'] = htmlentities($oneRow['lat']);
                    $temp_array['longitude'] = htmlentities($oneRow['lng']);
                    $temp_array['title'] = htmlentities($oneRow['description']);
                    $temp_array['content'] = $temp_array['title'];
                    // rraman: 2022 - use green icons instead of numbered pins
                    // $temp_array['map_number']= htmlentities($oneRow['map_number']);
                    // $temp_array['color'] = ($oneRow['type'] == 'trolley' ? 'orange' : 'blue'); // rraman: since we're merging trolleys with artists on one map, make orange
                    $temp_array['map_icon'] = "trolley_icon_green.png";
                    $temp_array['modified'] = htmlentities($oneRow['modified']);

                    $markerArray[$mapNumber]=$temp_array;

                //} else {


                //    $markerArray[$mapNumber]['content'] .= ',<br>'.$link_to_profile  ;
                //}

// If the map number is duplicate do nothing (if listing all artists names in the comment do that here....

            }


            $json = '{"markers":['; // start json

            $isFirst = true;
            foreach ($markerArray as $oneRow){
                if (!$isFirst) {
                    $json .= ',';
                }
                $isFirst = false;
                $json .= json_encode($oneRow);
            }

            $json .= ']}';  // End json


            return ($json);

    }


}
