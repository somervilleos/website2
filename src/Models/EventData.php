<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/3/15
 * Time: 12:56 AM
 */

namespace SOSModels;


use PDO;

class EventData {

    private $sos_dbo;
    private $year;


    public function __construct(PDO $dbo, $year) {
        $this->sos_dbo = $dbo;
        $this->year = $year;

    }



    /* 8:10:36 PM localhost somerville_openstudios21  SELECT * FROM `events` WHERE `event_id` IN (1013,1017,1018,1019,1020); */




    /*
        static function init(\PDO $dbo , $memberID){
        return $stuff ? new self(\PDO $dbo , $memberID) : false;
    }
    */
    /**
     *
     *  Get Events for a user.  Used For ADMIN pages as will show events not marked as public yet..
     *
     * @return array of sponship data
     *
     */


    public function getEventListingAll($memberID, $approved = "Any") {

        $q = '';
        if ($approved == "Y") {
            $q = 'and emj.approved_selection ="Y" ';
        } else if ($approved == "N") {
            $q = 'and emj.approved_selection ="N" ';
        }


        $sql = "select emj.* , e.event_id,e.event_name, e.time_description, e.location_description
from events_member_join emj 
inner join events e
on emj.event_id = e.event_id
where emj.member_id = :member_id  {$q}  
";
        $stmt = $this->sos_dbo->prepare($sql);

        $stmt->bindValue(":member_id", $memberID, PDO::PARAM_STR);

        // run the query
        if ($stmt->execute()) {
            $eventInfo = $stmt->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP | PDO::FETCH_UNIQUE);
        } else {

            if (Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            $socialMediaData = array();
        }

        $indexByEvent = array();

        if (!empty($eventInfo)) {
            foreach ($eventInfo as $oneEvent) {
                $key = $oneEvent['event_id'];
                $indexByEvent[$key] = $oneEvent;
            }

        }


        return $indexByEvent;
    }


    /**
     *
     *  Get Events for a user.  Used For WEB pages as will show only events marked as public.
     *  uses "make_list_public" field
     * @return array of sponship data
     *
     */


    public function getPublicEventListing($memberID, $approved = "Any") {

        $q = '';
        if ($approved == "Y") {
            $q = 'and emj.approved_selection ="Y" ';
        } else if ($approved == "N") {
            $q = 'and emj.approved_selection ="N" ';
        }


        $sql = "select emj.* , e.event_id, e.event_url, e.event_name, e.time_description, e.location_description
from events_member_join emj 
inner join events e
on emj.event_id = e.event_id
where emj.member_id = :member_id  {$q}  and make_list_public = 'Y' and show_in_profile='Y' and e.year=:year order by display_order
";
        $stmt = $this->sos_dbo->prepare($sql);

        $stmt->bindValue(":member_id", $memberID, PDO::PARAM_STR);
        $stmt->bindValue(":year", $this->year, PDO::PARAM_STR);

        // run the  query
        if ($stmt->execute()) {
            $eventInfo = $stmt->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP | PDO::FETCH_UNIQUE);
        } else {

            if (Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            $socialMediaData = array();
        }

        $indexByEvent = array();

        if (!empty($eventInfo)) {
            foreach ($eventInfo as $oneEvent) {
                $key = $oneEvent['event_id'];
                $indexByEvent[$key] = $oneEvent;
            }

        }


        return $indexByEvent;
    }


    /*
     *
     * For a given Event Show users for an event.
     *
     */


    public function getMembersByEventID($eventID, $approved = "Any") {

        $q = '';
        if ($approved == "Y") {
            $q = 'and emj.approved_selection ="Y" ';
        } else if ($approved == "N") {
            $q = 'and emj.approved_selection ="N" ';
        }


        $sql = "SELECT   *  FROM current_active_artists_view av
        inner Join  events_member_join emj 
         on av.member_id = emj.member_id
        inner Join  events e
         on e.event_id = emj.event_id


        where emj.event_id = :event_id  {$q}
        ";

        print nl2br($sql);

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":event_id", $eventID, PDO::PARAM_STR);


        // run the  query
        if ($stmt->execute()) {
            $eventInfo = $stmt->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP | PDO::FETCH_UNIQUE);
        } else {

            if (Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            return [];
        }

        return $eventInfo;

    }


    // Only return subset of member info (just ID)
    public function getMembersByEventIDMini($eventID, $approved = "Any") {

        $q = '';
        if ($approved == "Y") {
            $q = 'and emj.approved_selection ="Y" ';
        } else if ($approved == "N") {
            $q = 'and emj.approved_selection ="N" ';
        }


        $sql = "SELECT    av.member_id,av.PublicFirstName,av.PublicLastName, av.BusinessName, emj.*   FROM current_active_artists_view av
        inner Join  events_member_join emj 
         on av.member_id = emj.member_id
        inner Join  events e
         on e.event_id = emj.event_id


        where emj.event_id = :event_id  {$q}
        ";


        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":event_id", $eventID, PDO::PARAM_STR);


        // run the  query
        if ($stmt->execute()) {
            $eventInfo = $stmt->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP | PDO::FETCH_UNIQUE);
        } else {

            if (Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            return [];
        }

        return $eventInfo;

    }






    /* Get the event row (Event Details) for a given row id */


    public function getEventDetails($eventID) {

        $sql = 'SELECT * FROM `events` WHERE `event_id` = :event_id';

        $stmt = $this->sos_dbo->prepare($sql);

        $stmt->bindValue(":event_id", $eventID, PDO::PARAM_STR);

        // run the insert query
        if ($stmt->execute()) {
            $eventInfo = $stmt->fetch(PDO::FETCH_ASSOC | PDO::FETCH_GROUP | PDO::FETCH_UNIQUE);
        } else {

            if (Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            return [];
        }

        return $eventInfo;
    }







// Add New!

///* INSERT INTO `events_member_join` (`id`, `member_id`, `event_id`, `approved`, `the_date`) VALUES ('6', '592', '1016', 'N', NULL);

    public function addOrUpdate($memberId, $eventID, $userSelection) {

        $status = array('status' => 'ok', 'message' => '');

        $eventDetails = $this->getEventDetails($eventID);

        if (empty($eventID)) {
            $status['status'] = "failed";
            $status['message'] = "Event Not Found";
            return $status;
        }


        // see if user already has a record for the event.  See if the same. Then do nothing. Save the date
        $memberEvents = $this->getEventListingAll($memberId, $eventID);


        $theDate = date("Y-m-d H:i:s");

        if (!array_key_exists($eventID, $memberEvents)) {

            // New!!  Add it.
            // figure out if


            $selection = 'N';

            // If the event are auto signup, then set the sign up to the value selected.
            //if ($eventDetails ['auto_signup'] == 'Y') {
             //   $selection = $userSelection;
           // }

            $sql = "INSERT INTO `events_member_join` (`id`, `member_id`, `event_id`, `member_selection`, `approved_selection`, `update_date`, `creation_date`) VALUES (NULL, :member_id, :event_id , :user_selection, :selection, :date, :date2);
";
            $stmt = $this->sos_dbo->prepare($sql);

            $stmt->bindValue(":event_id", $eventID, PDO::PARAM_STR);
            $stmt->bindValue(":member_id", $memberId, PDO::PARAM_STR);
            $stmt->bindValue(":user_selection",$userSelection , PDO::PARAM_STR);
            $stmt->bindValue(":selection", $selection, PDO::PARAM_STR);
            $stmt->bindValue(":date", $theDate, PDO::PARAM_STR);
            $stmt->bindValue(":date2", $theDate, PDO::PARAM_STR);


            // run the insert query
            if ($stmt->execute()) {


            } else {

                if (Globals::$sql_debug) {
                    print "SQL: $sql ";
                    echo "\nPDO::errorInfo():\n";
                    print_r($stmt->errorInfo());
                }
                return [];
            }

        } else {

            // check if different,  update as required.



            if ($memberEvents[$eventID]['member_selection'] != $userSelection) {


                // revert the selection back to 'N'if it must be approved.

                $selection ="N";

                // If the event are auto signup, then set the sign up to the value selected.
               // if ($eventDetails ['auto_signup'] == 'Y') {
                //    $selection = $userSelection;
               // }



                $sql = "UPDATE `events_member_join` SET `member_selection` = :user_selection, 
                                approved_selection = :selection,
                                update_date = :date
                                WHERE `member_id` = :member_id and event_id = :event_id  ;";


                $stmt = $this->sos_dbo->prepare($sql);

                $stmt->bindValue(":event_id", $eventID, PDO::PARAM_STR);
                $stmt->bindValue(":member_id", $memberId, PDO::PARAM_STR);
                $stmt->bindValue(":user_selection", $userSelection, PDO::PARAM_STR);
                $stmt->bindValue(":selection", $selection, PDO::PARAM_STR);
                $stmt->bindValue(":date", $theDate, PDO::PARAM_STR);

                // run the insert query
                if ($stmt->execute()) {
                    $status = array('status' => 'ok', 'message' => 'updated event ' . $eventDetails['event_name'] . "to " . $userSelection);

                } else {

                    if (Globals::$sql_debug) {
                        echo "\nPDO::errorInfo():\n";
                        print_r($stmt->errorInfo());
                    }
                    return [];
                }
            } else {

                print'';
            }
        }

        return $status;
    }



    public function updateApprovedSelection ($memberId, $eventID, $selection) {


        $status = array('status' => 'ok', 'message' => '');

        $eventDetails = $this->getEventDetails($eventID);

        if (empty($eventID)) {
            $status['status'] = "failed";
            $status['message'] = "Event Not Found";
            return $status;
        }


        // see if user already has a record for the event.  See if the same. Then do nothing. Save the date
        $memberEvents = $this->getEventListingAll($memberId, $eventID);


        $theDate = date("Y-m-d H:i:s");

        if (array_key_exists($eventID, $memberEvents)) {
            // check if different,  update as required.


            if ($memberEvents[$eventID]['approved_selection'] != $selection) {

                // revert the selection back to 'N'if it must be approved.


                $sql = "UPDATE `events_member_join` SET 
                                approved_selection = :selection,
                                update_date = :date
                                WHERE `member_id` = :member_id and event_id = :event_id  ;";


                $stmt = $this->sos_dbo->prepare($sql);

                $stmt->bindValue(":event_id", $eventID, PDO::PARAM_STR);
                $stmt->bindValue(":member_id", $memberId, PDO::PARAM_STR);
                $stmt->bindValue(":selection", $selection, PDO::PARAM_STR);
                $stmt->bindValue(":date", $theDate, PDO::PARAM_STR);

                // run the insert query
                if ($stmt->execute()) {
                    $status = array('status' => 'ok', 'message' => 'updated event ' . $eventDetails['event_name'] . "to " . $selection);

                } else {
                    $status = array('status' => 'query Didnt work', 'message' => '');

                    if (Globals::$sql_debug) {
                        echo "\nPDO::errorInfo():\n";
                        print_r($stmt->errorInfo());
                    }
                    return [];
                }
            } else {

                print'';
            }
        }
    }



     /**
      *   return all events approved for all acitve artists.
      *
      */




     public function getAllEventsApprovedArtists(){


            $sql = "SELECT emj.member_id, emj.event_id, emj.approved_selection,	emj.exception
FROM `events_member_join` emj
inner join events e
on emj.event_id = e.event_id
WHERE e.year = :year and emj.approved_selection = 'Y'  and e.make_list_public = 'Y'
";



         $stmt = $this->sos_dbo->prepare($sql);

         $stmt->bindValue(":year", $this->year, PDO::PARAM_STR);


         // run the insert query
         if ($stmt->execute()) {
             $eventInfo = $stmt->fetchAll( PDO::FETCH_GROUP| PDO::FETCH_ASSOC  );

         } else {
             $eventInfo = array();
             if (Globals::$sql_debug) {
                 echo "\nPDO::errorInfo():\n";
                 print_r($stmt->errorInfo());
             }
             return [];
         }

         $newEvents = [];

         if (! empty ($eventInfo)) {
             foreach ($eventInfo as $memberID=>$eventsArray) {

                 $newEvents[$memberID] = [];
                 foreach ($eventsArray as $oneEvent){
                     $eventID=$oneEvent['event_id'];
                     $newEvents[$memberID][$eventID] = $oneEvent;
                 }
             }
         }


         return ($newEvents);
/*
 *  )
|    ['member_id'] => Array (4)
|    (
|    |    ['event_id'] => Array (2)
|    |    (
|    |    |    ['event_id'] = String(4) "1013"
|    |    |    ['approved_selection'] = String(1) "Y"
|    |    )
|    |    ['1018'] => Array (2)
|    |    (
|    |    |    ['event_id'] = String(4) "1018"
|    |    |    ['approved_selection'] = String(1) "Y"
|    |    )
 */
    }

    public function getAllEvents(){


        $sql = "SELECT  * from  events e WHERE e.year = :year order by display_order";
        $eventInfo = array();
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue(":year", $this->year, PDO::PARAM_STR);

        // run the insert query
        if ($stmt->execute()) {
            $eventInfo = $stmt->fetchAll( PDO::FETCH_GROUP| PDO::FETCH_UNIQUE | PDO::FETCH_ASSOC  );

        } else {

            if (Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            return [];
        }

        return $eventInfo;
     }


public function getEventDescription($eventid){

    return "This event {$eventid}";
}




}
