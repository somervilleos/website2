<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/12/15
 * Time: 6:19 PM
 */

namespace SOSModels;


class VolunteerData
{

    private $sos_dbo;



    public function __construct (\PDO $dbo){
        $this->sos_dbo = $dbo;
    }

    public function addNewVolunteer($name, $email, $comment, $checks){


        // 1 Add new User to volunteer contact

        $sql = "INSERT INTO `volunteer_contact` (`volunteer_id`, `name`, `email_address`, `hash`, `signup_date`, `confirmed`, `comment`) VALUES (NULL, :name, :email,  :hash, :date, 'Y', :comment );";

        $stmt = $this->sos_dbo->prepare ($sql);

        //set reset key to date
        //    print "$mysql_date $new_pw $local_id ";
        $mysqlDate=  date("Y-m-d H:i:s");
        $hash = sha1("sos346283^".$email."3dowexdxglkdn43d4".$mysqlDate );

        $stmt->bindParam(':name', $name , \PDO::PARAM_STR);
        $stmt->bindParam(':email', $email , \PDO::PARAM_STR);
        $stmt->bindParam(':hash', $hash , \PDO::PARAM_STR);
        $stmt->bindParam(':date', $mysqlDate , \PDO::PARAM_STR);
        $stmt->bindParam(':comment', $comment , \PDO::PARAM_STR);
        $didWork = $stmt->execute();

        if (!$didWork){
            return false;
        }
        $lastID = $this->sos_dbo->lastInsertId();

        // 2 Add to table volunteer match (each volunteer position selected)

        foreach ($checks as $onePosition) {
            $sql = "INSERT INTO `volunteer_match` (`contact_id`, `position_name`, `position_year`) VALUES (:contact_id, :name, '2106' );";

            $stmt = $this->sos_dbo->prepare($sql);

            $stmt->bindParam(':contact_id', $lastID, \PDO::PARAM_INT);
            $stmt->bindParam(':name', $onePosition, \PDO::PARAM_STR);

            $didWork = $stmt->execute();
        }


        return true;

    }




}