<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/3/15
 * Time: 12:56 AM
 */

namespace SOSModels;

// Like the Other Artists data, but doesn't reveal Contact information.


class ArtistDataPublic {

    private $sos_dbo;
    private $imageProfile;
    private $memberID;
    private $cache_images;

    public function __construct (\PDO $dbo , $memberID){
        $this->sos_dbo = $dbo;
        $this->artistProfile = null;
        $this->memberID = $memberID;
        $this->cache_images = \SOSModels\Globals::$cacheProfileImages;;
        $this->publicCols = ["id","Participating","Active","`location_id`","`profile_id`","`member_id`","`HandicapAccessible`","`Website`","`Books`","`Collage`","`Drawing`","`Fiber`","`Multimedia`","`Mixed_Media`","`Painting`","`Photography`","`Pottery`",
"`Furniture`","`Glass`","`Graphic`","`Installation`","`Jewelry`","`Printmaking`","`Sculpture`","`Video`","`Other_Media`","`Other`","`ShortDescription`","`PublicLastName`","`PublicFirstName`","`BusinessName`",
"`BusinessNameSortKey`","`OrganizationDescription`","`address_details`","`use_images_for_promotion`","`home_studio`","`handicap_accessible`","`building_name`","`street_address`","`street_number`",
"`street_name`","`city`","`state`","`zip`","`notes`","`lat`","`lng`","`map_number`","`map_coordinates`"];
        $this->publicColsStr = implode(','. $this->publicCols);


    }
/*
    static function init(\PDO $dbo , $memberID){




    return $stuff ? new self(\PDO $dbo , $memberID) : false;
}
*/
    /**
     *
     * return all artists information. cached when called..
     *  use forceRefresh = true to force reload from database.
     *
     * @return null
     *
     */
    public function getArtistInfo($forceRefresh = false){

        if ($this->artistProfile == null || $forceRefresh == true) {

            $sql = "SELECT * from current_active_artists_view v JOIN statement s ON (v.id=s.member_id) WHERE v.id = :member_id";

            $stmt = $this->sos_dbo->prepare($sql);
            $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);

            $stmt->execute();
            $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (empty($row)){
                return null;
            }
            $this->artistProfile = $row[0];

        }

        return $this->artistProfile;

    }



    /**
     * @param bool $showBlanks
     * @return array
     *
     */

    public function getArtistEventInfo($year){



        $sql = "select emj.* , e.event_id,e.event_name, e.time_description, e.location_description
from events_member_join emj 
inner join events e
on emj.event_id = e.event_id
where emj.member_id = :member_id  and e.year = :year
";
        $stmt = $this->sos_dbo->prepare($sql);

        $stmt->bindValue(":member_id", $this->memberID, \PDO::PARAM_STR);
        $stmt->bindValue(":year", $year, \PDO::PARAM_STR);

        // run the query
        if ($stmt->execute()) {
            $eventInfo = $stmt->fetchAll(\PDO::FETCH_ASSOC | \PDO::FETCH_GROUP | \PDO::FETCH_UNIQUE);
        } else {

            if (\SOSModels\Globals::$sql_debug) {
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            $socialMediaData = array();
        }

        $indexByEvent = array();

        if (!empty($eventInfo)) {
            foreach ($eventInfo as $oneEvent) {
                $key = $oneEvent['event_id'];
                $indexByEvent[$key] = $oneEvent;
            }

        }


        return $indexByEvent;

    }



    /*
     *
     *
     *  array (size=3)
    0 =>
      array (size=7)
      'id' => string '3991' (length=4)
      'member_id' => string '592' (length=3)
      'ImageNumber' => string '1' (length=1)
      'Title' => string 'surfer - Hurley pro tour' (length=24)
      'Medium' => string 'photo' (length=5)
      'Dimensions' => string 'photo size' (length=10)
      'filename' => string 'artist_files/artist_images/profile/592-01.jpg' (length=45)
  1 =>
    array (size=7)
     *
     */



    // Returns array of image data

    public function getArtistImagesData($showBlanks = true){

        $oneArtist = $this->getAllArtistMemberView();


        if ($this->artistProfile['Expanded_Web'] == 'Y'){
            $numberOfImages = 8;
        } else {
            $numberOfImages = 8;   // changed from usual 3, for SOS 2020 only
        }

        $sos_image_path='artist_files/artist_images/profile/';
        $imageArray = array();
        $jpg_number= array( '01','02','03','04','05','06','07','08');

        // lookup imaage details in database

        $sql = "SELECT * from image WHERE member_id = :member_id ";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        // index based on image number..
        $imageDetails = array();
        foreach ($rows as $oneImage){
            $imageDetails[($oneImage['ImageNumber'])] = $oneImage;
        }


        // loop through images:

        for ($i=0; $i< $numberOfImages; $i++){

            $fileExists = false;

            $profile_jpg = $sos_image_path  . $oneArtist['member_id'] . '-'.$jpg_number[$i] .'.jpg';


            if (!file_exists( $profile_jpg )){

                if ($showBlanks == false && $i == 0){
                    $profile_jpg = $sos_image_path . "noimage.jpg";
                    $fileExists = true;
                }
                if ($showBlanks == true) {
                    $profile_jpg = $sos_image_path . "noimage.jpg";
                    $fileExists = true;
                }

            } else {

                $fileExists = true;
                if ($this->cache_images == false){

                    $profile_jpg .= "?r=".rand(0,1000);
                }
            }


            $indx=$i+1;
            if (array_key_exists($indx,$imageDetails) && $fileExists) {

                $imageDetails[$indx]['filename']= $profile_jpg;
                $imageArray[] = $imageDetails[$indx];

            } else if ($fileExists) {

                // No data about the image.  Shouldn't happen, but if it does return blanks...
                $imageArray[] = array ( 'filename'=> $profile_jpg, 'id' => '','member_id' =>'', 'ImageNumber' =>$indx,'Title' => '','Medium' => '','Dimensions' => '');
            }

        }

        return $imageArray;

    }


    // the thumbnail image had no other information associated with it.. So just report the name/path and  if the image exists.

    public function getArtistThumbnailData()
    {


        $imageName = "{$this->memberID}.jpg";
        $sos_image_path = 'artist_files/artist_images/splash/';

        $fullFileName = $sos_image_path.''.$imageName;

        if (file_exists( $fullFileName)){
            $fileExists = true;
        } else {
            $imageName ="dummy.gif";
            $fullFileName = $sos_image_path.''.$imageName;
            $fileExists = false;

        }



        return array('image_name'=>$imageName, 'path'=>$sos_image_path, 'filename_and_path'=> $fullFileName   ,'image_exists'=>$fileExists);



    }


    /**
     * given user info, get what informatoin to show.
     *
     *
     */

    public function getUserFeatures()
    {


        $features = array();
        $this->getArtistInfo();

        if (empty($this->artistProfile)){

            return null;
        }

        $feature['expanded_web'] =$this->artistProfile['Expanded_Web'];
        $feature['friday_night'] =$this->artistProfile['FridayNight'];
        $feature['show_business_name'] =$this->artistProfile['BusinessNameOption'];
        $feature['show_organization_description'] =$this->artistProfile['MemberType']>=2?true:false;
        $feature['extended_profile'] =$this->artistProfile['Expanded_Web'];
        $feature['fashion_show'] =$this->artistProfile['fashion_show'];



        // Open Fridays?
        // show group information?
        // extended profile
        // participating in fashion show.
        // participating in artists' choice
        //


        return $feature;
    }


    // Get ARtists member info direct: for registration

    public function getArtistMemberInfo(){

        $sql = "SELECT * from member where id = :member_id";

        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);

        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (empty($row)){
            return null;
        }

        return $row[0];
    }

    /**
     *
     * Get the the artists Members
     *
     * @return mixed|null
     */


    public function getAllArtistMemberView(){

        //$sql = "SELECT * from current_all_artists_view where id = :member_id";
        $sql = "SELECT * from current_all_artists_view v JOIN statement s ON (v.id=s.member_id) WHERE v.id = :member_id";
        $stmt = $this->sos_dbo->prepare($sql);
        $stmt->bindValue("member_id", $this->memberID, \PDO::PARAM_INT);

        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (empty($row)){
            return null;
        }

        return $row[0];
    }



}