<?php

namespace SOSModels;

// This is from the older SOS site.. But it generates a list of artists as JSON... So we keep.
// Probably overkill, but we keep.

use \SOS\SQLHelper;

class ProfileUpdate
{


    public $profileUpdateFields; // fields able to be updated by this class
    private $uniqueCol;

// constructor.
    public function __construct(\PDO $dbo)
    {
        $this->sos_dbo = $dbo;


        //
        // these are the only field that can be updated by this class.
        // The index key is the table that can be updated
        // in this case only one table is involved, but for commonality..


        $this->profileUpdateFields = array('profile'=>array(
            'HandicapAccessible',
            'ShortDescription',
            'PublicLastName',
            'PublicFirstName',
            'BusinessName',
            'OrganizationDescription',
            'address_details',
            'use_images_for_promotion',
            'home_studio'),
        'member'=>array(
            'FashionInfo',
            'FridayNight'),
            'statement'=>array('statement')
        );


        $this->tableToWhereColumn = array ('profile'=>'member_id', 'member'=>'id', 'statement'=>'member_id');


    }


    public function updateFromArray($originalData, $newData, $memberid)
    {

        $status = array("status" => "passed", 'messages' => array());


        // $fieldNames = array ('profile_id', 'member_id', 'location_id', 'HandicapAccessible', 'ShortDescription', 'PublicLastName', 'PublicFirstName', 'BusinessName', 'BusinessNameSortKey', 'OrganizationDescription', 'address_details', 'use_images_for_promotion', 'home_studio');
        foreach ($this->profileUpdateFields as $tablename=> $fieldNames) {



            // make a list of the changed fields, we will update only those;
            $changedFields =  \SOS\SQLHelper::findChangesInArray($fieldNames, $originalData, $newData);


            if (empty ($changedFields)) {
                $status['messages'][] = "No Changes in fields detected.  {$tablename} Form not updated";
                continue;

            }



            // build the sql using list of field->updated values;

            $fieldUpdateArray = array();
            foreach ($changedFields as $fieldName => $value) {
                $fieldUpdateArray[] = "`{$fieldName}` = :{$fieldName}";
            }

            $fieldNamesSQL = implode(',', $fieldUpdateArray);


            // for this case the update variable names are the same as the names that came into the form
            // so create an array where the keys = the values.  eg ("one"=>"one", "two"=>"two")...

            // before we send to the binding function, add the profile id.


            $colsToBind = array_keys($changedFields);

            // get the column name used in the where clause
            $whereColName = $this->tableToWhereColumn[$tablename];
            $colsToBind[$whereColName] = $whereColName ;

            $paramsToBind = array_combine($colsToBind, $colsToBind);

            if (! empty($changedFields)) {
                $changedFields[$whereColName] = $memberid;


                // set up the base SQL
                $sql = "update  `{$tablename}` set {$fieldNamesSQL} WHERE $whereColName= :{$whereColName}";

                // bind the input fields to the sql
                $stmt = \SOS\SQLHelper::bindSQL($this->sos_dbo, $sql, $paramsToBind, $changedFields);

                // run the insert query
                if ($stmt->execute()) {
                    $status['messages'][] = "+++ Updated OK";
                } else {

                    $status['status'] = "failed";
                    $status['messages'][] = "+++ Updated Failed";
                    if (\SOSModels\Globals::$sql_debug) {
                        echo "\nPDO::errorInfo():\n";
                        print_r($stmt->errorInfo());
                    }
                }
            }
        }

        return $status;
    }


    public function updateArtistGenre()
    {


//$sql = 'INTO `profile` (  `Books`, `Collage`, `Drawing`, `Fiber`, `Multimedia`, `Mixed_Media`, `Painting`, `Photography`, `Pottery`, `Furniture`, `Glass`, `Graphic`, `Installation`, `Jewelry`, `Printmaking`, `Sculpture`, `Video`, `Other_Media`,  `Other`) VALUES (  :Books,  :Collage, :Drawing, :Fiber, :Multimedia, :Mixed_Media, :Painting, :Photography, :Pottery, :Furniture, :Glass, :Graphic, :Installation, :Jewelry, :Printmaking, :Sculpture, :Video, :Other_Media, :Other')"

    }


}