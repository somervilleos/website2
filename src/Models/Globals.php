<?php

//global state variable for SOS

namespace SOSModels;

class Globals{

    public static $sql_debug = true;
    public static $debug = true;

    public static $sos_year = "2022";
    public static $sos_ad_year = "2020";
    public static $sos_financial_year = "2021";
    public static $sos_date = "April 30 +May 1 2022";
    public static $sos_date_with_preview = "May 1+1 2020";


    public static $social_media_icon_dir = "web/images/social_media_icons";

    // lock down map book  information in the database
    public static $profileFreeze = false;
    public static $showMapNumber = true;
    public static $cacheProfileImages = false;

   /// $app['mapSettings'] = array('bounds'=>'false', 'showSponsors'=>'false', 'showExhibits'=>'false', 'showTrolley'=>'false','initial_center'=>'42.391228,-71.101692' );


// if profile
  // $app['profileFreeze'] = true;

/*
public function __construct()
{




}
*/

}