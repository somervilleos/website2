<?php
namespace SOSModels;


/* This is a Class to contain some information about memberhip levels

Its mainly used for registration now.   It also contains the costs... so  there is that.

*/

    class MembershipLevels {


        public static $membershipLevels = array(
            1 =>array('name'  => 'Sustaining Membership',
                'description' => 'membership for Artists not participating in the Open Studios Event',
                'cost' =>30,
                'memberTypetoStore' =>1,
                'participating' =>'N',
                'extraMembership' => ",community_space ='N' ",
                'paymentText' =>'Sustaining Membership (not exhibiting)'
            ),
            2 =>array('name'  => 'Event Membership',
                'description' => 'membership for Artists participating in the Open Studios Event showing from their home or studio',
                'cost' =>70,
                'memberTypetoStore' =>2,
                'participating' =>'Y',
                'extraMembership' => ",community_space ='N' ",
                'paymentText' =>'Event Membership / Individual'
            ),
            3 =>array('name'  => 'Event Membership + Community Space',
                'description' => 'membership for Artists participating in the Open Studios Event that are using Community Space',
                'cost' =>130,
                'memberTypetoStore' =>2,
                'participating' =>'Y',
                'extraMembership' => ",community_space ='Y' ",
                'setLocationTo' => 62,
                'paymentText' =>'Event Membership / Individual + Comm Space'
            ),

            4 =>array('name'  => 'Arts Related Non-Profit Membership',
                'description' => 'membership for Arts based Non-Profit (501c or equivalent) Organizations participating in the Open Studios Event',
                'cost' =>95,
                'memberTypetoStore' =>4,
                'participating' =>'Y',
                'extraMembership' => ",community_space ='N' ",
                'paymentText' =>'Event Membership / Arts Non-Profit Org'
            ),

            5 =>array('name'  => 'Non-Profit Membership',
                'description' => 'membership for Non-Profit (501c or equivalent) Organizations participating in the Open Studios Event',
                'cost' =>95,
                'memberTypetoStore' =>5,
                'participating' =>'Y',
                'extraMembership' => ",community_space ='N' ",
                'paymentText' =>'Event Membership / Other Non-Profit Org'
            ),
            6=>array('name'  => 'Business Membership',
                'description' => 'membership for Non-Profit (501c or equivalent) Organizations participating in the Open Studios Event',
                'cost' =>125,
                'memberTypetoStore' =>6,
                'participating' =>'Y',
                'extraMembership' => ",community_space ='N' ",
                'paymenText' =>'Event Membership / For Profit Org'
            )
        );


    }



