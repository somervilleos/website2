<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 12/3/15
 * Time: 12:56 AM
 */

namespace SOSModels;


class SponsorshipData {

    private $sos_dbo;
    private $year;


    public function __construct (\PDO $dbo ,$year){
        $this->sos_dbo = $dbo;
        $this->year = $year;
        $this->levelsOrder = array ('Gold','Silver','Silver_Banners','Silver_Plus', 'Basic', 'Mini', 'Thanks');



    }
/*
    static function init(\PDO $dbo , $memberID){




    return $stuff ? new self(\PDO $dbo , $memberID) : false;
}
*/
    /**
     *
     *  Get sponsorhip data from the database
     *
     * @return array of sponship data
     *
     */
    public function getSponsorData( $type = "all"){

        if ($type=='food'){
            $sql = "select * FROM sponsors WHERE food_and_drink ='Y' AND year= :year Order By map_number";
        } else {
            $xr = mt_rand(1,10);
            if ($xr>5){
                $sql = "select * FROM sponsors WHERE year= :year Order By sponsor_name desc";
            } else {
                $sql = "select * FROM sponsors WHERE year= :year Order By sponsor_name asc";
            }
        }


            $stmt = $this->sos_dbo->prepare($sql);
            $stmt->bindValue(":year", $this->year, \PDO::PARAM_INT);

            $stmt->execute();
            $this->sponsorData = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (empty($this->sponsorData )){
                return array();
            }




        return  $this->sponsorData;

    }

    public function getOrderArray(){
        return $this->levelsOrder;

    }





}