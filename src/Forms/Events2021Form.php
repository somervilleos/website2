<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class Events2021Form {

    private static $sql_debug = false;
    private static $debug = false;
    public   $defaultData = [];



    public  function getDefaultData ($profile, $eventInfo){



        /*'PublicLastName'=> $profile['PublicLastName'],
            'BusinessName'=> $profile['BusinessName'],
            'ShortDescription'=> $profile['ShortDescription'],
            'OrganizationDescription'=> $profile['OrganizationDescription'],*/

        /*

        $defaultData = array('PublicFirstName'=> $profile['PublicFirstName'],
            //   'artist_statement'=>$profile['artist_statement'],
            'use_images_for_promotion'=> isset($profile['use_images_for_promotion']) ? $profile['use_images_for_promotion'] :'N',
            'home_studio'=> isset($profile['home_studio']) ? $profile['home_studio'] :'N',
            'FridayNight'=> isset($profile['FridayNight']) ? $profile['FridayNight'] :'N',
            'FashionInfo'=> isset($profile['FashionInfo']) ? $profile['FashionInfo'] :'N',
            'statement'=>  $profile['statement']
        );

*
         */

        //(1013,1017,1018,1019,1020);


        $defaultData = [];

        $defaultData["event_booth_central"] =isset($eventInfo['1013']) ? $eventInfo['1013']['member_selection'] :'N';
        $defaultData["event_video"] = isset($eventInfo['1020']) ? $eventInfo['1020']['member_selection'] :'N';
        $defaultData["event_outdoors_sos"] = isset($eventInfo['1018']) ? $eventInfo['1018']['member_selection'] :'N';
        $defaultData["event_i_have_tent"] = isset($eventInfo['1019']) ? $eventInfo['1019']['member_selection'] :'N';
        $defaultData["event_outdoors_myspace"]= isset($eventInfo['1017']) ? $eventInfo['1017']['member_selection'] :'N';


        $this->defaultData = $defaultData;

        return $defaultData;

    }




    public  function getForm(\Silex\Application $app, $profile, $eventInfo ) {


        $defaultData = self::getDefaultData($profile, $eventInfo);


        // The Y/N choice box
        $nyChoice = \SOSForms\CommonForm::$nyChoice;
         $nyChoice =  array('Yes'=>'Y','No'=>'N');



        $profileFreeze = \SOSModels\Globals::$profileFreeze;


        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData);



        $form = $form->add('event_booth_central', ChoiceType::class, array( 'required'   => true,
                'choices' => $nyChoice,
                'expanded' => true,
                'multiple'=> false,
                'label' => 'Booth Central ',
                'data' => $defaultData["event_booth_central"]
            ))

            -> add('event_video', ChoiceType::class, array( 'required'   => true,
                'choices' => $nyChoice,
                'expanded' => true,
                'multiple'=> false,
                'label' => 'SOS Artist’s Video.',
                'data' => $defaultData["event_video"]
            ))
            -> add('event_outdoors_sos', HiddenType::class, array( 'required'   => false,
                'label' => 'SOS Outdoor Group Display - not available',
                'data' => $defaultData["event_outdoors_sos"]
            ))
            -> add('event_i_have_tent', HiddenType::class, array( 'required'   => false,
                'label' => 'SOS Outdoor Group Display - not available',
                'data' =>  $defaultData["event_i_have_tent"]
            ))
            -> add('event_outdoors_myspace', ChoiceType::class, array( 'required'   => true,
                'choices' => $nyChoice,
                'expanded' => true,
                'multiple'=> false,
                'label' => 'Individual Artists Outdoor Display',
                'data' => $defaultData["event_outdoors_myspace"]
            ));





        $form = $form->add('submit', SubmitType::class, [
            'label' => 'Save and Continue',
            'attr' => array('class' => 'btn btn-lg btn-success')

        ])->getForm();


        return $form;

    }

    public  function processFormData ( \PDO $dbo, Application $app, $formData, $member_id, $existingData ){

        $status = array("status" => "passed", 'messages' => array());

        $eventObj = new \SOSModels\EventData($dbo, 2021);



        $status = array();

       $toProcess=  array("event_booth_central"=>1013,
            "event_video"=>1020,
            "event_outdoors_sos"=>1018,
            "event_i_have_tent"=>1019,
           "event_outdoors_myspace"=>1017);

       foreach ($toProcess as $name=>$eventID){


           if (array_key_exists($name, $formData )){
               $status[] = $eventObj->addOrUpdate($member_id, $eventID, $formData[$name]);

           }




       }



        return $status;


    }

}