<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;




class PasswordResetForm {

    public function getFormReset (\Silex\Application $app) {

        $form = $app['form.factory']->createBuilder(FormType::class)
         ->setMethod('POST')
         ->add('email', TextType::class, array('label'=>'Email :','required' => true, 'constraints' => new Assert\Email()))
         ->add('submit', SubmitType::class)
         ->getForm();

        return $form;
    }


    public function processResetForm(\Silex\Application $app, $formData) {

        $LoginService = new \SOS\LoginService($app['pdo'], $app);

        $loginCheck = $LoginService->resetPasswordRequest($formData['email']);


        $resetOK = $loginCheck->didReset;

        if ($resetOK){

            $app['session']->getFlashBag()->add('info','Sent reset email. ');
        } else {
            $app['session']->getFlashBag()->add('danger','Unable to send reset email.  Contact webmaster@somervilleopenstudios.org for assistance');


        }

        return $resetOK;

    }




    /**
     *
     * Store form data.  Goes into 2 database tables (volunteer_contact, volunteer_match)
     *
     * @param $dbo
     * @param $formData
     * @return bool
     */



    public function addNewVolunteer($dbo, $formData){

        $volunteerOptions = $this->positionList();
        $volunteerChoices = array();
        foreach ($volunteerOptions as $key=>$oneVol){
            if (in_array($key,($formData['vol_pos']))) {
                $volunteerChoices[] = $oneVol->option_name;
            }
        }


        // 1 Add new User to volunteer contact

        $sql = "INSERT INTO `volunteer_contact` (`volunteer_id`, `name`, `email_address`, `hash`, `signup_date`, `confirmed`, `comment`) VALUES (NULL, :name, :email,  :hash, :date, 'Y', :comment );";

        $stmt = $dbo->prepare ($sql);

        //set reset key to date
        //    print "$mysql_date $new_pw $local_id ";
        $mysqlDate=  date("Y-m-d H:i:s");
        $hash = sha1("sos346283^".$formData['email']."3dowexdxglkdn43d4".$mysqlDate );

        $stmt->bindParam(':name',  $formData['name'] , \PDO::PARAM_STR);
        $stmt->bindParam(':email', $formData['email'] , \PDO::PARAM_STR);
        $stmt->bindParam(':hash', $hash , \PDO::PARAM_STR);
        $stmt->bindParam(':date', $mysqlDate , \PDO::PARAM_STR);
        $stmt->bindParam(':comment',  $formData['comments'] , \PDO::PARAM_STR);
        $didWork = $stmt->execute();

        if (!$didWork){
            return false;
        }
        $lastID = $dbo->lastInsertId();

        // 2 Add to table volunteer match (each volunteer position selected)

        foreach ($volunteerChoices as $onePosition) {
            $sql = "INSERT INTO `volunteer_match` (`contact_id`, `position_name`, `position_year`) VALUES (:contact_id, :name, '2018' );";

            $stmt = $dbo->prepare($sql);

            $stmt->bindParam(':contact_id', $lastID, \PDO::PARAM_INT);
            $stmt->bindParam(':name', $onePosition, \PDO::PARAM_STR);

            $didWork = $stmt->execute();
        }


        return $didWork;

    }






        //-----------------------------
// List of Volunteer Positions
//-----------------------------
//
    protected function positionList() {


        $volunteerOptions = array();

        $obj = new \stdClass;
        $obj->name = 'Sponsorship / Ad sales';
        $obj->option_name = 'sponsorship';
        $obj->description = ' - Now through end of Feb.<br>
Promote SOS sponsorship to businesses<br>
<i>Team Lead: Suzanne Lubeck</i>';
        $obj->number_position = 6;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Marketing';
        $obj->option_name = 'marketing';
        $obj->description = ' - Now through May.<br>
Promote and Market SOS<br>
<i>Team Lead: Margaret Demille</i>';
        $obj->number_position = 6;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Poster Distribution';
        $obj->option_name = 'poster_dist';
        $obj->description = ' - mid-March to Mid-April. <br>
tape posters in shop windows<br>
<i>Team Lead: Matt Kaliner</i>';
        $obj->number_position = 25;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Mapbook Distribution';
        $obj->option_name = 'mapbook_dist';
        $obj->description = ' - mid-April through SOS weekend<br>
fill selected mapstands, place maps in select businesses. <br>
<i>Team Lead: Cira Louise Brown</i>';
        $obj->number_position = 25;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Proof Readers';
        $obj->option_name = 'proof_readers';
        $obj->description = ' - mid March<br>
carefully read through entire mapbook<br>
<i>Team Lead: TBD</i>';
        $obj->number_position = 3;
        $volunteerOptions[] = $obj;


//5
        $obj = new \stdClass;
        $obj->name = 'Exhibit Support - Volunteer Show';
        $obj->option_name = 'volunteer_show_exhibit_support';
        $obj->description = '<br>
Sat. March 29 &amp; Sun. March 30 - Drop-off help<br>
Tues. April 1 - hang group art show at Bloc 11<br>
<i>Team Leads: Christina Tedesco & Gretchen Graham</i> ';
        $obj->number_position = 6;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Exhibit Support -  Artists\' Choice Show ';
        $obj->option_name = 'artists_choice_exhibit_support';
        $obj->description = ' - dates in late March through mid-May<br>
Wed. March 26 &amp; Sat. March 29: accept art, process forms<br>
Thurs. May 15 &amp; Sat. May 17: return art, process forms<br>
<i>Team Lead: Mara Brod </i>';;
        $obj->number_position = 8;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Hang Artists\' Choice Show ';
        $obj->option_name = 'hang_artists_choice_show';
        $obj->description = ' - Mon. March 31 and Tues. April 1: hang show<br>
<i>Team Lead: Mara Brod</i>';
        $obj->number_position = 10;
        $volunteerOptions[] = $obj;


//7---------------------------------------------

        $obj = new \stdClass;
        $obj->name = 'Artists\' Choice Reception';
        $obj->option_name = 'artists_choice_reception';
        $obj->description = ' - Friday, April 11, 7-9 pm<br>
set-up food, clean up<br>
<i>Team Lead: Mara Brod</i>';
        $obj->number_position = 5;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Map Stands Install / Remove';
        $obj->option_name = 'map_stands';
        $obj->description = ' - 1 or 2 days, early Apr.<br>
driving, lifting, installing mapstands<br>
<i>Team Lead: Hilary Scott</i>';
        $obj->number_position = 3;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Flatbread fundraiser table sitting';
        $obj->option_name = 'flatbread';
        $obj->description = ' - Tuesday, April 22, 5-11 pm<br>
hour-long shifts: sell raffle tickets, promote SOS <br>
<i>Team Lead: Christina Tedesco </i>';
        $obj->number_position = 4;
        $volunteerOptions[] = $obj;
        /*
        $obj = new \stdClass;
        $obj->name='Fashion Show Crew';
        $obj->option_name='fasion_crew';
        $obj->description=' - Apr. 29 - May 1<br>
        prep, dress rehearsal, show<br>
        door greeters, stage hands, runners<br>
        <i>Team Lead: JoAnne Coppolo </i>';
        $obj->number_position=8;
        $volunteerOptions[] = $obj;
        */

// 9
        $obj = new \stdClass;
        $obj->name = 'During-Event Ambassadors';
        $obj->option_name = 'during_event_ambassadors';
        $obj->description = ' - Sat. May 3, Sun., May 4<br>
2 Hour Shifts from 11:00am - 6pm<br>
at Info Booths and on Trolleys<br>
greet visitors, explain event<br>
<i>cannot be participating artist!!</i></br>
<i>Team Lead: TBA</i>';
        $obj->number_position = 20;
        $volunteerOptions[] = $obj;

        $obj = new \stdClass;
        $obj->name = 'Other';
        $obj->option_name = 'other';
        $obj->description = '<br/>Other volunteer opportunities,  fill in details of what you\'re looking to help with in the comments';
        $obj->number_position = 'Many';
        $volunteerOptions[] = $obj;

        return $volunteerOptions;

    }

}