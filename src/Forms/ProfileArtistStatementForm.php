<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


    class ProfileArtistStatementForm {



    private static $sql_debug = false;
    private static $debug = false;




    public static function getDefaultData ($profile){

        $defaultData = array(
            'statement'=>  $profile['statement']
        );


        return $defaultData;

    }




    public  function getForm(\Silex\Application $app, $profile) {


        $defaultData = self::getDefaultData($profile);


        $genres = array_flip(\SOSModels\ArtistsGenreList::$column_name_to_genre_name);
        $artistGenreObj =  new \SOSModels\ArtistsGenreList($app['pdo']);
        $checkedGenre = $artistGenreObj->getGenreFormCheckboxes($profile);

        /*
        print "<h3> Genre Checked </h3><pre>";

        var_dump ($checkedGenre);

        print "</pre>";

        print "<h3> Profile </h3><pre>";
        var_dump ($profile);

        print "</pre>";

        */
        $nyChoice = \SOSForms\CommonForm::$nyChoice;

        $profileFreeze = $app['profileFreeze'];
        // \SOSModels\Globals::$profileFreeze;

        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData);

        $form = $form->add('statement', TextareaType::class, array('label'=>'Artist Statement',
            'required'   => false,
            'attr' => array('style' => 'height:550px'),
            'label' => "Artist Statement (up to 5000 characters)",

            'constraints' => array(new Assert\NotBlank(),
                new Assert\Length(array('min' => 2, 'max'=>9000)))
        ));



        $form = $form->add('submit', SubmitType::class, [
            'label' => 'Save and Continue',
        ])->getForm();


        return $form;

    }

    public  function processFormData ( \PDO $dbo, $originalArtistsData, $formData, Application $app, $memberid ){


        $profileUpdateObj = new \SOSModels\ProfileUpdate($dbo);

        $status = $profileUpdateObj->updateFromArray($originalArtistsData, $formData, $memberid);

        return $status;


    }

}