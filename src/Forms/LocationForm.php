<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class LocationForm {

    private $dbo;
    private $locationObj;


    public function __construct( \PDO $dbo, $locationObj)
    {
        $this->dbo = $dbo;
        $this->locationObj = $locationObj;

    }


    public function getForm(\Silex\Application $app, $profile, $location)
    {

        // we're getting the location field because it contains more details than are available in the profile

        if (!empty ($location) && is_array($location) ) {
            $defaultData = array('street_number' => $location['street_number'],
                'street_name' => $location['street_name'],
                'address_details' => $profile['address_details'],
                'home_studio' => $profile['home_studio'],
                'accessible' => $profile['HandicapAccessible'],
                'outdoor_display' => 'N' #'$profile['outdoor_display']
            );
            $locationID = $location['location_id'];

        } else {
            $defaultData= array();
            $locationID = 0;
        }

        $studioBuildings = $this->locationObj->getLocationsWith4More();


        $studioDropdown = array('Other'=>'Other');
        $defaultDropdown = 'Other';

        foreach ($studioBuildings as $location_id=>$oneBuilding){
            if ($oneBuilding == false){
                continue;
            }

            $key = $oneBuilding['building_name']. ' - '.$oneBuilding['street_address'];
            $studioDropdown[$key] = $location_id;

            if ($locationID == $location_id){

                $defaultDropdown = $location_id;
            }
        }

        $defaultData['studio_picker']= $defaultDropdown;



        //
        //$dropdownValue =

        $nyChoice = \SOSForms\CommonForm::$nyChoice;

        $form = $app['form.factory']->createBuilder(FormType::class, $defaultData)



            ->add('studio_picker', ChoiceType::class, array( 'required'   => true,
                'choices' => $studioDropdown,
                'expanded' => false,
                'multiple'=> false,
                'label' => 'If you are in a studio building, please pick it from the list. Otherwise select "OTHER"'
            ))
            ->add('building_name', TextType::class,
                array('label'=>'Building Name (if location has more than 4 artists)',
                    'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                    'required'   => false
                ))
            ->add('street_number', TextType::class,
                array('label'=>'Street Number',
                    'attr' => array('style' => 'width:150px', 'placeholder' => ''),
                    'required'   => false
            ))
            ->add('street_name', TextType::class, array('label'=>'Street Name',
                'required' => false
            ))

            ->add('address_details', TextType::class, array(
                'label' => 'My studio # / Unit # ',
                'required'   => false,
            ))
           ->add('home_studio', ChoiceType::class, array( 'required'   => true,
                'choices' => $nyChoice,
                'expanded' => true,
                'multiple'=> false,
                'label' => 'My studio is my home'
            ))
            ->add('accessible', ChoiceType::class, array( 'required'   => true,
                'choices' => $nyChoice,
                'expanded' => true,
                'multiple'=> false,
                'label' => 'My studio is accessible (Americans With Disabilities Act  description [1]'
            ))

            ->add('submit', SubmitType::class, [
                'label' => 'Save and Continue',
                'attr' => array('class' => 'btn-info')

            ])
            ->getForm();


        return $form;

    }


    // The location form is a little complex.  The choice type
    // returns array ('data_ok'> Boolean, 'message'=> "sting")

    public  function checkFormData ($formData ){

        // if users selected a location from the drop down,  the form is ok.
        $returnData =  array ('data_ok'=> true, 'message'=> "");
        if (is_numeric($formData['studio_picker'])) {
            $returnData['data_ok'] = true;
            return $returnData;
        }

        // check street name is not blank,  only after checking other information
        $street_name = trim($formData['street_name']);
        if (empty ($street_name)){
            $returnData['data_ok'] = false;
            $returnData['message'] = 'street name cannot be blank';
        }

        return $returnData;
    }

    public  function processFormData ($memberid, $formData, Application $app ){

        $resultData = $this->locationObj->saveMemberLocation ($memberid, $formData);


        return $resultData;


    }

}