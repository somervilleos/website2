<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;




class ItineraryForm {

    public function getForm(\Silex\Application $app, $defaultData =null) {





        $form = $app['form.factory']->createBuilder(FormType::class)
     ->setMethod('POST')
     ->add('share_code', TextType::class, array('label'=>'Share Key :','required' => true,
                     'attr' => array('style' => 'width:350px', 'placeholder' => '')

     ))
     ->add('submit', SubmitType::class)
     ->getForm();




        return $form;
}


    /**
     *
     * Add a key based on the input specified.
     *
     * @param $dbo
     * @param $formData
     * @return bool
     */



    public function processForm (\Silex\Application $app, $dbo, $formData){

        $itinObj = new \SOSModels\Itinerary($dbo);
        $share_key = trim ($formData['share_code']);

        $itinDetails = $itinObj->getDetailsFromShareKey($share_key);

        if (!empty($itinDetails)) {
            \SOS\ItineraryCookie::create($itinDetails['edit_key'], $itinDetails['share_key']);
            $app['session']->getFlashBag()->add('info','Updated Itinerary Information. ');
        } else {

            $app['session']->getFlashBag()->add('danger','Could Not Find Itinerary from that code. ' .$itinDetails['edit_key'] .'Itinerary not updated');
        }


    }

}