<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class Refund2020Form {

    private static $sql_debug = false;
    private static $debug = false;


    public static function getForm(\Silex\Application $app, $existingData,  $memeber_id, $member_info) {

        //var_dump($existingData);
        $option1 = '';

        if (empty($existingData)){
            $existingData = array();
        }
        if ($member_info['MemberType'] != 1) {


            if ($member_info['community_space'] == 'Y'){
                $option1 =' MAINTAIN MY MEMBERSHIP  (PARTIAL REFUND) - Change my membership from Participating Artist to Sustaining and refund the difference of $30 + $55 Community space fee ($85).  I will remain an SOS member and keep my page in the SOS online artist directory.  ';
            } else {
                $option1 = ' MAINTAIN MY MEMBERSHIP  (PARTIAL REFUND) - Change my membership from Participating Artist to Sustaining and refund the difference of $30. I will remain an SOS member and keep my page in the SOS online artist directory. ';


            }
            $refund_type2 = array($option1 =>'to_web_only');
        }

        $refund_type = array (
            ' DONATION IN LIEU OF REFUND - Instead of refunding me, please consider the amount (that would have otherwise been refunded) as a donation to SOS. I understand that I won’t receive a refund but I will be supporting SOS.' => 'none',
            ' FULL REFUND – I would like to maintain my SOS membership and keep my page in the online artist directory but request a full refund of all fees. (Donations made when registering aren’t refunded except by special request).' => 'all',
            ' OTHER - Please refund another amount (please note the amount in the comment box below and provide a brief explanation).' => 'other',

        );

        if (! empty($refund_type2)){
            $refund_type = array_merge($refund_type2, $refund_type);

        }


        $form = $app['form.factory']->createBuilder(FormType::class, $existingData)



            ->add('refund_type', ChoiceType::class, array(
                'choices' => $refund_type,
                'expanded' => true,
                'data' => 'to_web_only',
                'label' => 'Refund Type'
            ))
            ->add('comment', TextareaType::class, array('label'=>'comment :',
                'required' => false,
                'attr' => array('style' => 'width:350px', 'placeholder' => '')
            ))
            ->add('member_id', HiddenType::class, array(
                'data' => $memeber_id
            ))

            ->add('submit', SubmitType::class, [
                'label' => 'Done',
            ])
            ->getForm();


        return $form;

    }





    public static function processFormData ( \PDO $dbo, $existingData, $member_id, $formData, Application $app){


        $year = "";

        date_default_timezone_set('America/New_York');
        $date = date("Y-m-d H:i:s");

        if ($existingData) {
            $sql = 'UPDATE `refunds2020` SET `refund_selected` = :refund_type, `comment`= :comment, date_updated = :date_updated  WHERE `refunds2020`.`member_id` = :member_id; ;';

            $stmt = $dbo->prepare($sql);
            $stmt->bindValue(':date_updated',$date, \PDO::PARAM_STR);
            $stmt->bindValue(':refund_type',$formData['refund_type'], \PDO::PARAM_STR);
            $stmt->bindValue(':comment',$formData['comment'], \PDO::PARAM_STR);
            $stmt->bindValue(':member_id',$formData['member_id'], \PDO::PARAM_STR);
        } else {

            $sql = 'REPLACE INTO `refunds2020` (`id`,`member_id`, `refund_selected`, `comment`, `date_pending`) VALUES (null, :member_id, :refund_type, :comment, :date_pending);';

            $stmt = $dbo->prepare($sql);
            $stmt->bindValue(':date_pending',$date, \PDO::PARAM_STR);
            $stmt->bindValue(':member_id',$formData['member_id'], \PDO::PARAM_STR);
            $stmt->bindValue(':refund_type',$formData['refund_type'], \PDO::PARAM_STR);
            $stmt->bindValue(':comment',$formData['comment'], \PDO::PARAM_STR);
        }

        //var_dump($formData);






       // print "sql: $sql <br>";

        if ($stmt->execute()) {

            //$resultData = $stmt->fetchAll(\PDO::FETCH_ASSOC | \PDO::FETCH_GROUP);
            $return = true;

        } else {
            $return = false;

            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());


            }
            return null;
        }

        //----------------------------------------------------
        // Send email


/*
 *
        $emailObj = new \SOS\EmailService();
        $message = <<<EOF

Donation info:  
name: {$formData['first_name']}  {$formData['last_name']}
phone: {$formData['phone']}
email: {$formData['email']}
address: {$formData['street_address']}
date: {$mysql_date} 


EOF;
        $emailObj->sendEmail("acomjean@gmail.com,treasurer@somervilleopenstudios.org ",
            "Sponsorship info", $message, 'webmaster@somervilleopenstudios.org' );




        //---------------------------------------------------------------
        // lets create a payment record so that the payment can be made.

        $typePurchased = "Somerville Open Studios Donation";
        $cost = $formData['amount'];

        $paymentObj = new \SOSModels\Payments($dbo);
        $name = $formData['first_name'].' '. $formData['last_name'];
        $email = $formData['email'];


        $blankPayment = $paymentObj->get_blank_payment();
        $blankPayment['item_description']= "SOS sponsorship -". $typePurchased;
        $blankPayment['payer_name']= $name;
        $blankPayment['transaction_amount'] = $cost;
        $blankPayment['donation_amount'] = 0;
        $blankPayment['payer_email']=$email;
        $blankPayment['member_id']=0;
        $blankPayment['type']='sponsorship';


        $paymentID = $paymentObj->add_payment($blankPayment);

        $hash = $paymentObj->getPaymentHash($paymentID);



        return array('pid'=>$paymentID, 'hid'=>$hash);


*/
        return $return;

    }

}
