<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class FinAidForm {

    private static $sql_debug = false;
    private static $debug = false;


    public static function getForm(\Silex\Application $app) {


        $form = $app['form.factory']->createBuilder(FormType::class)
            ->add('q1', TextareaType::class, array(
                'label'=>'have participated in SOS in the past? If so how long have you been participating? Past participation will not affect your eligibility for receiving aid but it’s helpful for us to understand whether new or returning members are in need of assistance.',
                'required' => true,
                'attr' => array('style' => 'width:500px', 'placeholder' => '')
            ))
            ->add('q2', TextareaType::class, array(
                'label'=>'amount of aid requested (full amount or some $ figure)',
                'required' => false,
                'attr' => array('style' => 'width:550px', 'placeholder' => '')
            ))

            ->add('q3', TextareaType::class, array(
                'label'=>'Please provide a brief statement regarding the general nature of your specific financial hardship. We don’t need personal details, just the general category of your situation (e.g. medical reasons, dramatic loss of income, dramatic increase unexpected costs). :',
                'required' => true,
                'attr' => array('style' => 'width:500px', 'placeholder' => '')
            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Apply',
            ])
            ->getForm();


        return $form;

    }

    public static function processFormData ( \PDO $sos_dbo, $memberID, $formData, Application $app){

        $year = "";
        date_default_timezone_set('America/New_York');
        $now = date('Y-m-d H:i:s');

        $sql= 'INSERT INTO `faid` (`member_id`, `question_1`, `question_2`, `question_3`, `date`) VALUES (:member_id, :one, :two, :three, :datetime)';
        $stmt = $sos_dbo->prepare ($sql);
        $stmt->bindParam (':member_id',$memberID, \PDO::PARAM_STR );
        $stmt->bindParam (':one', $formData['q1'] , \PDO::PARAM_STR );
        $stmt->bindParam (':two',$formData['q2'] , \PDO::PARAM_STR );
        $stmt->bindParam (':three', $formData['q3'] , \PDO::PARAM_STR );
        $stmt->bindParam (':datetime',$now, \PDO::PARAM_STR );

        $result = $stmt->execute();

        if ($result){
            $apply_string= ' Thank for applying, you should be hearing back from us shortly.  Your Registration is complete.  Questions? email:  mailto:fin-aid@somervilleopenstudios.org   fin-aid@somervilleopenstudios.org.';
                $app['session']->getFlashBag()->add('info', $apply_string);

            return true;

        } else {

            $apply_string= 'We seem to have trouble saving.  Please contact: mailto:fin-aid@somervilleopenstudios.org"> fin-aid@somervilleopenstudios.org.';

            $app['session']->getFlashBag()->add('danger', $apply_string);

            return false;

            /*if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());
            }*/

        }

        //----------------------------------------------------
        // Send email

/*
        $emailObj = new \SOS\EmailService();

        $message = <<<EOF

        Donation info:
        name: {$formData['first_name']}  {$formData['last_name']}
        phone: {$formData['phone']}
        email: {$formData['email']}
        address: {$formData['street_address']}
        date: {$mysql_date}


EOF;
        $emailObj->sendEmail("acomjean@gmail.com,treasurer@somervilleopenstudios.org ",
            "Sponsorship info", $message, 'webmaster@somervilleopenstudios.org' );
*/

    }

}