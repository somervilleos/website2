<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;




class ItineraryEmail {

    public function getForm(\Silex\Application $app) {





        $form = $app['form.factory']->createNamedBuilder('itin_email', (FormType::class))
     ->setMethod('POST')

     ->add('em', TextType::class, array(
                'attr' => array('style' => 'width:350px', 'placeholder' => ''),
                'label' => 'Email Address',
                'constraints' => new Assert\Email()
            ))
     ->add('submit', SubmitType::class)
     ->getForm();




        return $form;
}


    /**
     *
     * Add a key based on the input specified.
     *
     * @param $dbo
     * @param $formData
     * @return bool
     */



    public function processForm (\Silex\Application $app, $itinData , $formData){


        $shareKey = trim ($itinData['share_key']);
        $editKey = trim ($itinData['edit_key']);


        $emailObj = new \SOS\EmailService();

        $message = <<<EOF


        Here is a link to you Somerville Open Studios Favorites Itinerary.  Open this in your browser to enable the itinerary on this device.

        Note if you have an existing itinerary it will be overwritten

        https://www.somervilleopenstudios.org/web/artists/artist_directory/set_itinerary/{$shareKey}/{$editKey}



EOF;

        $to = $formData['em'];



        $emailObj->sendEmail($to,
            "SOS Itinerary info", $message, 'webmaster@somervilleopenstudios.org' );



            $app['session']->getFlashBag()->add('info','Email Sent ');
        sleep(5);


    }

}