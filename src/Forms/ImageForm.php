<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 10/24/17
 * Time: 12:34 AM
 */

namespace SOSForms;


use Silex\Application;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Validator\Constraints as Assert;


class ImageForm {

    private static $sql_debug = false;
    private static $debug = false;


    public function getForm(\Silex\Application $app, $imageData) {
/*
        'id' => string '10923' (length=5)
      'member_id' => string '592' (length=3)
      'ImageNumber' => string '3' (length=1)
      'Title' => string 'No bon pain' (length=11)
      'Medium' => string 'Photo' (length=5)
      'Dimensions' => string 'Venti' (length=5)
      'filename' => string 'artist_files/artist_images/profile/592-03.jpg' (length=45)
*/

        $form = $app['form.factory']->createBuilder(FormType::class)

            ->add('attachment', FileType::class, array( 'required'   => false,
            'label' => '1. Select Image (jpg or png) Click Browse',
                'attr' => array('class' => 'btn'),
            ))
            ->add('ImageNumber', HiddenType::class, array( 'required'   => false,
                'label' => 'other (really only if none other fit)',
                'data' => $imageData["ImageNumber"]
            ))

            ->add('Title', TextType::class, array( 'required'   => false,
                'label' => 'Title',
                'data' => $imageData["Title"]
            ))
            ->add('Medium', TextType::class, array( 'required'   => false,
                'label' => 'Medium',
                'data' => $imageData["Medium"]
            ))

            ->add('Dimensions', TextType::class, array( 'required'   => false,
                'label' => 'Dimensions: eg (10"x12)',
                'data' => $imageData["Dimensions"]
            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Upload/Update',
            ])
            ->getForm();


        return $form;

    }

    public  function processFormData ( \PDO $dbo, $formData, $file, Application $app,  $memberID ){

        $target_dir = "artist_files/artist_images/profile/";

        /*
         * Images are displayed at a maximum width of 700px. This is defined in
         * templates/artists/artist_profile.html.twig
         *
         * To the best extent possible, resize images down to the maximum
         * display width while keeping storage and bandwidth requirements in
         * check:
         *
         * - Landscape oriented photos are resized to 700px wide x <700px high.
         * - Portrait oriented photos with an aspect ratio < 16:9 (1244px high x
         * 700px wide) will be resized down to 700px wide x 700px to 1244px
         * wide.
         * - Portrait oriented photos with an aspect ratio > 16:9 will be resized
         * down to <700px wide, 1250px high.
         *
         * 16:9 is chosen as the maximum aspect ratio because in the year 2019,
         * some smartphones will take stills in this aspect ratio. It more than
         * covers the aspect ratio of 35mm film (3:2), which has carried over
         * into full-frame DSLRs and the APS-C sensors common in other DSLRs.
         * Other common aspect ratios for still photography include 4:3 and 5:4
         * which are likewise covered.
         */
        $MAX_WIDTH  =700;
        $MAX_HEIGHT =1250;
        $MAX_ASPECT_RATIO = 16.0 / 9.0;
        //define('MAX_TWIDTH', 200  );
        //define('MAX_THEIGHT', 200);

        $year = "";
        /*
        'attachment' =>
    object(Symfony\Component\HttpFoundation\File\UploadedFile)[960]
      private 'test' => boolean false
      private 'originalName' => string 'Screenshot from 2018-09-24 21-24-35.png' (length=39)
      private 'mimeType' => string 'image/png' (length=9)
      private 'size' => int 257074
      private 'error' => int 0
      private 'pathName' (SplFileInfo) => string '/tmp/phpwUV14M' (length=14)
      private 'fileName' (SplFileInfo) => string 'phpwUV14M' (length=9)
*/
        //https://github.com/symfony/symfony/blob/4.2/src/Symfony/Component/HttpFoundation/File/UploadedFile.php
        //$file->move($directory, $someNewFilename);


        if (!empty($file)) {
            try {
                $extension = $file->getClientOriginalExtension();
                $mime = $file->getClientMimeType();
                $path = $file->getPathName();
                //$name= $file->getName();


                //$valid_extensions = array('jpg', 'png', 'jpeg');
                $image_num = $formData['ImageNumber'];
                $jpg_number = array('space', '-01.jpg', '-02.jpg', '-03.jpg', '-04.jpg', '-05.jpg', '-06.jpg','-07.jpg', '-08.jpg', '-09.jpg');


                $name = $memberID . $jpg_number[$image_num];
                $target_file = $target_dir . $name;

                $image = new \SOS\SimpleImage();
                $loadStatus = $image->load($path);

                if (!$loadStatus) {
                    $app['session']->getFlashBag()->add('danger','Unable to update '.$formData['ImageNumber']  .' Image - '. $image->message);
                }else{
                    $w = $image->getWidth();
                    $h = $image->getHeight();

                    /* See comment above */
                    if ($w > $h || (($h / $w) < $MAX_ASPECT_RATIO)) {
                        $image->resizeToWidth($MAX_WIDTH);
                    } else {
                        $image->resizeToHeight($MAX_HEIGHT);
                    }
                    $image->save($target_file);
                    $app['session']->getFlashBag()->add('info', 'Image ' . $formData['ImageNumber'] . ' updated successfully');
                }
            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $app['session']->getFlashBag()->add('danger','Unable to update '.$formData['ImageNumber']  .' Image');

            }
        }


        // remove the old image data from the database.   If a new info is to be saved.

        $del_sql = "delete from image where member_id = :member_id and ImageNumber = :image_number";
        $del_stmt = $dbo->prepare ($del_sql);
        $del_stmt->bindParam(':image_number', $formData['ImageNumber'], \PDO::PARAM_INT);
        $del_stmt->bindParam(':member_id', $memberID , \PDO::PARAM_INT);

        $delStatus = $del_stmt->execute();



        $sql = "insert into image (member_id, ImageNumber, Title, Medium, Dimensions) VALUES (:member_id, :image_number, :title, :medium, :dimensions)";

        $stmt = $dbo->prepare ($sql);

        $stmt->bindParam(':title', $formData['Title'] , \PDO::PARAM_STR);
        $stmt->bindParam(':medium', $formData['Medium'] , \PDO::PARAM_STR);
        $stmt->bindParam(':dimensions', $formData['Dimensions']  , \PDO::PARAM_STR);
        $stmt->bindParam(':member_id', $memberID , \PDO::PARAM_INT);
        $stmt->bindParam(':image_number', $formData['ImageNumber'] , \PDO::PARAM_INT);

        if ($stmt->execute()) {

            $app['session']->getFlashBag()->add('info','Text for image '.$formData['ImageNumber']  .' updated successfully');

        } else {


            if (SELF::sql_debug) {
                echo " Query didn't work : {$sql} \n";
                print_r($stmt->errorInfo());


            }
            $resultData = null;
        }

        $resultData = 5;
        return $resultData;


    }


}
