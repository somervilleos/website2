<?php

if (    $_SERVER ["SERVER_NAME"]=="127.0.0.1" ||  $_SERVER ["SERVER_NAME"]=="localhost"){

    require_once '../vendor/autoload.php';
    require_once 'dboClass.php';

    $pdo = dboClass::getDBO();


} else {



    require_once('../../includes/SOS.php');
    require_once('../../includes/sql.php');
    $pdo = $sos_dbo;
}

echo '

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SOS Events</title>
    
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> 

  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>

    
            <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script> 
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>  
           <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    
    
     <style>
        [v-cloak] {
            display: none
        }

        /* Tables from https://css-tricks.com/complete-guide-table-element/ */
        table {
            border-collapse: collapse;
        }

        td,
        th {
            border: 1px solid #999;
            padding: 0.5rem;
            text-align: left;
        }

        table tbody tr:nth-child(2n) td {
            background: #edeef3;
        }

    </style>
    
</head>
<body>
<h1> Event Signup </h1>

<p> Click to sort column.  shift+click to select multiple columns to sort</p>

';




// UPDATE SOCIAL MEDIA IF SET
/*
$memberID = 0;


if (isset($_POST["social_media_id"])){


    $sm_id = $_POST["social_media_id"];
    $data = $_POST["new_val"];
    $memberID =  $_POST["member_id"];


    print "<b> Updating </b>";
    $sql = "UPDATE `profile_social_media` SET `user_name` = :data WHERE `id` = :sm_id;";
    $stmt = $pdo->prepare($sql);

    $stmt->bindParam(':sm_id', $sm_id, PDO::PARAM_STR);
    $stmt->bindParam(':data', $data, PDO::PARAM_STR);
    $result=$stmt->execute();

    if (!$result){
        echo "Error Details<br>";
        print_r($stmt->errorInfo());
    }

}


*/





$message = "";





$sql = "  SELECT  events.event_name, m.FirstName, m.LastName, m.EmailAddress, e.* FROM `events_member_join` e inner join member m on e.member_id =m.id inner join events on e.event_id = events.event_id ORDER BY `e`.`member_selection` ASC, `events`.`event_name` ASC 
            ";
// echo "$sql  $memberID<br>";
// . $id;
$stmt = $pdo->prepare($sql);

$stmt->bindParam(':member_id',$memberID, PDO::PARAM_STR);
$result=$stmt->execute();

if (!$result){
    echo "Error Details<br>";
    print_r($stmt->errorInfo());
}

$rows= $stmt->fetchAll();

$tablecols = ["event_name","FirstName","LastName","EmailAddress","id","member_id","event_id","member_selection","approved_selection","update_date","creation_date" ];


$count = count($rows) /5;


print "<p> Arists count: $count </p>";

print "<table id='events'><thead>";
foreach ($tablecols as $oneCol){
    print "<th> $oneCol </th> ";
}

//print "<th></th> <th></th> <th></th> ";
print "</thead><tbody>";
foreach ($rows as $oneRow){
    print "<tr>";
    foreach ($tablecols as $oneCol){
        print "<td> {$oneRow[$oneCol]} </td>";


    }


/*
print <<<EOL
<td>
   <form ACTION="{$_SERVER['PHP_SELF'] }" name="form1" method="POST" ">
    <input name="new_val" type="text" id="new_val" size="45" value="{$oneRow['']}">
</td>
<td>
   
       <input type="hidden" name="social_media_id" value="{$oneRow['id']}">
       <input type="hidden" name="member_id" value="{$memberID}">
       </td>
       <td>
       <input name="submit" type="submit" id="submit" value="go">
       </td>
   </form>
EOL;
*/
    print "</tr>";


}
print "</tbody></table>";


print <<<SC
<script>
$(document).ready( function () {
    
       var d = new Date();
            var yearText = d.getFullYear() + "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) + "-" +
                ("00" + d.getDate()).slice(-2) + " " +
                ("00" + d.getHours()).slice(-2) + ":" +
                ("00" + d.getMinutes()).slice(-2) + ":" +
                ("00" + d.getSeconds()).slice(-2);
    
    $('#events').DataTable({
         dom: 'lBfrtip',
         buttons: {
                        buttons: [
                            //'pageLength',
                            {
                                extend: 'copy',
                                exportOptions: {
                                    rows: {search: 'none'},
                                },
                            },
                            
                            {
                                extend: 'excelHtml5',
                                exportOptions: {
                                    rows: {search: 'none'},
                                },
                                //exportOptions: {orthogonal: 'export'},
                                title: "sos_event" + yearText
                            },
                            {
                                extend: 'csvHtml5',
                                //exportOptions: {orthogonal: 'export'},
                                exportOptions: {
                                    rows: {search: 'none'},
                                },
                                title: "sos_event_" + yearText

                            }
                        ],
                        dom: {
                            button: {
                                className: 'btn btn-outline-secondary'
                            }
                        }
                    },
            "lengthChange": true,
            "lengthMenu": [10, 50, 100, 200, 400, 500, 750, 1000,1400],
            "pageLength" : 1400,
        }
        
    );
} );
</script>


SC;

