<?php
/**
 * Created by PhpStorm.
 * User: acomjean
 * Date: 2/8/19
 * Time: 9:25 PM
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../vendor/autoload.php';
require_once 'dboClass.php';
require_once 'do_dump.php';
// Create a payment.

$pdo=  dboClass::getDBO();

$pid= 5000;

$paymentObj = new \SOSModels\Payments($pdo);

$hash = $paymentObj->getPaymentHash($pid);

print "$hash";

print "<br> payment/{$pid}/{$hash}";