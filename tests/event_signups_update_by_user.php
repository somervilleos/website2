<?php

if ($_SERVER ["SERVER_NAME"] == "127.0.0.1" || $_SERVER ["SERVER_NAME"] == "localhost") {

    require_once '../vendor/autoload.php';
    require_once 'dboClass.php';
    $pdo = dboClass::getDBO();


} else if ($_SERVER ["SERVER_NAME"] == "join.somervilleopenstudios.org") {

    require_once('../../includes/SOS.php');
    require_once('../../includes/sql.php');
    $pdo = $sos_dbo;
} else {
    print "non";
    exit();
}

echo '

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SOS Events</title>
    
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> 

  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>

    
            <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script> 
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>  
           <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    
    
     <style>
        [v-cloak] {
            display: none
        }

        /* Tables from https://css-tricks.com/complete-guide-table-element/ */
        table {
            border-collapse: collapse;
        }

        td,
        th {
            border: 1px solid #999;
            padding: 0.5rem;
            text-align: left;
        }

        table tbody tr:nth-child(2n) td {
            background: #edeef3;
        }

    </style>
    
</head>
<body>
<h1> Event Signup </h1>
  <br><a href="index.html"> admin home</a><br>
<p> Click to sort column.  shift+click to select multiple columns to sort</p>

';


// FOR For User Search

print $userForm = <<<FORM
<h4> Memeber ID or name</h4>
<form ACTION="{$_SERVER['PHP_SELF']}" name="form1" method="GET" ">
    <input name="searchval" type="text" id="searchval" size="45" value="">
       <input name="submit" type="submit" id="submit" value="go">
   </form><br><br>
FORM;


function addBlankEvents($dbo, $memberID) {


    $ok_event_ids = ["1013", "1019", "1017", "1020", "1018"];

    $response = "N";
    $theDate = date("Y-m-d H:i:s");

    foreach ($ok_event_ids as $eventID) {

        $sql = "INSERT INTO `events_member_join` (`id`, `member_id`, `event_id`, `member_selection`, `approved_selection`, `update_date`, `creation_date`) VALUES (NULL, :member_id, :event_id , :user_selection, :selection, :date, :date2);
";
        $stmt = $this->sos_dbo->prepare($sql);

        $stmt->bindValue(":event_id", $eventID, \PDO::PARAM_STR);
        $stmt->bindValue(":member_id", $memberID, \PDO::PARAM_STR);
        $stmt->bindValue(":user_selection", $response, \PDO::PARAM_STR);
        $stmt->bindValue(":selection", $response, \PDO::PARAM_STR);
        $stmt->bindValue(":date", $theDate, \PDO::PARAM_STR);
        $stmt->bindValue(":date2", $theDate, \PDO::PARAM_STR);


// run the insert query
        if ($stmt->execute()) {


        } else {

            if (\SOSModels\Globals::$sql_debug) {
                print "SQL: $sql ";
                echo "\nPDO::errorInfo():\n";
                print_r($stmt->errorInfo());
            }
            return [];
        }
    }
}


if (isset($_GET["searchval"])) {


    if (is_numeric($_GET["searchval"])) {

        $memberID = $_GET["searchval"];

    } else {

        $name = $pdo->quote("%" . $_GET["searchval"] . "%");

        print ("<h2> $name </h2>");

        $sql = <<<EM
 SELECT * FROM `current_active_artists_view` WHERE `PublicFirstName` LIKE {$name} 
        or PublicLastName like {$name}
EM;


        $stmt = $pdo->prepare($sql);
        $result = $stmt->execute();

        if (!$result) {
            echo "Error Details<br>";
            print_r($stmt->errorInfo());

        } else {


            $rows = $stmt->fetchAll();

            /*  print ("<pre>");
              var_dump ($rows);
              print ("</pre>");
            */


            if (!empty($rows)) {
                print "<b> Select Artist Name to Show Events </b>";
                print "<ul>";
                foreach ($rows as $oneRow) {

                    print <<<TY
<li>
                    <a href="{$_SERVER['PHP_SELF']}?searchval={$oneRow['member_id']}">  {$oneRow['PublicFirstName']} {$oneRow['PublicLastName']}       </a>
    </li>                                    
TY;

                }
                print "</ul>";

            }


            exit();
        }
    }
}


print "<pre>";
//var_dump ($_SERVER);
print "</pre>";


if (!isset($memberID)) {

    exit();
}


// ---------------------------------------------
//  UPDATE DATA FROM POST
// ---------------------------------------------


$sm_id = 0;
if (isset($_POST["event_id"])) {

    $sm_id = $_POST["event_id"];
    $data = $_POST["change_to_approved"];


    print "<b> Updating </b>";
    $sql = "UPDATE `events_member_join` SET `approved_selection` = :data WHERE `id` = :sm_id limit 1;";
    $stmt = $pdo->prepare($sql);

    $stmt->bindParam(':sm_id', $sm_id, PDO::PARAM_STR);
    $stmt->bindParam(':data', $data, PDO::PARAM_STR);
    $result = $stmt->execute();

    if (!$result) {
        echo "Error Details<br>";
        print_r($stmt->errorInfo());
    }

}

if (isset($_POST["event_id_mem"])) {

    $sm_id = $_POST["event_id_mem"];
    $data = $_POST["change_to_member"];


    print "<b> Updating Memeber Select </b>";
    $sql = "UPDATE `events_member_join` SET `member_selection` = :data WHERE `id` = :sm_id limit 1;";
    $stmt = $pdo->prepare($sql);

    $stmt->bindParam(':sm_id', $sm_id, PDO::PARAM_STR);
    $stmt->bindParam(':data', $data, PDO::PARAM_STR);
    $result = $stmt->execute();

    if (!$result) {
        echo "Error Details<br>";
        print_r($stmt->errorInfo());
    }

}


$message = "";


$member_ids = [$memberID];

foreach ($member_ids as $oneMember) {

    $sql = "SELECT  events.event_name, m.FirstName, m.LastName, m.EmailAddress, e.* FROM `events_member_join` e inner join member m on e.member_id =m.id inner join events on e.event_id = events.event_id where e.member_id = :member_id ORDER BY `e`.`event_id` ASC 
            ";
// echo "$sql  $memberID<br>";
// . $id;
    $stmt = $pdo->prepare($sql);

    $stmt->bindParam(':member_id', $memberID, PDO::PARAM_STR);
    $result = $stmt->execute();

    if (!$result) {
        echo "Error Details<br>";
        print_r($stmt->errorInfo());
        continue;
    }

    $rows = $stmt->fetchAll();

    $tablecols = ["FirstName", "LastName", "EmailAddress", "id", "member_id", "event_id", "member_selection", "approved_selection", "change approved", "update_date"];


    $count = count($rows);


    print "<h2>  " . $rows[0]['event_name'] . "</h2>";

    print "<p> Count: $count </p>";

    print "<table id='event_{$oneMember}'><thead>";
    foreach ($tablecols as $oneCol) {
        print "<th> $oneCol </th> ";
    }

//print "<th></th> <th></th> <th></th> ";
    print "</thead><tbody>";
    foreach ($rows as $oneRow) {


        if ($oneRow['id'] == $sm_id) {
            print "<tr style='background-color:lightyellow !important'>";
        } else {
            print "<tr>";
        }


        foreach ($tablecols as $oneCol) {


            if ($oneCol == "member_selection") {

                if ($oneRow['member_selection'] == 'N') {
                    print "<td style='background-color:lightcoral'>{$oneRow[$oneCol]}";
                } else {
                    print "<td style='background-color:lightgreen'>{$oneRow[$oneCol]}";
                }


                if ($oneRow['member_selection'] == 'N') {
                    $changeTo = 'Y';
                } else {
                    $changeTo = 'N';
                }


                print <<<FORM
 <form ACTION="{$_SERVER['REQUEST_URI']}" name="form2" method="POST" ">
          <input type="hidden" name="event_id_mem" value="{$oneRow['id']}">
          <input type="hidden" name="change_to_member" value="{$changeTo}">
              <input name="submit" type="submit" id="submit" value="change to {$changeTo}">
   </form></td>
FORM;

            } else if ($oneCol == "approved_selection") {


                if ($oneRow['approved_selection'] == 'N') {
                    print "<td style='background-color:lightcoral'>{$oneRow[$oneCol]} ";
                } else {
                    print "<td style='background-color:lightgreen'>{$oneRow[$oneCol]} ";
                }

            } else if ($oneCol == "change approved") {

                // get the change to button.
                if ($oneRow['approved_selection'] == 'N') {
                    $changeTo = 'Y';
                } else {
                    $changeTo = 'N';
                }


                print <<<FORM
 <form ACTION="{$_SERVER['REQUEST_URI']}" name="form1" method="POST" ">
          <input type="hidden" name="event_id" value="{$oneRow['id']}">
          <input type="hidden" name="change_to_approved" value="{$changeTo}">
              <input name="submit" type="submit" id="submit" value="change to {$changeTo}">
   </form></td>
FORM;
            } else {
                if ($oneRow['id'] == $sm_id) {
                    print "<td style='background-color:yellow !important'>";;
                } else {


                    print "<td> ";
                }

                print " {$oneRow[$oneCol]} </td>";


            }


        }

    print "</tr>";


}
print "</tbody></table>";


print <<<SC
<script>



$(document).ready( function () {
    
    
    $('input[type=radio]').on('change', function() {
    $(this).closest("form").submit();
});

    
    
       var d = new Date();
            var yearText = d.getFullYear() + "-" +
                ("00" + (d.getMonth() + 1)).slice(-2) + "-" +
                ("00" + d.getDate()).slice(-2) + " " +
                ("00" + d.getHours()).slice(-2) + ":" +
                ("00" + d.getMinutes()).slice(-2) + ":" +
                ("00" + d.getSeconds()).slice(-2);
    
    $('#event_{$oneEvents}').DataTable({
         dom: 'lBfrtip',
         buttons: {
                        buttons: [
                            //'pageLength',
                            {
                                extend: 'copy',
                                exportOptions: {
                                    rows: {search: 'none'},
                                },
                            },
                            
                            {
                                extend: 'excelHtml5',
                                exportOptions: {
                                    rows: {search: 'none'},
                                },
                                //exportOptions: {orthogonal: 'export'},
                                title: "sos_event" + yearText
                            },
                            {
                                extend: 'csvHtml5',
                                //exportOptions: {orthogonal: 'export'},
                                exportOptions: {
                                    rows: {search: 'none'},
                                },
                                title: "sos_event_" + yearText

                            }
                        ],
                        dom: {
                            button: {
                                className: 'btn btn-outline-secondary'
                            }
                        }
                    },
            "lengthChange": true,
            "lengthMenu": [10, 50, 100, 200, 400, 500, 750, 1000,1400],
            "pageLength" : 1400,
        }
        
    );
} );
</script>


SC;

}