
#
# Typically as registration begins we reset the info in the database.  The website should display it as "not yet registered for YYYY"
#

update member set Active='N', community_space='N', Participating ='N', Pending ='N', `Pending_Date`=NULL, `approval_date`= NULL, `payment_id` = NULL, `checked` = 'N', `inside_out`='N',`fashion_show`='N', `FashionInfo`='N', `financial_aid`='N', `community_space_info`=''