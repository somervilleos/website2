<?php

// configure your app for the production environment

$app['twig.path'] = array(__DIR__.'/../templates');
//$app['twig.form.templates'] = array("bootstrap_3_horizontal_layout.html.twig");
$app['twig.form.templates'] = array("bootstrap_3_layout.html.twig");

$app['google_map_key'] ='AIzaSyA101IuRXIsQ0lzVObJ7uEXZTx-xCLDrtY';

$app['year'] = 2020;
$app['financial_year'] = 2019;

// SOS PDO DATABASE object



$s= $_SERVER['HTTP_HOST'];
if (strpos($s,'dev.somer') != false){

    $app['pdo.dsn'] = 'mysql:dbname=database_name';
    $app['pdo.user'] = 'my_username';
    $app['pdo.password'] = 'my_password';
} else {
    $app['pdo.dsn'] = 'mysql:dbname=database_name';
    $app['pdo.user'] = 'my_username';
    $app['pdo.password'] = 'my_password';
}


$app['pdo'] = new PDO(
    $app['pdo.dsn'],
    $app['pdo.user'],
    $app['pdo.password'],
    array(
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    )
);


